#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n;
	cin >> n;
	int temp;
	map<int, int> hashMap;
	for (int i = 1; i <= n; i++) {
		cin >> temp;
		int diff = abs(temp - i);
		hashMap[diff]++;
	}
	for (auto it = hashMap.rbegin(); it != hashMap.rend(); it++) {
		if(it->second != 1)
			cout << it->first << " " << it->second << endl;
	}

	system("pause");
	return 0;
}