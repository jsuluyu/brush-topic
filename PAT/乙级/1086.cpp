#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int x, y;
	cin >> x >> y;
	long long sum = x * y;
	int temp;
	vector<int> vArr;
	while (sum) {
		temp = sum % 10;
		sum /= 10;
		vArr.push_back(temp);
	}
	bool flag = true;
	for (auto it = vArr.begin(); it != vArr.end(); it++) {
		if (*it == 0 && flag)
			continue;
		if (*it != 0)
			flag = false;
		cout << *it;
	}

	system("pause");
	return 0;
}