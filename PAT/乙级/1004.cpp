#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

struct student {
	string name;
	string ID;
	int score;
};

int main()
{
	int n;
	int nTemp;
	vector<struct student> stu;
	cin >> n;
	nTemp = n;
	while (nTemp--) {
		struct student temp;
		cin >> temp.name >> temp.ID >> temp.score;
		stu.push_back(temp);
	}

	int max = -1, maxIdx = 0;
	int min = 101, minIdx = 0;
	for (int i = 0; i < n; i++) {
		if (stu[i].score > max) {
			maxIdx = i;
			max = stu[i].score;
		}
		if (stu[i].score < min) {
			minIdx = i;
			min = stu[i].score;
		}
	}

	cout << stu[maxIdx].name << " " << stu[maxIdx].ID << endl;
	cout << stu[minIdx].name << " " << stu[minIdx].ID << endl;

	//system("pause");
	return 0;
}
