#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct player
{
	string id;
	int x;
	int y;
	double dis;
};

bool cmp(player p1, player p2)
{
	if (p1.dis < p2.dis)
		return true;
	else
		return false;
}

int main()
{
	int n;
	cin >> n;
	vector<player> vPl(n);
	for (int i = 0; i < n; i++) {
		cin >> vPl[i].id >> vPl[i].x >> vPl[i].y;
		int temp = pow(vPl[i].x, 2) + pow(vPl[i].y, 2);
		vPl[i].dis = sqrt(temp);
	}
	sort(vPl.begin(), vPl.end(), cmp);
	cout << vPl[0].id << " " << vPl[n - 1].id << endl;

	system("pause");
	return 0;
}