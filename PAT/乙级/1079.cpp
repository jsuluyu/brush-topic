#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

bool isPali(string& num)
{
	int len = num.length() - 1;
	for (int i = 0; i <= (len+1) / 2; i++) {
		if (num[i] != num[len - i])
			return false;
	}
	return true;
}

string add(string& A, string& B) 
{
	string ans = "";
	int temp, carry = 0;
	int len = A.length();
	for (int i = len - 1; i >= 0; i--) {
		temp = A[i] - '0' + B[i] - '0' + carry;
		ans += to_string(temp % 10);
		carry = temp / 10;
	}
	if (carry)
		ans += "1";
	reverse(ans.begin(), ans.end());
	return ans;
}

int main()
{
	string num;
	cin >> num;
	int count = 0;
	if (isPali(num)) {
		cout << num << " is a palindromic number." << endl;
		return 0;
	}
	while (count < 10) {
		string A = num;
		string B;
		reverse(num.begin(), num.end());
		B = num;
		cout << A << " + " << B << " = ";
		num = add(A, B);
		cout << num << endl;
		if (isPali(num)) {
			cout << num << " is a palindromic number." << endl;
			break;
		}
		count++;
	}
	if (count >= 10)
		cout << "Not found in 10 iterations." << endl;
	
	system("pause");
	return 0;
}