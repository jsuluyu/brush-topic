#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n, m;
	int tempA, tempB;
	vector<int> couple(100000, -1);
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> tempA >> tempB;
		couple[tempA] = tempB; // A的对象是B
		couple[tempB] = tempA; // B的对象是A
	}

	cin >> m;
	vector<int> guest(m), isExist(100000); // isExist表示某人的对象是否来了
	for (int i = 0; i < m; i++) {
		cin >> guest[i];
		if (couple[guest[i]] != -1) // 表示guest[i]有对象  couple[guest[i]]：表示guest[i]的对象 
			isExist[couple[guest[i]]] = 1; // 表示guest[i]的对象到场了
	}
	// 遍历 1.哪些有对象的但是对象没到场的 2.没对象的
	set<int> hashSet;
	for (int i = 0; i < m; i++) {
		if (!isExist[guest[i]])
			hashSet.insert(guest[i]);
	}
	cout << hashSet.size() << endl;
	for (auto it = hashSet.begin(); it != hashSet.end(); it++) {
		if (it != hashSet.begin()) cout << " ";
		printf("%05d", *it);
	}

	system("pause");
	return 0;
}