#include <iostream>
#include <cstdio>
#include <string>
using namespace std;

int main()
{
	int n;
	int pInx, tIdx;
	string str;
	cin >> n;
	while (n > 0) {
		bool flag = false;
		int count[3] = { 0 };
		cin >> str;
		// cout << str;
		pInx = tIdx = 0;
		for (int i = 0; i < str.size(); i++) {
			if (str[i] != 'P' && str[i] != 'A' && str[i] != 'T') {
				flag = true;
				cout << "NO" << endl;
				break;
			}
			if (str[i] == 'P') {
				pInx = i;
				count[0]++;
			}
			if (str[i] == 'T') {
				tIdx = i;
				count[2]++;
			}
		}
		if (!flag) {
			if (count[0] != 1 || count[2] != 1 || pInx > tIdx || (tIdx - pInx) == 1 || (pInx * (tIdx - pInx - 1) != str.size() - tIdx - 1))
				cout << "NO" << endl;
			else
				cout << "YES" << endl;
		}
		
		n--;
	}

	//system("pause");
	return 0;
}
