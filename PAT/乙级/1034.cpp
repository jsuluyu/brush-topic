#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

#define lg long 

// 辗转相除法
lg getGcd(lg x, lg y)
{
	if (y == 0)
		return x;
	else
		return getGcd(y, x % y);
}

void getFraction(lg x, lg y)
{
	// 首先判断被除数是否为0，若为0，直接返回inf
	if (y == 0) {
		printf("Inf");
		return;
	}

	// 记录除数，被除数的正负形式
	int inegative = 1;
	if (x < 0) { x = -x; inegative *= -1; }
	if (y < 0) { y = -y; inegative *= -1; }

	// 化简分数，先求得分子分母的最大公约数，然后再同除最大公约数
	long gcd = getGcd(x, y);
	x /= gcd;
	y /= gcd;

	// 开始按题目要求输出结果
	if (inegative == -1) printf("(-");
	if (x / y && x % y) // 带分数形式
		printf("%ld %ld/%ld", x / y, x % y, y);
	else if (x % y)
		printf("%ld/%ld", x % y, y);
	else
		printf("%ld", x / y);
	if (inegative == -1) printf(")");
}

int main()
{
	lg a1, b1, a2, b2;
	scanf_s("%ld/%ld %ld/%ld", &a1, &b1, &a2, &b2);

	char op[4] = { '+', '-', '*', '/' };
	for (char ch : op) {
		getFraction(a1, b1); // 转化除数为分数形式
		printf(" %c ", ch);
		getFraction(a2, b2); // 转化被除数为分数形式
		printf(" = ");
		switch (ch) {
			case '+': getFraction(a1 * b2 + a2 * b1, b1 * b2); break;
			case '-': getFraction(a1 * b2 - a2 * b1, b1 * b2); break;
			case '*': getFraction(a1 * a2, b1 * b2); break;
			case '/': getFraction(a1 * b2, b1 * a2); break;
		}
		printf("\n");
	}

	system("pause");
	return 0;
}
