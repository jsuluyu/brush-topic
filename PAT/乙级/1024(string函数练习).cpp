// string函数练习

#include<iostream>
#include<string>
using namespace std;
int main(){
	//substr截取字符串
	//find返回字符串位置下标
	//string s6(5,'0');  //生成一个字符串，包含5个'0'字符
	
	 //整数//点//小数//指数 
	 
	string str;
	cin>>str;
	int dotIndex; 
	int eIndex;
	dotIndex=str.find('.');
	eIndex=str.find('E');
	//cout<<dotIndex<<endl<<eIndex<<endl<<"==============="<<endl;

	
	string integer;//整数 
	string decimal;//小数 
	int exp;//指数 
	integer=str.substr(1,dotIndex-1);//截取整数
	decimal=str.substr(dotIndex+1,eIndex-dotIndex-1);//截取小数 
	exp=stoi(str.substr(eIndex+2));//截取指数 ，并转为整数 
	//cout<<integer<<endl<<decimal<<endl<<exp<<endl<<"==============="<<endl;
	
	if(str[0]=='-'){//第一位输出正负号//正号不输出 
		cout<<"-";
	}
	
 	//指数为-，和整数部分比较谁大//整数小前面补0//否则不补
	if(str[eIndex+1]=='-'){
		//指数为-向左移动 
		if(integer.size()>exp){
			cout<<integer.substr(0,integer.size()-exp)<<"."<<integer.substr(integer.size()-exp)<<decimal<<endl;
		}else{
			cout<<"0."<<string(exp-integer.size(),'0')<<integer<<decimal<<endl;
		}
	}else{//指数为+向右移动 
		if(decimal.size()>exp){//小数位数够移 
			cout<<integer<<decimal.substr(0,exp)<<"."<<decimal.substr(exp)<<endl;
			//    整数    小数部分截取指数位，    点    从上一次最后截取的位置起，截到最后 
		}else{//小数位数不够移
			cout<<integer<<decimal<<string(exp-decimal.size(),'0')<<endl;
			//    整数    小数部分   补不够的位数的0 
		}
	}
	return 0;
}
