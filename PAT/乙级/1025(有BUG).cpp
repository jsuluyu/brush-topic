#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
using namespace std;

/*
测试用例
00100 6 4
00000 4 99999
00100 1 12309
68237 6 -1
33218 3 00000
99999 5 68237
12309 2 33218
*/

typedef struct linkNode
{
	string address;
	int data;
	string next;
};

int main()
{
	int N, K;
	string head;
	cin >> head >> N >> K;
	vector<linkNode> tLinkList(N); // 读入输入数据，然后再组织成链表结构
	vector<linkNode> linkList;

	for (int i = 0; i < N; i++) {
		cin >> tLinkList[i].address >> tLinkList[i].data >> tLinkList[i].next;
	}

	// 构造链表
	int len = 0; // 链表的实际长度
	string tHead = head;

	while(tHead != "-1") {
		for (int j = 0; j < N; j++) {
			if (tLinkList[j].address == tHead) {
				//linkList[i] = tLinkList[j];
				linkList.push_back(tLinkList[j]);
				tHead = tLinkList[j].next;
				len++;
				break;
			}
		}
	}

	/*cout << head <<" " << head.length() << endl;
	for (int i = 0; i < N; i++) {
		cout << linkList[i].address << " " << linkList[i].data << " " << linkList[i].next << endl;
	}
	cout << endl;*/

	int end = len - len % K - 1; // 最后翻转的结点索引

	for (int i = 0; i <= end; i += K) {
		int tempEnd = i + K - 1;
		int k = 0;
		int num = (tempEnd + i) / 2;
		for (int j = i; j <= num; j++) {
			linkNode Node = linkList[tempEnd - k];
			linkList[tempEnd - k] = linkList[j];
			linkList[j] = Node;
			k++;
		}
	}

	for (int i = 0; i < len; i++) {
		cout << linkList[i].address << " " << linkList[i].data << " " << linkList[i].next << endl;
	}

	system("pause");
	return 0;
}
