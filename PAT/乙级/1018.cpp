#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
using namespace std;

int MAX(int count[][3], int idx)
{
	int max = -1;
	int maxIdx = 0;
	for (int i = 0; i < 3; i++) {
		if (count[idx][i] > max) {
			max = count[idx][i];
			maxIdx = i;
		}
	}
	return maxIdx;
}

int main()
{
	int N;
	string str[1001];
	int count[2][3] = {0}; //0--B  1--c 2--j  
	int record[2][3] = { 0 }; // 0--胜利 1--平 2--失败

	cin >> N;
	for (int i = 0; i <= N; i++) {
		getline(cin, str[i]);
	}
	/*for (int i = 0; i < N; i++) {
		cout <<str[i] << endl;
	}*/

	for (int i = 0; i <= N; i++) {
		if (str[i][0] == 'C' && str[i][2] == 'B') { // 锤子<布
			count[1][0]++; // 胜利的首饰
			record[1][0]++; // 胜利的选手
			record[0][2]++; // 失败的选手
		} 
		if (str[i][0] == 'C' && str[i][2] == 'J') { // 锤子>剪刀
			count[0][1]++; // 胜利的首饰
			record[0][0]++; // 胜利的选手
			record[1][2]++; // 失败的选手
		}
		if (str[i][0] == 'C' && str[i][2] == 'C') { // 锤子==锤子
			//count[0][1]++; // 胜利的首饰
			record[0][1]++; // 胜利的选手
			record[1][1]++; // 失败的选手
		}
		if (str[i][0] == 'B' && str[i][2] == 'C') { // 布 > 锤子
			count[0][0]++; // 胜利的首饰
			record[0][0]++; // 胜利的选手
			record[1][2]++; // 失败的选手
		}
		if (str[i][0] == 'B' && str[i][2] == 'J') { // 布 < 剪刀
			count[1][2]++; // 胜利的首饰
			record[1][0]++; // 胜利的选手
			record[0][2]++; // 失败的选手
		}
		if (str[i][0] == 'B' && str[i][2] == 'B') { // 布==布
			//count[0][1]++; // 胜利的首饰
			record[0][1]++; // 胜利的选手
			record[1][1]++; // 失败的选手
		}
		if (str[i][0] == 'J' && str[i][2] == 'B') { // 剪刀>布
			count[0][2]++; // 胜利的首饰
			record[0][0]++; // 胜利的选手
			record[1][2]++; // 失败的选手
		}
		if (str[i][0] == 'J' && str[i][2] == 'C') { // 剪刀<锤子
			count[1][1]++; // 胜利的首饰
			record[1][0]++; // 胜利的选手
			record[0][2]++; // 失败的选手
		}
		if (str[i][0] == 'J' && str[i][2] == 'J') { // 剪刀==剪刀
			//count[0][1]++; // 胜利的首饰
			record[0][1]++; // 胜利的选手
			record[1][1]++; // 失败的选手
		}
	}

	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 3; j++) {
			cout << (j == 0 ? "" : " ") << record[i][j];
		}
		cout << endl;
	}
	int t1 = MAX(count, 0);
	int t2 = MAX(count, 1);
	if (t1 == 0)
		cout << "B ";
	if (t1 == 1)
		cout << "C ";
	if (t1 == 2)
		cout << "J ";
	if (t2 == 0)
		cout << "B";
	if (t2 == 1)
		cout << "C";
	if (t2 == 2)
		cout << "J";

	system("pause");
	return 0;
}
