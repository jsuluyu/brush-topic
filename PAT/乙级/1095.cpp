#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct student
{
	string id;
	int score;
};

struct tStu
{
	string clas;
	int count = 0;
};

bool cmp(student&x, student& y)
{
	if (x.score != y.score) return x.score > y.score;
	else return x.id < y.id;
}

bool cmp2(tStu& x, tStu& y)
{
	if (x.count != y.count) return x.count > y.count;
	else return x.clas < y.clas;
}

int main()
{
	int n, m;
	cin >> n >> m;
	vector<student> vArr(n);
	for (int i = 0; i < n; i++) {
		cin >> vArr[i].id >> vArr[i].score;
	}
	int command;
	for (int i = 0; i < m; i++) {
		cin >> command;
		if (command == 1) {
			char ch;
			cin >> ch;
			vector<student> meetReq;
			for (int j = 0; j < n; j++) {
				if (vArr[j].id[0] == ch)
					meetReq.push_back(vArr[j]);
			}
			printf("Case %d: %d %c\n", i + 1, command, ch);
			if (meetReq.empty())
				printf("NA\n");
			else {
				sort(meetReq.begin(), meetReq.end(), cmp);
				for (int j = 0; j < meetReq.size(); j++)
					cout << meetReq[j].id << " " << meetReq[j].score << endl;
				meetReq.clear();
			}
		}
		else if (command == 2) {
			string str; // 考场号
			cin >> str;
			int count = 0; // 班级个数
			int sum = 0; // 总分
			for (int j = 0; j < n; j++) {
				string subStr = vArr[j].id.substr(1, 3);
				if (subStr == str) {
					count++;
					sum += vArr[j].score;
				}
			}
			printf("Case %d: %d %s\n", i + 1, command, str.c_str());
			if (count == 0)
				printf("NA\n");
			else
				printf("%d %d\n", count, sum);
		}
		else if (command == 3) {
			string str; // 日期
			cin >> str;
			unordered_map<string, int> hashMap;
			//vector<int> v(1000, 0);
			vector<tStu> v;
			tStu temp;
			for (int j = 0; j < n; j++) {
				string subStr = vArr[j].id.substr(4, 6); // 日期
				if (subStr == str) {
					string tClass = vArr[j].id.substr(1, 3);
					//v[stoi(tClass)]++;
					hashMap[tClass]++;
				}
			}
			for (auto it = hashMap.begin(); it != hashMap.end(); it++) {
				temp.clas = it->first;
				temp.count = it->second;
				v.push_back(temp);
			}
			sort(v.begin(), v.end(), cmp2);
			printf("Case %d: %d %s\n", i + 1, command, str.c_str());
			if (v.size() == 0)
				printf("NA\n");
			else {
				for (int j = 0; j < v.size(); j++) {
					printf("%s %d\n", v[j].clas.c_str(), v[j].count);
				}
			}
		}
		else
			printf("NA\n");
	}

	system("pause");
	return 0;
}