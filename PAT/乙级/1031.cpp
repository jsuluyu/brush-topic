#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

char M[11] = { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'};
int weight[17] = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};

bool checkAllNum(string str)
{
	for (int i = 0; i < str.length()-1; i++) { // 注意最后一位校验码可以是“X”，因此只需检测前17位是否是数字
		if (str[i] < '0' || str[i] > '9')
			return false;
	}
	return true;
}

bool getCheckCode(string str)
{
	int sum = 0;
	for (int i = 0; i < str.length(); i++) {
		int num = str[i] - '0';
		sum += num * weight[i];
	}
	int Z = sum % 11;
	char code = M[Z];
	if (str[17] != code)
		return false;
	else
		return true;
}

int main()
{
	int N;
	vector<string> arr;

	string temp;
	cin >> N;
	for (int i = 0; i < N; i++) {
		cin >> temp;
		arr.push_back(temp);
	}

	int count = 0;
	for (int i = 0; i < N; i++) {
		if (!checkAllNum(arr[i]) || !getCheckCode(arr[i])) // 首先检测是否是全数字,再检查校验码
			cout << arr[i] << endl;
		else
			count++;
	}

	if (count == N)
		cout << "All passed" << endl;

	system("pause");
	return 0;
}
