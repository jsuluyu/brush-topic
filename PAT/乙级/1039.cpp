#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	string str1, str2;
	cin >> str1 >> str2;

	// 在str2中寻找str1中的元素
	for (int i = 0; i < str1.length(); i++) {
		for (int j = 0; j < str2.length(); j++) {
			if (str1[i] == str2[j]) {
				str1[i] = '$';
				str2[j] = '$';
				break;
			}
		}
	}

	int count1 = 0, count2 = 0;
	for (int i = 0; i < str2.length(); i++) { // str2[i] != '$'代表这个珠子str1中没有或者不够
		if (str2[i] != '$') count2++; 
	}
	if (count2 == 0) {
		for (int i = 0; i < str1.length(); i++) {
			if (str1[i] != '$') count1++;
		}
	}

	if (count2 == 0)
		printf("Yes %d", count1);
	else
		printf("No %d", count2);

	system("pause");
	return 0;
}


