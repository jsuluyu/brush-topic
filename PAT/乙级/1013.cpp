#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <algorithm>
#include <cmath>
using namespace std;

bool check(int num)
{
	for (int i = 2; i <= sqrt(num); i++) {
		if (num % i == 0)
			return false;
	}
	return true;
}

int main()
{
	int m, n;
	int startPrime = 2;
	int count = 0; // 素数的索引
	cin >> m >> n;

	int temp = startPrime;
	while (count < m) {
		
		if (check(temp)) {
			count++;
			startPrime = temp;
		}
		temp++;
	}

	// cout << startPrime << count << endl;
	int num = 0; // 控制换行
	while(count <= n) {
		//int num = startPrime;
		if (check(startPrime)) {
			if (num % 10 == 0 && num != 0)
				cout << endl;
			else
				cout << (num != 0 ? " " : "");
			cout << startPrime;
			//cout << (count == n + 1 ? "" : " ");
			//if (num % 10 != 0)
			//	cout << " ";
			num++;
			count++;	
		}		
		startPrime++;
	}

	system("pause");
	return 0;
}
