#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	string str1, str2;
	getline(cin, str1);
	getline(cin, str2);
	string str = str1 + str2;
	int hashSet[200] = {0};
	for (int i = 0; i < str.length(); i++) {
		if (hashSet[str[i]] == 0) cout << str[i];
		hashSet[str[i]] = 1;
	}

	system("pause");
	return 0;
}