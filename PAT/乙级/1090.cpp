#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n, m;
	cin >> n >> m;
	map<int, vector<int>> hashMap;
	int str1, str2;
	for (int i = 0; i < n; i++) {
		cin >> str1 >> str2;
		hashMap[str1].push_back(str2);
		hashMap[str2].push_back(str1);
	}
	int num;
	for (int i = 0; i < m; i++) {
		bool flag = false;
		cin >> num;
		vector<int> vArr(num);
		vector<int> help(100001, 0);
		for (int j = 0; j < num; j++) {
			cin >> vArr[j];
			help[vArr[j]] = 1; // 标记同批货物出现的货物
		}
			
		for (int j = 0; j < num; j++) { // 遍历货物列表
			for (int k = 0; k < hashMap[vArr[j]].size(); k++) { // 遍历不相容清单
				int temp = hashMap[vArr[j]][k]; // 不相容的货物
				// cout << hashMap[vArr[j]][k] << " " << vArr[j] << endl;
				if (help[temp] == 1) { // 不相容的货物出现了
					flag = true;
					break;
				}
			}
		}
		printf("%s\n", flag ? "No" : "Yes");
		vArr.clear();
	}
	
	system("pause");
	return 0;
}