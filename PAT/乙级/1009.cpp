#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <algorithm>
using namespace std;

int main()
{
	stack<string> stack1;
	string str;
	getline(cin, str);

	for (int i = 0; i < str.size(); i++) {
		if (str[i] == '\n')
			break;
		int idx = i; // 空格的位置
		string temp = "";
		while (str[idx] == ' ') idx++; // 去掉开头多余空格
		i = idx;
		while (str[idx] != ' ' && str[idx] != '\0')	idx++;
		while (i < idx) {
			temp += str[i];
			i++;
		}
		stack1.push(temp);
		i = idx-1;
	}

	//int len = stack1.size();
	while (!stack1.empty()) {
		cout << stack1.top();
		stack1.pop();
		cout << (stack1.empty() ? "" : " ");
	}

	system("pause");
	return 0;
}
