#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
using namespace std;

int main()
{
	string A, Q = ""; // 假设A=123456  B=5
	int B, R = 0;

	cin >> A >> B;

	for (int i = 0; i < A.length(); i++) { // 模仿除法过程
		R = R * 10 + A[i] - '0'; // 令R为A的当前位
		Q += R / B + '0';
		R = R % B;
	}

	while (!Q.empty() && Q.front() == '0') // 删除首部的0
		Q.erase(Q.begin());
	if (Q.size() == 0)
		cout << "0 " << R;
	else {
		for (int i = 0; i < Q.size(); i++) {
			cout << Q[i];
		}
		cout << " " << R;
	}

	//printf("%s %d", Q.size() == 0 ? "0" : Q.c_str(), R);//Q为空先输出一个0再输出R

	system("pause");
	return 0;
}
