#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	string str;
	string model = "PATest";
	getline(cin, str);
	unordered_map<char, int> hashMap;

	int sum = 0; // 记录总共含有model的个数
	for (char ch : str) {
		if (model.find(ch) != string::npos) {
			hashMap[ch]++;
			sum++;
		}
			
	}

	while (sum) {
		for (char ch : model) {
			if (hashMap[ch] != 0) {
				cout << ch;
				hashMap[ch]--;
				sum--;
			}
		}
	}

	system("pause");
	return 0;
}


