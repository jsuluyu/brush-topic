#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

bool checkTail(int& x, string& str)
{
	int len = str.length();
	string temp = to_string(x);
	string subStr = temp.substr(temp.length()-len); //截取temp.length()-len到结尾
	if (subStr == str) return true;
	else return false;
}

int main()
{
	int n;
	cin >> n;
	string num;
	for (int i = 0; i < n; i++) {
		bool flag = false;
		cin >> num;
		int powNum = pow(stoi(num), 2);
		for (int j = 1; j < 10; j++) {
			int temp = powNum * j;
			if (checkTail(temp, num)) {
				cout << j << " " << temp << endl;
				flag = true;
				break;
			}
		}
		if (!flag) cout << "No" << endl;
	}
	
	system("pause");
	return 0;
}