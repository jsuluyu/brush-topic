#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int m, n, s;
	cin >> m >> n >> s;
	vector<string> list(m+1);
	vector<string> winner;
	unordered_map<string, bool> hashMap;
	for (int i = 1; i <= m; i++) {
		cin >> list[i];
		hashMap[list[i]] = false;
	}

	/*if (n > m) {
		cout << "Keep going..." << endl;
		return 0;
	}*/
	for (int i = s; i <= m; i += n) {
		if (!hashMap[list[i]]) {
			winner.push_back(list[i]);
			hashMap[list[i]] = true;
		}
		else {
			for (int j = i + 1; j <= m; j++) {
				if (!hashMap[list[j]]) {
					winner.push_back(list[j]);
					hashMap[list[j]] = true;
					i = j;
					break;
				}
			}
		}
	}

	// cout << endl;
	if (winner.empty())
		cout << "Keep going...";
	else {
		for (string str : winner)
			cout << str << endl;
	}
			 
	system("pause");
	return 0;
}