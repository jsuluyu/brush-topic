#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n;
	cin >> n;
	vector<int> a(n);
	vector<int> b(n);
	vector<int> c;

	for (int i = 0; i < n; i++) {
		cin >> a[i];
		b[i] = a[i];
	}

	sort(a.begin(), a.end());
	int max = 0;
	for (int i = 0; i < n; i++) {
		if (max < b[i]) max = b[i];
		if (a[i] == b[i] && max == a[i]) c.push_back(a[i]);
	}

	cout << c.size() << endl;
	if (c.size() == 0) cout << endl;
	for (int i = 0; i < c.size(); i++) {
		if (i != 0) cout << " ";
		cout << c[i];
	}

	system("pause");
	return 0;
}


