#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
using namespace std;

int main()
{
	double start;
	double end;
	double timeConsume;
	int hour, minute, second;
	cin >> start >> end;
	double temp = (end - start) / 100;
	timeConsume = round(temp);

	// 1分=60秒 1小时=3600秒
	if (timeConsume < 60) {
		printf("00:00:%02d", timeConsume);
		// return 0;
	}
	else if (timeConsume < 3600) {
		minute = timeConsume / 60;
		second = timeConsume - (minute * 60);
		printf("00:%02d:%02d", minute, second);
	}
	else {
		hour = timeConsume / 3600;
		int remainTime = timeConsume - (hour * 3600);
		minute = remainTime / 60;
		second = remainTime - (minute * 60);
		printf("%02d:%02d:%02d", hour, minute, second);
	}

	system("pause");
	return 0;
}
