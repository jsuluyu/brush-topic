#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	string str1, str2;
	string num = "0123456789JQK";
	string ans = "";
	cin >> str1 >> str2;
	reverse(str1.begin(), str1.end());
	reverse(str2.begin(), str2.end());

	int len1 = str1.length();
	int len2 = str2.length();
	if (len1 > len2) {
		for (int i = 0; i < len1 - len2; i++)
			str2 += '0';
	}
	else {
		for (int i = 0; i < len2 - len1; i++)
			str1 += '0';
	}

	int id;
	int len = len1 > len2 ? len1 : len2;
	for (int i = 0; i < len; i++) {
		if ((i+1) % 2 == 0) {
			id = str2[i] - str1[i];
			if (id < 0) id += 10;
			ans += (id + '0');
		}
		else {
			id = (str2[i] + str1[i] - '0' - '0') % 13;
			ans += num[id];
		}
	}

	reverse(ans.begin(), ans.end());
	cout << ans << endl;

	system("pause");
	return 0;
}


