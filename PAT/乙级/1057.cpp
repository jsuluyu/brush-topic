#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	string str;
	string alpha = "abcdefghijklmnopqrstuvwxyz";
	getline(cin, str);

	int sum = 0;
	for (char ch : str) {
		if(isalpha(ch))
			ch = tolower(ch);
		int pos = alpha.find(ch); // 法1，耗时高
		// int pos = ch - 'a'; // 法2
		sum += (pos + 1);
	}

	int count0 = 0, count1 = 0;
	int temp;
	while (sum != 0) {
		temp = sum % 2;
		if (temp == 0) count0++;
		else count1++;
		sum /= 2;
	}

	cout << count0 << " " << count1 << endl;

	system("pause");
	return 0;
}