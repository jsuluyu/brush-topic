#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <algorithm>
using namespace std;

int main()
{
	int n;
	int temp; // 接收每次的输入数字
	int ans[5] = {0};
	int size = 1; // A2负数的标志
	int count[5] = {0}; // A4的个数

	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> temp;
		if (temp % 10 == 0) { // A1的情况
			ans[0] += temp;
			count[0]++;
		}
		if (temp % 5 == 1) { // A2的情况
			ans[1] += (size * temp);
			size *= -1; // 每一轮取反
			count[1]++;
		}
		if (temp % 5 == 2) { // A3的情况
			ans[2]++;
			count[2]++;
		}
		if (temp % 5 == 3) { // A4的情况
			ans[3] += temp;
			count[3]++;
		}
		if (temp % 5 == 4) { // A5的情况
			if (temp > ans[4]) {
				ans[4] = temp;
				count[4]++;
			}
				

		}
	}
	for (int i = 0; i < 5; i++) {
		if (i == 3 && count[3] != 0) {
			double t = 1.0 *ans[3] / count[3];
			printf("%.1f ", t);
			continue;
		}
		if (count[i] == 0)
			cout << "N";
		else
			cout << ans[i];
		cout << (i ==  4 ? "" : " ");
	}

	system("pause");
	return 0;
}
