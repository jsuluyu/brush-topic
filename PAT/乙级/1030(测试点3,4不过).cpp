#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int N;
	long long p;
	int maxCount = 0;
	vector<long long> arr;

	int temp;
	cin >> N >> p;
	for (int i = 0; i < N; i++) {
		cin >> temp;
		arr.push_back(temp);
	}

	sort(arr.begin(), arr.end());

	long long  m, M;
	for (int i = 0; i < N; i++) {
		m = arr[i];
		if (N - i + 1 <= maxCount)
			break;
		for (int j = N - 1; j > i; j--) {
			M = arr[j];
			if (m * p > arr[N - 1])
				break;
			if (m * p >= M) {
				if (j - i + 1 > maxCount) {
					maxCount = j - i + 1;
					break;
				}
			}
		}
	}

	cout << maxCount << endl;

	system("pause");
	return 0;
}
