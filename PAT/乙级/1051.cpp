#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
复数乘法运算法则：
(a+bi)*(c+di) = ac + adi + bci + bdi2 = (ac - bd) + (ad + bc)i
*/
int main()
{
	double r1, p1, r2, p2;
	cin >> r1 >> p1 >> r2 >> p2;
	double a = r1 * cos(p1);
	double b = r1 * sin(p1);
	double c = r2 * cos(p2);
	double d = r2 * sin(p2);

	double A = a * c - b * d; // 结果头部
	double B = a * d + b * c; // 复数头部

	double accuracy = 0.01; // 因为是保留两位小数，若结果A, B小于0.01且为复数，则直接保存为0
	if (fabs(A) < accuracy) A = 0;
	if (fabs(B) < accuracy) B = 0;

	if (B < 0)
		printf("%.2lf%.2lfi", A, B);
	else
		printf("%.2lf+%.2lfi", A, B);

	system("pause");
	return 0;
}