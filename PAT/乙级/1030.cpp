#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int N;
	long long p;
	int maxCount = 0;
	vector<long long> arr;

	int temp;
	cin >> N >> p;
	for (int i = 0; i < N; i++) {
		cin >> temp;
		arr.push_back(temp);
	}

	sort(arr.begin(), arr.end());

	/*
	注意两点:
		1. 数据类型不能用int(10的9次方超出了int范围), 不然最后一个测试点过不去
		2. 在双重循环那需要优化, 不然倒数第二个测试点过不去(时间复杂度过高)
		优化时, 因为之前已经记录了最长的个数, 所以在下一次循环的时候直接从a[i] 到 a[i + result]的地方
	*/
	for (int i = 0; i < N; i++) {
		for (int j = i + maxCount; j < N; j++) {
			if (arr[i] * p >= arr[j]) {
				maxCount = maxCount > j - i + 1 ? maxCount : j - i + 1;
			}
			else
				break;
		}
	}

	cout << maxCount << endl;

	system("pause");
	return 0;
}
