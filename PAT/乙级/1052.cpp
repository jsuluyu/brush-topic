#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	vector<vector<string>> arr;
	for (int i = 0; i < 3; i++) {
		string str;
		getline(cin, str);
		vector<string> temp;
		int j = 0, k = 0;
		while (j < str.length()) {
			if (str[j] == '[') {
				while (k++ < str.length()) {
					if (str[k] == ']') {
						temp.push_back(str.substr(j + 1, k - j - 1));
						break;
					}
				}
			}
			j++;
		}
		arr.push_back(temp);
	}
	
	int n;
	cin >> n;
	for (int i = 0; i < n; i++) {
		int a, b, c, d, e;
		cin >> a >> b >> c >> d >> e;
		if (a > arr[0].size() || b > arr[1].size() || c > arr[2].size() || d > arr[1].size() || e > arr[0].size()
			|| a < 1 || b < 1 || c < 1 || d < 1 || e < 1) {
			cout << "Are you kidding me? @\\/@" << endl;
			continue;
		}
		cout << arr[0][a - 1] << "(" << arr[1][b - 1] << arr[2][c - 1] << arr[1][d - 1] << ")" << arr[0][e - 1] << endl;
	}

	system("pause");
	return 0;
}