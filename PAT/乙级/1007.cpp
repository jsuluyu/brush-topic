#include <iostream>
#include <cstdio>
#include <cmath>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

bool check(int num)
{
	int count = (int)sqrt((double)num);
	for (int i = 2; i <= count; i++) {
		if (num % i == 0)
			return false;
	}
	return true;
}

int main()
{
	int n;
	int ans = 0;

	cin >> n;
	for (int i = 2; i <= n; i++) {
		if (check(i) && check(i+2) && i+2 <= n) {
			// cout << i << " " << i + 2 << endl;
			ans++;
		}
	}

	cout << ans << endl;

	// system("pause");
	return 0;
}
