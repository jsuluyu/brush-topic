#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n;
	cin >> n;
	string str;
	getchar();
	for (int i = 0; i < n; i++) {
		getline(cin, str);
		bool flag = false; // 含有非法字符
		bool flag1 = false; // 数字标志位
		bool flag2 = false; // 单词标志位
		int len = str.length();
		if (len < 6) {
			cout << "Your password is tai duan le." << endl;
			continue;
		}
		for (int j = 0; j < len; j++) {
			if (isdigit(str[j])) flag1 = true;
			else if (isalpha(str[j])) flag2 = true;
			else if (str[j] == '.') continue;
			else {
				flag = true;
				cout << "Your password is tai luan le." << endl;
				break;
			}
		}
		if (flag) continue;
		if (flag1 && flag2)
			cout << "Your password is wan mei." << endl;
		else if (flag1)
			cout << "Your password needs zi mu." << endl;
		else if (flag2)
			cout << "Your password needs shu zi." << endl;
	}

	system("pause");
	return 0;
}