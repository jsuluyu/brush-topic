#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct student
{
	string id;
	int G1 = -1; // 编程分数
	int G2 = -1; // 期中分数
	int G3 = -1; // 期末成绩
	int G; // 最终总成绩
};

bool cmp(student& x, student& y)
{
	if (x.G != y.G) return x.G > y.G;
	else if (x.id != y.id) return x.id < y.id;
}

int main()
{
	int p, m, n;
	cin >> p >> m >> n;
	int score;
	string id;
	unordered_map<string, student> hashMap;
	for (int i = 0; i < p; i++) {
		cin >> id >> score;
		hashMap[id].G1 = score;
		hashMap[id].id = id;
	}
	for (int i = 0; i < m; i++) {
		cin >> id >> score;
		hashMap[id].G2 = score;
		hashMap[id].id = id;
	}
	for (int i = 0; i < n; i++) {
		cin >> id >> score;
		hashMap[id].G3 = score;
		hashMap[id].id = id;
	}
	for (auto it = hashMap.begin(); it != hashMap.end(); it++) {
		if (it->second.G2 > it->second.G3)
			it->second.G = round(it->second.G2 * 0.4 + it->second.G3 * 0.6);
		else
			it->second.G = it->second.G3;
	}
	int len = hashMap.size();
	student temp;
	vector<student> vArr; // 保存所有合法的结果
	for (auto it = hashMap.begin(); it != hashMap.end(); it++) {
		if (it->second.G1 >= 200 && it->second.G >= 60) {
			temp.id = it->first;
			temp.G1 = it->second.G1;
			temp.G2 = it->second.G2;
			temp.G3 = it->second.G3;
			temp.G = it->second.G;
			vArr.push_back(temp);
		}
	}
	sort(vArr.begin(), vArr.end(), cmp);
	for (student stu : vArr) {
		cout << stu.id << " " << stu.G1 << " " << stu.G2 << " " << stu.G3 << " " << stu.G << endl;
	}

	system("pause");
	return 0;
}