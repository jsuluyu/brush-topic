#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n, m; // n为学生个数，m为题目个数
	cin >> n >> m;
	vector<string> qAnswer; // 存放题目答案
	vector<int> counts(m); // 记录每道题答对的人数
	scanf_s("\n", "");//吸收掉换行符
	for (int i = 0; i < m; i++) {
		string temp;
		getline(cin, temp);
		qAnswer.push_back(temp);
	}

	for (int i = 0; i < n; i++) { // 读取学生的答案
		string answer;
		getline(cin, answer);
		int idx = 1; // 跳过回答的第一个括号
		double grade = 0.0; // 记录得分
		for (int k = 0; k < m; k++) { // 共m道题
			if (idx < answer.length()) {
				int pIdx = idx; // 记录指针的初始位置
				int num = answer[idx] - '0'; // 学生回答的当前题答案个数
				int pos = 4; // 指向题库的索引
				int t;
				idx += 2; // 索引下移准备比较选项内容
				pos += 2;
				for (t = 0; t < num; t++) { // 遍历每道题的每道答案
					if (qAnswer[k].find(answer[idx]) == string::npos) // 答案里没有学生的答案，说明选择了错误选项
						break;
					idx += 2; pos += 2;
				}
				if (t == num && answer[pIdx] == qAnswer[k][4]) { // 全部回答正确
					counts[k]++;
					grade += qAnswer[k][0] - '0';
				}
				else if (t == num) { // 未完全回答正确，但是没选错误答案
					grade += (qAnswer[k][0] - '0') / 2.0;
				}
				idx = pIdx + num * 2 + 4; //指向下一题
			}
		}
		printf("%.1lf\n", grade);
	}

	// 寻找错的最多的题目
	int max = 0, maxId;
	for (int i = 0; i < m; i++) {
		if (n - counts[i] > max) {
			max = n - counts[i];
			maxId = i;
		}
	}
	if (max == 0) cout << "Too simple" << endl;
	else {
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < 5; j++) {
				if (n - counts[i] == max) {
					printf("%d %d-%c\n", max, i + 1, j + 'a');
				}
			}
			//if (n - counts[i] == max) {
			//	cout << max;
			//	cout << " " << i + 1;
			//	int num = qAnswer[i][2] - '0'; // 选项的个数
			//	map<char, int> hashMap;
			//	for (char ch = 'a'; ch < 'a' + num; ch++) {
			//		if()
			//		hashMap[ch]++;
			//	}
			}
				
		}
	 
	system("pause");
	return 0;
}