#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

bool isPrime(int num)
{
	for (int i = 2; i <= (int)sqrt(num); i++) {
		if (num % i == 0)
			return false;
	}
	return true;
}

int main()
{
	int n, m;
	cin >> n;
	vector<string> stuId(n);
	unordered_map<string, bool> hashMap;
	for (int i = 0; i < n; i++) {
		cin >> stuId[i];
		hashMap[stuId[i]] = false;
	}
		
	cin >> m; // 要查询的人数
	for (int i = 0; i < m; i++) {
		int j;
		string checkId;
		cin >> checkId;
		/*auto it = find(stuId.begin(), stuId.end(), checkId);
		if (it == stuId.end()) {
			cout << checkId << ": " << "Are you kidding?" << endl;
			continue;
		}*/
		for (j = 0; j < n; j++) {
			if(hashMap[checkId]) {
				printf("%s: Checked\n", checkId.c_str());
				break;
			}
			if (checkId == stuId[j]) {
				if (j == 0) {
					printf("%s: Mystery Award\n", checkId.c_str());
					break;
				}
				else if (isPrime(j+1)) {
					printf("%s: Minion\n", checkId.c_str());
					break;
				}
				else {
					printf("%s: Chocolate\n", checkId.c_str());
					break;
				}
			}
		}
		if(j == n) 
			cout << checkId << ": " << "Are you kidding?" << endl;
		else 
			hashMap[checkId] = true;
	}

	system("pause");
	return 0;
}