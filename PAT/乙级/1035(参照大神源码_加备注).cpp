#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n, a[100], b[100], i, j;
	cin >> n;
	for (int i = 0; i < n; i++)
		cin >> a[i];
	for (int i = 0; i < n; i++)
		cin >> b[i];
	for (i = 0; i < n - 1 && b[i] <= b[i + 1]; i++); // 循环找到从左到右第一个不满足从小到大顺序的序号
	for (j = i + 1; a[j] == b[j] && j < n; j++); // 前半部分是有序的，后半部分是无序的，那么a,b自然相等(因为未进行排序)
	if (j == n) {
		cout << "Insertion Sort" << endl;
		sort(a, a + i + 2);
	}
	else {
		cout << "Merge Sort" << endl;
		int k = 1, flag = 1;
		while (flag) {
			flag = 0;
			for (i = 0; i < n; i++) {
				if (a[i] != b[i])
					flag = 1;
			}
			k = k * 2;
			for (i = 0; i < n / k; i++) // 首轮2个2个一组，然后再4个一组，依次类推
				sort(a + i * k, a + (i + 1) * k);
			for (i = 0; i < n; i++)
				cout << a[i] << " ";
			cout << endl;
			sort(a + n / k * k, a + n); // 这句一定不能忘记，这是在按组排序过后，对末尾未进行排序的数组进行排序
			for (i = 0; i < n; i++)
				cout << a[i] << " ";
			cout << endl << "--------------------------" << endl;
		}
	}
	for (j = 0; j < n; j++) {
		if (j != 0) printf(" ");
		printf("%d", a[j]);
	}

	system("pause");
	return 0;
}

/*
测试用例
10
3 1 2 8 7 5 9 4 0 6
1 3 2 8 5 7 4 9 0 6
*/

/*
分析：先将i指向中间序列中满足从左到右是从小到大顺序的最后一个下标，再将j指向从i+1开始，第一个不满足a[j] == b[j]的下标，
如果j顺利到达了下标n，说明是插入排序，再下一次的序列是sort(a, a+i+2);否则说明是归并排序。归并排序就别考虑中间序列了，
直接对原来的序列进行模拟归并时候的归并过程，i从0到n/k，每次一段段得sort(a + i * k, a + (i + 1) * k);
最后别忘记还有最后剩余部分的sort(a + n / k * k, a + n);这样是一次归并的过程。直到有一次发现a的顺序和b的顺序相同，
则再归并一次，然后退出循环～

注意：一开始第三个测试点一直不过，天真的我以为可以模拟一遍归并的过程然后在过程中判断下一步是什么。。
然而真正的归并算法它是一个递归过程。。也就是先排左边一半，把左边的完全排列成正确的顺序之后，再排右边一半的。。
而不是左右两边一起排列的。。后来改了自己的归并部分判断的代码就过了。。。｡◕‿◕｡
*/
