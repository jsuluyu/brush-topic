#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n, m;
	cin >> n;
	vector<int> husband(n), wife(n);
	set<int> hashSet;
	for (int i = 0; i < n; i++) {
		cin >> husband[i] >> wife[i];
	}
	cin >> m;
	vector<int> guest(m);
	for (int i = 0; i < m; i++)
		cin >> guest[i];

	for (int i = 0; i < m; i++) {
		int temp = -1;
		bool flag = false;
		for (int j = 0; j < n; j++) {
			if (guest[i] == husband[j]) {
				temp = wife[j];
				break;
			}
			if (guest[i] == wife[j]) {
				temp = husband[j];
				break;
			}
		}
		if (temp == -1) hashSet.insert(guest[i]);
		else {
			for (int j = 0; j < m; j++) {
				if (guest[j] == temp) {
					flag = true;
					break;
				}
			}
		}
		if (!flag) hashSet.insert(guest[i]);
	}

	bool flag = false;
	set<int>::iterator it;
	cout << hashSet.size() << endl;
	for(it = hashSet.begin(); it != hashSet.end(); it++) {
		if (flag) cout << " ";
		cout << *it;
		flag = true;
	}

	system("pause");
	return 0;
}