#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int change(int x)
{
	string str = "";
	while (x) {
		int t = x % 10;
		x /= 10;
		str += to_string(t);
	}
	//reverse(str.begin(), str.end());
	return stoi(str);
}

int main()
{
	int m, x, y;
	cin >> m >> x >> y;
	int t1 = 100, t2 = 0;
	double t3; // 注意这里的丙是可以为小数的，但是甲乙必须为整数
	bool flag = false;
	for (int i = 99; i >= 10; i--) {  // 甲
		t2 = change(i); // 乙
		t3 = 1.0 * t2 / y;
		if (abs(i - t2) == x * t3) {
			flag = true;
			t1 = i;
			break;
		}
	}
	
	if (flag) {
		vector<double> vArr;
		vArr.push_back(t1);
		vArr.push_back(t2);
		vArr.push_back(t3);
		// for (int i = 0; i < 3; i++)
		//  	cout << vArr[i] << " ";
		cout << vArr[0] << " ";
		for (int i = 0; i < 3; i++) {
			if (m > vArr[i]) cout << "Gai";
			else if (m < vArr[i]) cout << "Cong";
			else cout << "Ping";
			if (i != 2) cout << " ";
		}
	}
	else
		cout << "No Solution";
	
	system("pause");
	return 0;
}