#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct wineBox
{
	int sayNum1;
	int doNum1;
	int sayNum2;
	int doNum2;
};

int main()
{
	int n;
	cin >> n;
	vector<wineBox> wb(n);
	for (int i = 0; i < n; i++) {
		cin >> wb[i].sayNum1 >> wb[i].doNum1 >> wb[i].sayNum2 >> wb[i].doNum2;
	}

	int count1 = 0;
	int count2 = 0;
	for (int i = 0; i < n; i++) {
		int temp = wb[i].sayNum1 + wb[i].sayNum2;
		if (temp == wb[i].doNum1 && temp != wb[i].doNum2) count2++;
		if (temp == wb[i].doNum2 && temp != wb[i].doNum1) count1++;
	}

	cout << count1 << " " << count2 << endl;

	system("pause");
	return 0;
}


