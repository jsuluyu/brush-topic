#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	string str1;
	string str2;
	unordered_set<char> us;
	unordered_map<char, bool> uMap(128);
	
	cin >> str1;
	cin >> str2;
	for (char c : str2) {
		char ch = toupper(c);
		uMap[ch] = true;
	}
	for (char c : str1) {
		char ch = toupper(c);
		if (!uMap[ch]) {
			cout << ch;
			uMap[ch] = true;
		}
			
	}

	/*for (auto it = uMap.begin(); it != uMap.end(); it++) {
		
	}*/
	/*int len = str1.length() > str2.length() ? str1.length() : str2.length();
	int idx1 = 0, idx2 = 0;
	while(idx1 < len && idx2 < len) {
		if (str1[idx1] != str2[idx2]) {
			us.insert(toupper(str1[idx1]));
			idx1++;
		}
		else {
			idx1++;
			idx2++;
		}
	}

	for (auto it = us.begin(); it != us.end(); it++)
		cout << *it;*/

	system("pause");
	return 0;
}
