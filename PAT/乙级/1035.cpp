#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n;
	int a[101], b[101];
	cin >> n;
	for (int i = 0; i < n; i++)
		cin >> a[i];
	for (int i = 0; i < n; i++)
		cin >> b[i];

	// 寻找第一个不是从小到大的序号，即未开始排序的索引
	int idx1, idx2;
	for (idx1 = 0; idx1 < n - 1 && b[idx1] <= b[idx1 + 1]; idx1++);
	// 判断后半部分是不是完全未动，若是则是插入排序
	for (idx2 = idx1 + 1; idx2 < n && a[idx2] == b[idx2]; idx2++);

	if (idx2 == n) {
		cout << "Insertion Sort" << endl;
		sort(a, a + idx1 + 2);
	}
	else {
		cout << "Merge Sort" << endl;
		int i, k = 1, flag = 1;
		while (flag) {
			flag = 0;
			for (i = 0; i < n; i++) {
				if (a[i] != b[i]) {
					flag = 1;
					break;
				}
			}
			k = k * 2;
			for (i = 0; i < n / k; i++) // 先按组进行组内排序
				sort(a + i * k, a + (i + 1)*k);
			sort(a + n / k * k, a + n); // 由于会除不尽，导致后面部分无法排序，这里对后面未进行排序的进行排序
		}	
	}

	for (int i = 0; i < n; i++) {
		if (i != 0) cout << " ";
		cout << a[i];
	}	

	system("pause");
	return 0;
}

/*
测试用例
10
3 1 2 8 7 5 9 4 0 6
1 3 2 8 5 7 4 9 0 6
*/

/*
分析：先将i指向中间序列中满足从左到右是从小到大顺序的最后一个下标，再将j指向从i+1开始，第一个不满足a[j] == b[j]的下标，
如果j顺利到达了下标n，说明是插入排序，再下一次的序列是sort(a, a+i+2);否则说明是归并排序。归并排序就别考虑中间序列了，
直接对原来的序列进行模拟归并时候的归并过程，i从0到n/k，每次一段段得sort(a + i * k, a + (i + 1) * k);
最后别忘记还有最后剩余部分的sort(a + n / k * k, a + n);这样是一次归并的过程。直到有一次发现a的顺序和b的顺序相同，
则再归并一次，然后退出循环～

注意：一开始第三个测试点一直不过，天真的我以为可以模拟一遍归并的过程然后在过程中判断下一步是什么。。
然而真正的归并算法它是一个递归过程。。也就是先排左边一半，把左边的完全排列成正确的顺序之后，再排右边一半的。。
而不是左右两边一起排列的。。后来改了自己的归并部分判断的代码就过了。。。｡◕‿◕｡
*/
