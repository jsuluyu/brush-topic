#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n, m;
	scanf_s("%d %d", &n, &m);
	vector<int> v;
	int count = n;
	while (count--) {
		int sum = 0;
		int temp, temp2;
		cin >> temp2;
		for (int i = 0; i < n-1; i++) {
			cin >> temp;
			if (temp >= 0 && temp <= m)
				v.push_back(temp);
		}
		sort(v.begin(), v.end());
		for (int i = 1; i < v.size() - 1; i++) {
			sum += v[i];
		}
		double sum1 = sum / (v.size() - 2);
		sum1 = (sum1 + temp2) / 2;
		printf("%d\n", (int)round(sum1));
		v.clear();
	}
	
	system("pause");
	return 0;
}