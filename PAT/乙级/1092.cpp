#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct record
{
	int sum = 0;
	int id;
};

bool cmp(record& x, record& y)
{
	if (x.sum != y.sum) return x.sum > y.sum;
	else return x.id < y.id;
}

int main()
{
	int n, m;
	cin >> n >> m;
	vector<record> vArr(n);
	int temp;
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			cin >> temp;
			vArr[j].id = j + 1;
			vArr[j].sum += temp;
		}
	}
	sort(vArr.begin(), vArr.end(), cmp); // 先按sum从大到小排序，若sum相等则按照序号id从小到大排序
	int max = vArr[0].sum;
	cout << max << endl;
	cout << vArr[0].id;
	for (int i = 1; i < n; i++) {
		if (vArr[i].sum == max)
			cout << " " << vArr[i].id;
		else
			break;
	}
	
	system("pause");
	return 0;
}