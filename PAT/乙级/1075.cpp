#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct linkNode
{
	int val;
	int next;
};

int main()
{
	int n, k;
	int head;
	cin >> head;
	cin >> n >> k;
	vector<linkNode> link(100002);
	vector<int> v[3];
	int address, val, next;
	for (int i = 0; i < n; i++) {
		cin >> address >> val >> next;
		link[address].val = val;
		link[address].next = next;
	}

	int pId = head;
	while (pId != -1) {
		if (link[pId].val < 0)
			v[0].push_back(pId);
		else if (link[pId].val <= k)
			v[1].push_back(pId);
		else
			v[2].push_back(pId);
		pId = link[pId].next;
	}

	int flag = 0;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < v[i].size(); j++) {
			if (flag == 0) { // 输出第一个元素
				printf("%05d %d ", v[i][j], link[v[i][j]].val);
				flag = 1;
			}
			else { // 元素的next指针为下一个元素的首地址
				printf("%05d\n%05d %d ", v[i][j], v[i][j], link[v[i][j]].val);
			}
		}
	}
	printf("-1\n");

	system("pause");
	return 0;
}