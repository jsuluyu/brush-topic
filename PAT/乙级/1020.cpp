#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
using namespace std;

typedef struct goods 
{
	double weight; // 月饼的总数
	double money; // 月饼的总价
	double price; // 月饼的单价
};

bool cmp(goods x, goods y)
{
	if (x.price > y.price)
		return true;
	return false;
}

// 贪心算法
int main()
{
	int N, D;
	double maxIncome = 0.0;
	cin >> N >> D;
	vector<goods> G(N);

	for (int i = 0; i < N; i++)
		cin >> G[i].weight;
	for (int i = 0; i < N; i++)
		cin >> G[i].money;
	for (int i = 0; i < N; i++)
		G[i].price = 1.0 * G[i].money / G[i].weight;

	sort(G.begin(), G.end(), cmp); // 从大到小排序,sort是原地排序

	for (int i = 0; i < N; i++) {
		if (D > G[i].weight) {
			maxIncome += G[i].money;
			D -= G[i].weight;
		} 
		else {
			maxIncome += 1.0 * D * G[i].price;
			break;
		}
	}

	printf("%.2f", maxIncome);

	system("pause");
	return 0;
}
