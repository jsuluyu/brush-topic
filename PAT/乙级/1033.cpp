#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	string str1, str2;
	string ans = "";

	getline(cin, str1);
	getline(cin, str2);

	// 寻找‘+’
	bool flag = false;
	size_t p = str1.find('+');
	if (p != string::npos)
		flag = true;

	size_t position; // 因为string的find()若找不到则返回string::npos,其类型为size_t(unsigned int)
	for (char ch : str2) {
		if (flag == true && ch >= 'A' && ch <= 'Z') // 先判断是否有+，是否大写，若大写且有+，则直接跳过此字符
			continue;
		position = str1.find(toupper(ch));
		if (position != string::npos) {
			continue;
		}	
		ans += ch;
	}

	cout << ans;

	system("pause");
	return 0;
}
