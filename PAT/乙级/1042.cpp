#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	string str;
	getline(cin, str);
	int a[27] = {0};

	for (char ch : str) {
		if (isalpha(ch)) {
			int id = tolower(ch) - 'a';
			a[id]++;
		}
	}

	int maxId;
	int maxValue = 0;
	for (int i = 0; i < 27; i++) {
		if (a[i] > maxValue) {
			maxValue = a[i];
			maxId = i;
		}
	}

	cout << char('a' + maxId) << " " << maxValue;

	system("pause");
	return 0;
}


