#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
分析：
使用辗转相除法gcd计算a和b的最大公约数，因为要列出n1/m1和n2/m2之间的最简分数，
但是n1/m1不一定小于n2/m2，所以如果n1 * m2 > n2 * m1，说明n1/m1比n2/m2大，
则调换n1和n2、m1和m2的位置～假设所求的分数分母为k、分子num，先令num=1，
当n1 * k >= m1 * num时，num不断++，直到num符合n1/m1 < num/k为止～然后在n1/m1和n2/m2之间找符合条件的num的值，
用gcd(num, k)是否等于1判断num和k是否有最大公约数，如果等于1表示没有最大公约数，就输出num/k，然后num不断++直到退出循环～
*/

int gcd(int x, int y)
{
	if (y == 0)
		return x;
	else
		return gcd(y, x % y);
}

int main()
{
	int n1, m1, n2, m2, k;
	scanf_s("%d/%d %d/%d %d", &n1, &m1, &n2, &m2, &k);
	if (n1 * m2 > n2 * m1) {
		swap(n1, n2);
		swap(m1, m2);
	}
	int num = 1;
	bool flag = false;
	while (n1 * k >= num * m1) num++;
	while (num * m2 < n2 * k) {
		if (gcd(num, k) == 1) {
			if (flag) printf(" ");
			printf("%d/%d", num, k);
			flag = true;
		}	
		num++;
	}
	
	system("pause");
	return 0;
}