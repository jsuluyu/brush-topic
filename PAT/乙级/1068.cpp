#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n, m, tol;
	map<int, int> hashMap;
	cin >> m >> n >> tol;
	int pixel[100][100] = {0};
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++) {
			cin >> pixel[i][j];
			hashMap[pixel[i][j]]++;
		}
	int flag = 0;
	int x, y, num;
	for(int i = 0; i < n; i++)
		for (int j = 0; j < m; j++) {
			int num1 = abs(pixel[i][j] - pixel[i - 1][j - 1]); // 左上
			int num2 = abs(pixel[i][j] - pixel[i - 1][j]); // 上
			int num3 = abs(pixel[i][j] - pixel[i - 1][j + 1]); // 右上
			int num4 = abs(pixel[i][j] - pixel[i][j - 1]); // 左
			int num5 = abs(pixel[i][j] - pixel[i][j + 1]); // 右
			int num6 = abs(pixel[i][j] - pixel[i + 1][j - 1]); // 左下
			int num7 = abs(pixel[i][j] - pixel[i + 1][j]); // 下
			int num8 = abs(pixel[i][j] - pixel[i + 1][j + 1]); // 右下
			if (hashMap[pixel[i][j]] == 1 && num1 > tol && num2 > tol && num3 > tol && num4 > tol && num5 > tol && num6 > tol && num7 > tol && num8 > tol) {
				flag++;
				if (flag == 1) {
					x = i; y = j; 
					num = pixel[i][j];
				}
			}
		}
	if (flag == 1)
		printf("(%d, %d): %d\n", y+1, x+1, num);
	else if (flag == 0)
		printf("Not Exist\n");
	else
		printf("Not Unique\n");
			 
	system("pause");
	return 0;
}