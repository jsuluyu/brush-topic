#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n;
	int x, y;
	double max = 0.0;
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> x >> y;
		double num = (double)(pow(x, 2) + pow(y, 2));
		if (num > max) max = num;
	}
	printf("%.2lf", sqrt(max));
	
	system("pause");
	return 0;
}