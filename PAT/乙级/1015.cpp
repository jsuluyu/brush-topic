#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
using namespace std;

typedef struct student {
	string stuId;
	int dScore; // 德分
	int cScore; // 才分
};

void stuSort(vector<vector<student>>& arr) // 地址传递
{
	for (int idx = 0; idx < 4; idx++) {
		int len = arr[idx].size();
		for (int i = 0; i < len-1; i++) {
			for (int j = 0; j < len - i - 1; j++) {
				int scoreI = arr[idx][j].dScore + arr[idx][j].cScore;
				int socreJ = arr[idx][j+1].dScore + arr[idx][j+1].cScore;
				if (scoreI <= socreJ) {
					student temp = arr[idx][j];
					arr[idx][j] = arr[idx][j+1];
					arr[idx][j+1] = temp;
				}
			}
		}
	}	
}

int main()
{
	int N, L, H;
	vector<student> stu;

	cin >> N >> L >> H;
	student temp;
	for (int i = 0; i < N; i++) {
		cin >> temp.stuId >> temp.dScore >> temp.cScore;
		stu.push_back(temp);
	}

	vector<vector<student>> stuTemp(4);
	for (int i = 0; i < N; i++) {
		if (stu[i].dScore >= H && stu[i].cScore >= H) // 第一类考生
			stuTemp[0].push_back(stu[i]);
		else if (stu[i].cScore >= L && stu[i].cScore < H && stu[i].dScore >= H) // 第二类考生
			stuTemp[1].push_back(stu[i]);
		else if (stu[i].dScore < H && stu[i].cScore < H && stu[i].dScore >= stu[i].cScore && stu[i].cScore >= L && stu[i].dScore >= L) // 第三类考生
			stuTemp[2].push_back(stu[i]);
		else if (stu[i].cScore >= L && stu[i].dScore >= L) // 最后一类学生
			stuTemp[3].push_back(stu[i]);	
	}

	stuSort(stuTemp); // 排序

	// cout << endl;
	for (int i = 0; i < 4; i++) {
		int len = stuTemp[i].size();
		for (int j = 0; j < len; j++) {
			cout << stuTemp[i][j].stuId << " " << stuTemp[i][j].dScore << " " << stuTemp[i][j].cScore << endl;
		}
		// cout << endl;
	}
		 

	system("pause");
	return 0;
}
/*
3485djGkxh4hhG0
2984akGfkkkkgg0dsb
0&hgsfdk
0&Hyscvnm
*/