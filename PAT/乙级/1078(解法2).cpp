#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	char command;
	cin >> command;
	string str;
	getchar(); // 接收回车
	if (command == 'C') {
		getline(cin, str);
		int len = str.length();
		string ans = "";
		int j;
		for (int i = 0; i < len; i = j) {
			int num = 0;
			for (j = i; j < len && str[j] == str[i]; j++); // 找相同的字符个数
			num = j - i;
			if (num == 1) ans += str[i];
			else ans += to_string(num) + str[i];
		}
		cout << ans << endl;
	}
	else if (command == 'D') {
		getline(cin, str);
		int len = str.length();
		int j;
		string ans = "";
		for (int i = 0; i < len; i++) {
			if (isdigit(str[i])) {
				string count = "";
				for (j = i; j < len; j++) {
					if (isdigit(str[j])) count += str[j];
					else break;
				}
				i = j;
				int num = stoi(count);
				char ch = str[i];
				for (j = 0; j < num; j++)
					ans += ch;
				//i++; // 跳过 数字+字符 这个整体
			}
			else {
				ans += str[i];
			}
		}
		cout << ans << endl;
	}
	else;

	system("pause");
	return 0;
}