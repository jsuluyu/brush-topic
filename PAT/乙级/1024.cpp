#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
using namespace std;

/*
测试用例
+1.23400E-03
+1.23400E+03
-1.23400E-03
-1.23400E+03
-1.234E+03  //测试点3，4
*/

int change(string& temp)
{
	int idx = 0;
	string num = "";
	while (temp[idx] == '-' || temp[idx] == '+' || temp[idx] == '0')
		idx++;
	for (int i = idx; i < temp.length(); i++)
		num += temp[i];
	if (num.length() == 1)
		return 0;
	return stoi(num);
}

int main()
{
	string ans = "";
	string str;
	string temp;

	cin >> str;
	int len = str.length();
	int idx; // 记录E的位置
	for (int i = 0; i < len; i++) { // 获取指数部分
		if (str[i] == 'E') {
			idx = i;
			while (i++ < len)
				temp += str[i];
			break;
		}
	}

	if (str[0] == '-') // 负数前面加负号
		ans += '-';

	if (temp[0] == '-') {
		int num = change(temp); // 将指数转换为数字
		if (num != 0) {
			for (int i = 0; i < num; i++) {
				if (i == 0)
					ans += "0.";
				else
					ans += "0";
			}
			for (int i = 1; i < idx; i++) {
				if (str[i] == '.')
					continue;
				ans += str[i];
			}
		}
		else {
			for (int i = 1; i < idx; i++) {
				ans += str[i];
			}
		}
	}
	else {
		int num = change(temp); // 将指数转换为数字
		bool flag = false; // 找到小数点后，num-- 
		if (num != 0) {
			for (int i = 1; i < idx; i++) { // 先把数字部分保存
				if (str[i] == '.') {
					flag = true;
					continue;
				}	
				ans += str[i];
				if (flag) num--;
				if (num == 0 && str[i+1] != 'E') {
					ans += '.';
				}
			}
			for (int i = 0; i < num; i++) {
				ans += "0";
			}
		}
		else {
			for (int i = 1; i < idx; i++) {
				ans += str[i];
			}
		}
	}

	for (int i = 0; i < ans.length(); i++)
		cout << ans[i];
	cout << endl;

	system("pause");
	return 0;
}
