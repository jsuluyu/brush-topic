#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

string divideStr(string str)
{
	int id = 0;

	bool size = false;
	if (str[id] == '-') {
		size = true;
		id++;
	}

	if (str[id] == '0') return "0";

	string num1 = "";
	while (str[id] != '/') // 获取分母
		num1 += str[id++];
	id++;
	string num2 = "";
	while (id < str.length()) // 获取分子
		num2 += str[id++];
	int x = stoi(num1);
	int y = stoi(num2);

	string ans = "";
	if (size) {
		ans += '(';
		ans += '-';
	} 
	int temp1 = x / y;
	int temp2 = x % y;
	if (temp1 > 0) {
		ans += to_string(temp1);
	}
	if(temp1 > 0 && temp2 != 0) ans += " ";
	if (temp2 != 0) {
		ans += to_string(temp2);
		ans += "/";
		ans += to_string(y);
	}
	if (size) ans += ')';
	
	return ans;
}

int main()
{
	string str1, str2;
	
	cin >> str1 >> str2;

	string result1 = divideStr(str1);
	string result2 = divideStr(str2);

	cout << result1 << endl;
	cout << result2 << endl;



	system("pause");
	return 0;
}
