#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct student
{
	string id;
	int proScore;
	int midScore;
	int finScore;
	double score;
};

bool cmp(student& x, student& y)
{
	if (x.score > y.score)
		return true;
	else if (x.score == y.score) {
		return x.id > y.id ? false : true;
	}
	else
		return false;
}

int main()
{
	int p, m, n;
	cin >> p >> m >> n;
	vector<student> vStu;
	student temp;
	for (int i = 0; i < p; i++) {
		cin >> temp.id >> temp.proScore;
		if (temp.proScore >= 200) {
			temp.midScore = -1;
			temp.finScore = -1;
			vStu.push_back(temp);
		}
	}
	int len = vStu.size();
	for (int i = 0; i < m; i++) {
		int flag = -1;
		cin >> temp.id >> temp.midScore;
		for (int j = 0; j < len; j++) {
			if (temp.id == vStu[j].id) {
				flag = j;
				break;
			}	
		}
		if (flag != -1) vStu[flag].midScore = temp.midScore;
	}
	for (int i = 0; i < n; i++) {
		int flag = -1;
		cin >> temp.id >> temp.finScore;
		for (int j = 0; j < len; j++) {
			if (temp.id == vStu[j].id) {
				flag = j;
				break;
			}
		}
		if (flag != -1) vStu[flag].finScore = temp.finScore;
	}
	vector<student> vAns;
	for (int i = 0; i < len; i++) {
		if (vStu[i].midScore > vStu[i].finScore)
			vStu[i].score = round(vStu[i].midScore * 0.4 + vStu[i].finScore * 0.6);
		else
			vStu[i].score = vStu[i].finScore;
		if (vStu[i].score >= 60)
			// printf("%s %d %d %d %.0f\n", vStu[i].id.c_str(), vStu[i].proScore, vStu[i].midScore, vStu[i].finScore, vStu[i].score);
			vAns.push_back(vStu[i]);
	}
	sort(vAns.begin(), vAns.end(), cmp);
	for (int i = 0; i < vAns.size(); i++) {
		printf("%s %d %d %d %.0f\n", vAns[i].id.c_str(), vAns[i].proScore, vAns[i].midScore, vAns[i].finScore, vAns[i].score);
	}

	system("pause");
	return 0;
}