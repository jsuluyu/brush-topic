#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

string a[13] = { "tret","jan", "feb", "mar", "apr", "may", "jun", "jly", "aug", "sep", "oct", "nov", "dec" };
string b[13] = { "","tam", "hel", "maa", "huh", "tou", "kes", "hei", "elo", "syy", "lok", "mer", "jou" };

void numToWorld(string str)
{
	int num = stoi(str);
	cout << b[num / 13]; // 有进位就输出高位，没进位就输出“”
	if (num % 13 && num / 13) cout << " " << a[num % 13]; // 如果有进位，那么输出低位
	else if (num % 13) cout << a[num % 13]; // 没进位就直接输出低位
	else if (num == 0) cout << "tret"; // 0对应输出tret
	//cout << endl;
}

void worldToNum(string str)
{
	if (str.size() == 3) { // 只有1位，要分有没有高位的情况。因为若低位是0，那么就不输出0，此时仅输出高位
		for (int i = 0; i < 13; i++) { //遍历高位
			if (str == b[i]) {
				cout << i * 13;
				break;
			}
		}
		for (int i = 0; i < 13; i++) { //遍历低位
			if (str == a[i]) {
				cout << i;
			}
		}
	}
	else if (str.size() == 4) cout << 0;
	else {
		int num1, num2;
		string subStr1 = str.substr(0, 3);
		string subStr2 = str.substr(4, 3);
		for (int i = 0; i < 13; i++) { // 遍历高位
			if (subStr1 == b[i]) num1 = i * 13;
			if (subStr2 == a[i]) num2 = i;
		}
		cout << num1 + num2;
	}
	//cout << endl;
}

int main()
{
	int n;
	string str;
	cin >> n;
	getchar(); // 接收回车，防止回车也被当做第一个字符
	for (int i = 0; i < n; i++) {
		getline(cin, str);
		if (str[0] >= '0' && str[0] <= '9') numToWorld(str); // 数字转火星文
		else worldToNum(str); // 火星文转数字
	}

	system("pause");
	return 0;
}


