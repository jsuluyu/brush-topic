#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int c; // 系数
	int e; // 指数
	bool flag = false; // 标记第一次不留空格第一次
	while (1) {
		cin >> c >> e;
		if ((c == 0 && e == 0)) {
			cout << "0 0";
			break;
		}
		/*else if (e == 0) {
			break;
		}*/
		else {
			c *= e;
			--e;
			if (c != 0) {
				cout << (flag ? " " : "") << c << " " << e;
				flag = true;
			}
			else {
				if (!flag) cout << "0 0" << endl;
				break;
			}
		}	
	}

	system("pause");
	return 0;
}