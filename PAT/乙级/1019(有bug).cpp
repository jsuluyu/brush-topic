#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
using namespace std;

bool cmp(char x, char y)
{
	if (x > y)
		return true;
	return false;
}

int main()
{
	string str1;
	string str2;
	string temp = "";
	cin >> str1;
	str2 = str1;
	//sort(str1.begin(), str1.end(), cmp);
	//cout << str1 << endl;

	while (temp != "6174") {
		sort(str1.begin(), str1.end(), cmp); // 递减排序
		sort(str2.begin(), str2.end()); // 递减排序
	
		if (str1 == str2) {
			cout << str1 << " - " << str2 << " = " << "0000" << endl;
			break;
		}

		int t1 = atoi(str1.c_str()); // 字符串转数字
		int t2 = atoi(str2.c_str());
		int t3 = t1 - t2;
		temp = to_string(t3);
		
		printf("%04d - %04d = %04d\n", t1, t2, t3);

		str1 = str2 = temp;
		while (str1.size() < 4) {
			str1 = str1 + '0';
			str2 = str2 + '0';
		}		
	}

	system("pause");
	return 0;
}
