#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

//bool check(string str)
//{
//	double sum = 0.0;
//	int size = 1;
//	int i = 0;
//	int dotCount = 0;
//	if (str[0] == '-') { size = -1; i++; }
//	for (; i < str.length(); i++) {
//		if (dotCount > 1) return false;
//		if (str[i] == '.') { dotCount++;}
//		else if (str[i] < '0' || str[i] > '9') return false;
//	}
//	return true;
//}
//
//double change(string str)
//{
//	return stod(str);
//}
//
//bool checkDot(double num)
//{
//	int i;
//	num = num - (int)num;
//	for (i = 0; i < 100; i++) {
//		num *= 10;
//		if (num - (int)num == 0)
//			break;
//	}
//	if (i + 1 > 3) return false;
//	else return true;
//}


bool mydigit(char a)
{
	return !(isdigit(a) || a == '.');
}

int check(string s)
{
	int point = count(s.begin(), s.end(), '.');	// 记录小数点
	int m = 0;		//记录小数位数
	if (s.find('.') != string::npos) m = s.length() - s.find('.') - 1;
	if (!(s[0] == '-'&&s.length() > 1) && !isdigit(s[0])) // 第一位必为数字或者负号（不能只有负号）
		return 0;
	if (m <= 2 && abs(stof(s)) <= 1000) // 必须保证在题目范围内
		return 1;
	return 0;
}


int main()
{
	int n;
	int legalCount = 0;
	double sum = 0;
	cin >> n;
	string temp;
	while (n--) {
		cin >> temp;
		if (check(temp)) {
			legalCount++;
			sum += stof(temp);
		}
		else
			printf("ERROR: %s is not a legal number\n", temp.c_str());
	}

	cout << "The average of " << legalCount;
	if (legalCount == 1)						//看清题意
		cout << " number is ";
	else
		cout << " numbers is ";
	if (legalCount)
		printf("%0.2lf", (double)sum / legalCount);
	else
		cout << "Undefined";
	
	system("pause");
	return 0;
}