#include <iostream>
#include <cstdio>

using namespace std;

int main()
{
    int n;
    int num = 0;
    cin>>n;
    while(n != 1) {
        if(n % 2 == 0) // 偶数
            n = n / 2;
        else
            n = (3 * n + 1) / 2;
        num++;
    }
    cout<<num<<endl;
    // system("pause");
    return 0;
}