#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	int n, m;
	int arr[101];
	cin >> n >> m;
	for (int i = 0; i < n; i++)
		cin >> arr[i];
    
    // 改变m的值
	if (m > n)
		m = m - n;
	
	int start = n - m; // 最后要移动的位置
	int p = 0; // 每次开始移动的位置
	for (; start < n; start++) {
		int temp = arr[start];
		for (int i = start-1; i >= p; i--) {
			arr[i + 1] = arr[i];
		}
		arr[p] = temp;
		p++;
	}

	for (int i = 0; i < n; i++) {
		if (i == n - 1)
			cout << arr[i];
		else
			cout << arr[i] << " ";
	}

	//system("pause");
	return 0;
}
