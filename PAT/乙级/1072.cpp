#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n, m;
	cin >> n >> m;
	vector<int> proItems(m);
	for (int i = 0; i < m; i++)
		cin >> proItems[i];
	int stuTotal = 0, itemsTotal = 0;
	while (n--) {
		bool flag, flag1 = true;
		string name;
		int num, temp;
		cin >> name;
		cin >> num;
		for (int i = 0; i < num; i++) {
			flag = false;
			cin >> temp;
			for (int j = 0; j < m; j++) {
				if (proItems[j] == temp) {
					flag = true;
					itemsTotal++;
					break;
				}
			}
			if (flag && flag1) {
				stuTotal++;
				printf("%s: %04d", name.c_str(), temp);
				flag1 = false; // 只在第一次输出名字
			}
			else if (flag) printf(" %04d", temp);
		}
		if (!flag1)
			printf("\n");
	}

	printf("%d %d\n", stuTotal, itemsTotal);
			 
	system("pause");
	return 0;
}