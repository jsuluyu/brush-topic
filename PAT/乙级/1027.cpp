#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
using namespace std;

int main()
{
	int NUM, remain;
	char ch;
	cin >> NUM;
	cin >> ch;

	if (NUM < 7) {
		cout << ch<< endl;
		cout << NUM - 1;
	}
	else {
		int sum = 1;
		int level = 1;
		int maxCh = 0;
		for (int i = 3; i < NUM; i += 2) {
			if (i > NUM - sum)
				break;
			level++;
			sum = sum + i * 2;
			maxCh = i;
		}
		remain = NUM - sum;

		int black = 0;
		for (int i = maxCh; i >= 1; i-=2) {
			for (int k = 0; k < black; k++)
				cout << " ";
			for(int j=0; j<i; j++) // 输出符号
				cout << ch;
			cout << endl;
			black++;
		}

		black = black - 2;
		for (int i = 3; i <= maxCh; i += 2) {
			for (int k = 0; k < black; k++)
				cout << " ";
			for (int j = 0; j < i; j++) // 输出符号
				cout << ch;
			cout << endl;
			black--;
		}
		cout << remain;
	}
	

	system("pause");
	return 0;
}
