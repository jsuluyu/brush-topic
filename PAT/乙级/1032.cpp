#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct player
{
	int id;
	double score;
};

int main()
{
	int N;

	cin >> N;
	vector<player> arr(N);
	int count = 0;
	for (int i = 0; i < N; i++) {
		cin >> arr[i].id >> arr[i].score;
		if (count < arr[i].id)
			count = arr[i].id;
	}

	int maxId;
	double maxScore = 0;
	vector<double> sumScore(count+1, 0);
	for (int i = 0; i < N; i++) {
		sumScore[arr[i].id] += arr[i].score;
		if (sumScore[arr[i].id] > maxScore) {
			maxScore = sumScore[arr[i].id];
			maxId = arr[i].id;
		}	
	}

	printf("%d %.0f\n", maxId, maxScore);

	system("pause");
	return 0;
}
