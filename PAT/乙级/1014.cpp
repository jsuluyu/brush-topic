#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <algorithm>
#include <cmath>
using namespace std;

int main()
{
	string s1, s2, s3, s4;
	string data[7] = { "MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN" };
	char week, hour;
	int minute;
	bool flag = true; // 找到星期的标志位 

	cin >> s1 >> s2 >> s3 >> s4;
	int len = s1.size() < s2.size() ? s1.size() : s2.size(); // 选取小的长度
	
	for (int i = 0; i < len; i++) {
		if (s1[i] == s2[i] && flag && s1[i] >= 'A' && s1[i] <= 'G') {
			week = s1[i];
			flag = false; // 标识星期已经找到
			continue;
		}
		if (s1[i] == s2[i] && !flag && ((s1[i] >= 'A' && s1[i] <= 'N') || (s1[i] >= '0' && s1[i] <= '9'))) {
			hour = s1[i];
			break;
		}
	}

	int len2 = s3.size() < s4.size() ? s3.size() : s4.size(); // 选取小的长度
	for (int i = 0; i < len2; i++) {
		if (s3[i] == s4[i] && isalpha(s3[i])) {
			minute = i;
			break;
		}
	}

	// cout << week << hour << minute << endl;

	int idx;
	idx = week - 'A'; // 代表周几
	cout << data[idx] << " ";

	if (hour >= '0' && hour <= '9') {
		cout << '0' << hour << ':';
	}
	else {
		cout << hour - 'A' + 10 << ':';
	}
	printf("%02d", minute);

	system("pause");
	return 0;
}
/*
3485djGkxh4hhG0
2984akGfkkkkgg0dsb
0&hgsfdk
0&Hyscvnm
*/