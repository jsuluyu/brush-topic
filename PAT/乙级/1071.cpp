#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int total, k;
	cin >> total >> k;
	int n1, n2, b, t;
	for (int i = 0; i < k; i++) {
		cin >> n1 >> b >> t >> n2;
		if (t > total) {
			printf("Not enough tokens.  Total = %d.\n", total);
			continue;
		}	
		if (n1 > n2 && b == 1) {
			total -= t;
			printf("Lose %d.  Total = %d.\n", t, total);
		}
		if (n1 > n2 && b == 0) {
			total += t;
			printf("Win %d!  Total = %d.\n", t, total);
		}
		if (n1 < n2 && b == 0) {
			total -= t;
			printf("Lose %d.  Total = %d.\n", t, total);
		}
		if (n1 < n2 && b == 1) {
			total += t;
			printf("Win %d!  Total = %d.\n", t, total);
		}
		if (total == 0) {
			printf("Game Over.");
			break;
		}
	}
			 
	system("pause");
	return 0;
}