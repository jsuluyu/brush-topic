#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct student
{
	string name;
	int height;
};

bool cmp(student x, student y)
{
	if (x.height > y.height)
		return true;
	else if (x.height < y.height)
		return false;
	else
		return x.name > y.name ? false : true;
}

int main()
{
	int n, k;
	cin >> n >> k;
	vector<student> stu(n);
	for (int i = 0; i < n; i++)
		cin >> stu[i].name >> stu[i].height;
	sort(stu.begin(), stu.end(), cmp);
	
	int row = k, id = 0, m; // id 代表某一排的区间
	while (row) {
		if (row == k)
			m = n - (n / k) * (k - 1);
		else
			m = n / k;
		vector<string> arr(m);
		arr[m / 2] = stu[id].name;
		// 排最高的左边
		int j = m / 2 - 1;
		for (int i = id + 1; i < id + m; i += 2)
			arr[j--] = stu[i].name;
		// 排右边
		j = m / 2 + 1;
		for (int i = id + 2; i < id + m; i += 2)
			arr[j++] = stu[i].name;
		// 输出当前排
		for (int i = 0; i < m; i++) {
			if (i != 0) cout << " ";
			cout << arr[i];
		}
		cout << endl;
		id += m;
		row--;
	}

	system("pause");
	return 0;
}