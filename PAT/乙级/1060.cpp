#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

bool cmp(int x, int y)
{
	return x < y;
}

int main()
{
	int n;
	cin >> n;
	vector<int> arr(n);
	for (int i = 0; i < n; i++)
		cin >> arr[i];
	
	sort(arr.begin(), arr.end(), cmp);
	int E = 0;
	// 循环比较某元素的值是否大于其后续项数，若满足，则得到E值，退出
	for (int i = 0; i < n; i++) {
		if (arr[i] > n - i) {
			E = n - i;
			break;
		}
	}
	printf("%d\n", E);

	system("pause");
	return 0;
}