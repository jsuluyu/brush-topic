#include <iostream>
#include <algorithm>
#include <set>
#include <iterator>
using namespace std;
int main(){
	int N,a[100005]={-1};	scanf("%d",&N);
	for(int i=0;i<100005;i++)	a[i]=-1;
	for(int i=0;i<N;i++){
		int tem1,tem2;	scanf("%d %d",&tem1,&tem2);
		a[tem1]=tem2;
		a[tem2]=tem1;
	}
	int M;	scanf("%d",&M);
	int b[M];
	set<int> s;
	for(int i=0;i<M;i++)	scanf("%d",&b[i]);
	for(int i=0;i<M;i++){
		int cnt=0;
		for(int j=0;j<M;j++){
			if(a[b[i]]!=b[j])	cnt++;
			if(cnt==M)	s.insert(b[i]);
		}	
	}
	printf("%d\n",s.size());
	for(auto it=s.begin();it!=s.end();it++){
		if(it!=s.begin())	printf(" ");
		printf("%05d",*it);
	}
	return 0;
}