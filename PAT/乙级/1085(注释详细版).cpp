#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct school
{
	int score = 0; // 总分
	int count = 0; // 学生人数
	int grade[3] = { 0, 0, 0 };
};

struct rankSchool
{
	int score; // 总分
	int count; // 学生人数
	string name; // 学校姓名
};

void strToLow(string& str)
{
	for (int i = 0; i < str.length(); i++) {
		str[i] = tolower(str[i]);
	}
}

bool cmp(rankSchool& x, rankSchool& y)
{
	if (x.score != y.score) return x.score > y.score;
	else if (x.count != y.count) return x.count < y.count;
	else if (x.name != y.name) return x.name < y.name;
}

int main()
{
	int n;
	cin >> n;
	int score;  // 学生分数
	string t1, t2; // t1: 准考证号  t2: 学校
	unordered_map<string, school> hashMap; // 主键：学校名称 value:学校信息
	int id;
	for (int i = 0; i < n; i++) {
		cin >> t1 >> score >> t2;
		if (t1[0] == 'B') id = 0; // 乙级
		else if (t1[0] == 'A') id = 1; // 甲级
		else id = 2; // 顶级
		strToLow(t2); // 大写转小写字母
		hashMap[t2].count++; // 该校的学生数目+1
		hashMap[t2].grade[id] += score;
	}
	int len = hashMap.size(); // 学校的个数
	vector<rankSchool> vArr;
	rankSchool temp;
	for (auto it = hashMap.begin(); it != hashMap.end(); it++) {
		it->second.score = floor(it->second.grade[0] / 1.5 + it->second.grade[1] + it->second.grade[2] * 1.5); // 向下取整
		temp.name = it->first;
		temp.count = it->second.count;
		temp.score = it->second.score;
		vArr.push_back(temp);
	}
	sort(vArr.begin(), vArr.end(), cmp);
	cout << len << endl;
	id = 1;
	for (int i = 0; i < len; i++) {
		cout << id << " " << vArr[i].name << " " << vArr[i].score << " " << vArr[i].count << endl;
		if (i + 1 < len && vArr[i].score != vArr[i + 1].score)
			id = i + 2;
	}

	system("pause");
	return 0;
}