#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int change(int x)
{
	int sum = 0;
	while (x) {
		sum += x % 10;
		x /= 10;
	}
	return sum;
}

int main()
{
	int n;
	cin >> n;
	vector<int> number(n);
	for (int i = 0; i < n; i++) {
		cin >> number[i];
	}

	map<int, int> hashMap;
	for (int i = 0; i < n; i++) {
		int sum = change(number[i]);
		hashMap[sum] += 1;
	}

	cout << hashMap.size() << endl;
	int flag = false;
	for (auto it = hashMap.begin(); it != hashMap.end(); it++) {
		//if (it->second > 1) {
			if (flag) cout << " ";
			cout << it->first;
			flag = true;
		//}
	}

	system("pause");
	return 0;
}