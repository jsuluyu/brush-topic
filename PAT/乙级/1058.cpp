#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n, m; // n为学生个数，m为题目个数
	cin >> n >> m;
	vector<string> qAnswer; // 存放题目答案
	vector<int> counts(m); // 记录每道题答对的人数
	scanf_s("\n", "");//吸收掉换行符
	for (int i = 0; i < m; i++) {
		string temp;
		getline(cin, temp);
		qAnswer.push_back(temp);
	}

	for (int i = 0; i < n; i++) { // 读取学生的答案
		string answer;
		getline(cin, answer);
		int idx = 1; // 跳过回答的第一个括号
		int grade = 0; // 记录得分
		for (int k = 0; k < m; k++) { // 共m道题
			if (idx < answer.length()) {
				int pIdx = idx; // 记录指针的初始位置
				int num = answer[idx] - '0'; // 学生回答的当前题答案个数
				int pos = 4; // 指向题库的索引
				if (answer[idx] == qAnswer[k][pos]) { // 答案个数相同
					idx += 2; // 索引下移准备比较选项内容
					pos += 2;
					for (int t = 0; t < num; t++) { // 遍历每道题的每道答案
						if (answer[idx] != qAnswer[k][pos]) {
							//idx += 2;
							//pos += 2;
							break;
						}
						if (t == num - 1) { // 说明此题回答正确
							counts[k]++;
							grade += qAnswer[k][0] - '0';
						}
						idx += 2; pos += 2;
					}
				}
				idx = pIdx + num * 2 + 4; //指向下一题
			}
		}
		cout << grade << endl;
	}

	// 寻找错的最多的题目
	int max = 0, maxId;
	for (int i = 0; i < m; i++) {
		if (n - counts[i] > max) {
			max = n - counts[i];
			maxId = i;
		}
	}
	if (max == 0) cout << "Too simple" << endl;
	else {
		cout << max;
		for (int i = 0; i < m; i++) {
			if (n - counts[i] == max)
				cout << " " << i + 1;
		}
	}

	// 寻找错的最多的题目方式2
	//int j = 0;//记录正确率最低那道题的角标
	//for (int i = 1; i < m; i++) {
	//	if (counts[i] <= counts[j]) j = i;
	//}
	//int count = counts[j];//统计正确率最低的人数 
	//if (counts[j] == n) {
	//	cout << "Too simple";
	//	return 0;
	//}
	//cout << n - count << " ";
	//for (int i = 0; i < m && i < j; i++) {
	//	if (counts[i] == counts[j]) cout << i + 1 << " ";
	//}
	//cout << j + 1;

	system("pause");
	return 0;
}