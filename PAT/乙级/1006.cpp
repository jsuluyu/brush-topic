#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

int main()
{
	int n;
	int hundred = 0, ten = 0, single = 0;
	vector<char> ans;

	cin >> n;
	if (n > 100) {
		hundred = n / 100;
		n = n % 100;
		ten = n / 10;
		single = n % 10;
	}
	else if (n > 10) {
		ten = n / 10;
		single = n % 10;
	}
	else {
		single = n;
	}

	while (hundred--) {
		ans.push_back('B');
	}
	while (ten--) {
		ans.push_back('S');
	}
	int s = 1;
	// cout << single << endl;
	while (s <= single) {
		char temp = s + '0';
		ans.push_back(temp);
		s++;
	}

	for (int i = 0; i < ans.size(); i++) {
		cout << ans[i];
	}
	// system("pause");
	return 0;
}
