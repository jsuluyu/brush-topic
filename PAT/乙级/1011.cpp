#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <algorithm>
using namespace std;

int main()
{
	int n;
	long long A, B, C;
	int num = 1;

	cin >> n;
	for (; num <= n; num++) {
		cin >> A >> B >> C;
		if (A + B > C) {
			cout << "Case #" << num << ": " << "true" << endl;;
		}
		else {
			cout << "Case #" << num << ": " << "false" << endl;
		}
	}

	//system("pause");
	return 0;
}
