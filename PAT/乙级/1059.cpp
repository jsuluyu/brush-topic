#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

bool isPrime(int num)
{
	for (int i = 2; i <= (int)sqrt(num); i++) {
		if (num % i == 0)
			return false;
	}
	return true;
}

int main()
{
	int n, m;
	cin >> n;
	vector<string> stuId(n+1);
	unordered_map<string, int> rank;
	unordered_map<string, bool> hashMap;
	for (int i = 1; i <= n; i++) {
		cin >> stuId[i];
		hashMap[stuId[i]] = false;
		rank[stuId[i]] = i; // 排名
	}
		
	cin >> m; // 要查询的人数
	for (int i = 0; i < m; i++) {
		string checkId;
		cin >> checkId;

		if (rank[checkId] == 0) {
			cout << checkId << ": " << "Are you kidding?" << endl;
			hashMap[checkId] = true;
		}
		else if (hashMap[checkId]) {
			printf("%s: Checked\n", checkId.c_str());
		}	
		else if (rank[checkId] == 1) {
			printf("%s: Mystery Award\n", checkId.c_str());
			hashMap[checkId] = true;
		}
		else if (isPrime(rank[checkId])) {
			printf("%s: Minion\n", checkId.c_str());
			hashMap[checkId] = true;
		}
		else {
			printf("%s: Chocolate\n", checkId.c_str());
			hashMap[checkId] = true;
		}
	}

	system("pause");
	return 0;
}