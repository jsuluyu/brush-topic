#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
using namespace std;

struct person
{
	string name;
	string data;
};

bool judgment(string data)
{
	int year = stoi(data.substr(0, 4));
	int month = stoi(data.substr(5, 2));
	int day = stoi(data.substr(8, 2));
	if (year > 2014 || year < 1814)
		return false;
	else if ((year == 1814 && month < 9) || (year == 1814 && month == 9 && day < 6))
		return false;
	else if ((year == 2014 && month > 9) || (year == 2014 && month == 9 && day > 6))
		return false;
	else
		return true;
}

int calculate(string data)
{
	int mon[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	int year = stoi(data.substr(0, 4));
	int month = stoi(data.substr(5, 2));
	int day = stoi(data.substr(8, 2));
	int sum = 0;

	for (int i = year; i < 2014; i++) {
		if ((i % 4 == 0 && i % 100 != 0) || (i % 400 == 0))
			sum += 366;
		else
			sum += 365;
	}
	for (int i = 0; i < month; i++)
		sum += mon[i];
	sum += day;
	return sum;
}

int main()
{
	int N;
	int count = 0;
	string oldName, youngName;
	cin >> N;
	vector<person> P(N);

	for (int i = 0; i < N; i++) {
		cin >> P[i].name >> P[i].data;
	}
	
	int maxDay = 0, minDay = INT_MAX;
	for (int i = 0; i < N; i++) {
		string temp = P[i].data;
		if (judgment(temp)) {
			count++;
			int tempDay = calculate(temp);
			if (tempDay > maxDay) {
				maxDay = tempDay;
				oldName = P[i].name;
			}
			if (tempDay < minDay) {
				minDay = tempDay;
				youngName = P[i].name;
			}
		}
	}
	
	if (count == 0)
		cout << count;
	else
		cout << count << " " << oldName << " " << youngName;

	system("pause");
	return 0;
}
