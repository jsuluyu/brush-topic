#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct student
{
    string id;
    int tNum;
    int num;
};

int main()
{
    int n, m;
    cin>>n;
    vector<student> arr(n);
    for(int i=0; i<n; i++) {
        cin>>arr[i].id>>arr[i].tNum>>arr[i].num;
    }
    cin>>m;
    vector<int> temp(m);
    for(int i=0; i<m; i++)
        cin>>temp[i];
    for(int i=0; i<m; i++) {
        int tempNum = temp[i];
        for(int j=0; j<n; j++) {
            if(tempNum == arr[j].tNum) {
                cout<<arr[j].id<<" "<<arr[j].num<<endl;
                break;
            }
        }
    }
    return 0;
}