#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n, m, start, end, aim;
	cin >> n >> m >> start >> end >> aim;
	int temp;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			scanf("%d", &temp);
			if (temp >= start && temp <= end)
				temp = aim;
			if (j != 0) printf(" ");
			printf("%03d", temp);
		}
		printf("\n");
	}
	//printf("%04d", n);

	system("pause");
	return 0;
}