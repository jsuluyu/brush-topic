#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	string password;
	int n;
	cin >> password;
	cin >> n;
	getchar();
	string temp;
	getline(cin, temp);
	while (temp != "#") {	
		n--;
		if (temp == password && n >= 0) {
			printf("Welcome in\n");
			break;
		}
		if (temp != password && n >= 0) {
			printf("Wrong password: %s\n", temp.c_str());
			if (n == 0) {
				printf("Account locked");
				break;
			}
		}
		getline(cin, temp);		
	}

	system("pause");
	return 0;
}