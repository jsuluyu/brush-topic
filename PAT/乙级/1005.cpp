#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	int n;
	int arr[101];
	bool flag[101] = { false };
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> arr[i];
	}

	int idx = 0;
	while (idx < n) {
		int temp = arr[idx];
		while (temp != 1) {
			if (temp % 2 == 0) { // 偶数
				temp = temp / 2;
            if(temp <= 100)
			    flag[temp] = true;
			}	
			else {
				temp = (3 * temp + 1) / 2;
                if(temp <= 100)
				    flag[temp] = true;
			}	
		}
		idx++;
	}
	
	vector<int> ans;
	for (int i = 0; i < n; i++) {
		if (flag[arr[i]] != true)
			//cout << arr[i] << " ";
			ans.push_back(arr[i]);
	}
	sort(ans.begin(), ans.end());
	for (int i=ans.size()-1 ; i>=0; i--) {
        if(i != 0)
		    cout << ans[i] << " ";
        else
            cout << ans[i];
	}

	//system("pause");
	return 0;
}
