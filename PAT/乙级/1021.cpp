#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
using namespace std;

int main()
{
	string NUM;
	int hashMap[10] = {0};
	
	cin >> NUM;
	for (int i = 0; i < NUM.length(); i++) {
		int temp = NUM[i] - '0'; // char->int -'0'
		hashMap[temp]++;
	}

	for (int i = 0; i < 10; i++) {
		if (hashMap[i] != 0)
			cout << i << ":" << hashMap[i] << endl;
	}

	// system("pause");
	return 0;
}
