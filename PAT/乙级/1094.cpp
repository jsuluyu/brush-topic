#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

bool checkPrime(int& x)
{
	int sq = sqrt(x);
	for (int i = 2; i < sq; i++) {
		if (x % i == 0)
			return false;
	}
	return true;
}

int main()
{
	int n, m; // 总长度 素数长度
	cin >> n >> m;
	string str;
	cin >> str;
	int len = str.length();
	bool flag = false;
	for (int i = 0; i < len - m + 1; i++) {  // 注意n不一定大于m的，因此此for循环外部最好加一层判断：if(n >= m)
		string subStr = str.substr(i, m);
		int num = stoi(subStr);
		if (checkPrime(num)) {
			cout << subStr << endl;
			flag = true;
			break;
		}
	}
	if (!flag)
		cout << 404 << endl;

	system("pause");
	return 0;
}