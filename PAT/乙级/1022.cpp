#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
using namespace std;

void change(int sum, int D, string& ans)
{
	int temp;
	 do{ // 使用do-while是防止sum=0的情况出现
		int t1 = sum % D;
		ans += (t1 + '0'); // int->char +'0'
		sum /= D;
	 } while (sum != 0);
}

int main()
{
	int A, B, D, sum;
	string ans = "";

	cin >> A >> B >> D;
	sum = A + B;

	change(sum, D, ans);

	int len = ans.length();
	for(int i=len-1; i>=0; i--)
		cout << ans[i];
	cout << endl;

	// system("pause");
	return 0;
}
