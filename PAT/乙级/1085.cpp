#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct exam
{
	int score = 0;
	int count = 0;
	int grade[3] = { 0, 0, 0 };
};

struct school
{
	string name;
	int score;
	int count;
};

bool cmp(school& x, school& y)
{
	if (x.score > y.score)
		return true;
	else if (x.score < y.score)
		return false;
	else {
		if (x.count < y.count)
			return true;
		else if (x.count > y.count)
			return false;
		else {
			if (x.name < y.name)
				return true;
			else
				return false;
		}
	}
}

void strToLow(string& str)
{
	for (int i = 0; i < str.length(); i++) {
		str[i] = tolower(str[i]);
	}
}

int main()
{
	int n;
	cin >> n;
	unordered_map<string, exam> hashMap;
	string t1, t2;
	int num;
	for (int i = 0; i < n; i++) {
		int id = 0;
		cin >> t1 >> num >> t2;
		strToLow(t2);
		if (t1[0] == 'B') id = 0; // 乙级
		else if (t1[0] == 'A') id = 1; // 甲级
		else id = 2; // 顶级
		hashMap[t2].grade[id] += num; // 该校对应的等级成绩总和
		hashMap[t2].count++;
	}
	int len = hashMap.size();
	vector<school> vArr;
	for (auto it = hashMap.begin(); it != hashMap.end(); it++) {
		school temp;
		it->second.score = floor(it->second.grade[0] / 1.5 + it->second.grade[1] + it->second.grade[2] * 1.5);
		temp.name = it->first;
		temp.score = it->second.score;
		temp.count = it->second.count;
		vArr.push_back(temp);
	}
	sort(vArr.begin(), vArr.end(), cmp);
	cout << len << endl;
	int id = 1, sum = 1;
	for (int i = 0; i < len; i++) {
		sum++;
		cout << id << " " << vArr[i].name << " " << vArr[i].score << " " << vArr[i].count << endl;
		// if (i - 1 >= 0 && (vArr[i].score != vArr[i - 1].score))
		if (i + 1 < len && (vArr[i].score != vArr[i + 1].score))
			id = i+2;
	}

	system("pause");
	return 0;
}