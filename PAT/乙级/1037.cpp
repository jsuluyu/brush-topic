#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct money
{
	int g;
	int s;
	int k;
};

int main()
{
	money P, A, ans;
	scanf_s("%d.%d.%d", &P.g, &P.s, &P.k);
	scanf_s("%d.%d.%d", &A.g, &A.s, &A.k);

	int size = 1;
	if (P.g > A.g || (P.g == A.g && P.s > A.s) || (P.g == A.g && P.s == A.s && P.k < A.s)) {
		swap(P, A);
		size = -1;
	}
		
	if (A.k >= P.k) ans.k = A.k - P.k;
	else {
		if (A.s > 0) { A.s -= 1; A.k += 29; }
		else { A.g -= 1; A.s += 16; A.k += 29; }
		ans.k = A.k - P.k;
	}

	if (A.s >= P.s) ans.s = A.s - P.s;
	else {
		A.g -= 1;
		A.s += 17;
		ans.s = A.s - P.s;
	}

	ans.g = A.g - P.g;

	/*ans.g *= size;
	ans.s *= size;
	ans.k *= size;*/

	if (size == -1) printf("-");
	printf("%d.%d.%d", ans.g, ans.s, ans.k);

	system("pause");
	return 0;
}


