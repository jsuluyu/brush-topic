#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n;
	cin >> n;
	set<int> hashSet;
	for (int i = 1; i <= n; i++) {
		int temp = i / 2 + i / 3 + i / 5;
		hashSet.insert(temp);
	}
	cout << hashSet.size() << endl;

	system("pause");
	return 0;
}