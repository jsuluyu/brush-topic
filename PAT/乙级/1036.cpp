#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n;
	char ch;
	cin >> n;
	cin >> ch;

	int row = n / 2;
	row += n % 2 ? 1 : 0;

	for (int i = 0; i < row; i++) {
		if (i == 0 || i == row - 1) {
			for (int j = 0; j < n; j++)
				cout << ch;
			cout << endl;
		}
		else {
			for (int j = 0; j < n; j++) {
				if (j == 0 || j == n - 1)
					cout << ch;
				else
					cout << " ";
			}
			cout << endl;
		}
	}

	system("pause");
	return 0;
}


