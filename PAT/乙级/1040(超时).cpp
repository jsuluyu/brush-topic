#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	string str1;
	cin >> str1;

	int ans = 0;
	for (int i = 0; i < str1.length(); i++) {
		bool flag = true;
		int idx1 = i;
		if (str1[i] == 'P') {
			while (flag) {
				size_t position1 = str1.find('A', idx1);
				// size_t position2 = str1.find('T', position1);
				if (position1 != string::npos) {
					size_t position2 = position1;
					//cout << position2;
					while ((position2 = str1.find('T', position2)) != string::npos) {
						ans++;
						position2++;
					}
					idx1 = position1 + 1;
				}
				else
					flag = false;
			}	
		}
	}

	cout << ans % 1000000007;

	system("pause");
	return 0;
}


