#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n, D;
	int total1 = 0, total2 = 0;
	double e;
	cin >> n >> e >> D;
	int temp = n;
	while (temp--) {
		int num;
		int count = 0;
		cin >> num;
		vector<double> user(num);
		for (int i = 0; i < num; i++) cin >> user[i];
		for (int i = 0; i < num; i++) {
			if (user[i] < e)
				count++;
		}
		if (count > (num / 2)) {
			num > D ? total2++ : total1++;
		}
		user.clear();
	}

	double rate1 = total1 / (n * 1.0);
	double rate2 = total2 / (n * 1.0);
	printf("%.1lf%% %.1lf%%\n", rate1 * 100, rate2 * 100);

	system("pause");
	return 0;
}