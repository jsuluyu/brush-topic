#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
分析：用string s接收所需变换的数字，每次遍历s，从当前位置i开始，看后面有多少个与s[i]相同，设j处开始不相同，
那么临时字符串 t += s[i] + to_string(j – i);然后再将t赋值给s，cnt只要没达到n次就继续加油循环下一次，最后输出s的值～
*/

int main()
{
	string s;
	int n, j;
	cin >> s >> n;
	for (int id = 1; id < n; id++) {
		string t;
		for (int i = 0; i < s.length(); i = j) {
			for (j = i; j < s.length() && s[i] == s[j]; j++); // 注意这个for循环是单独的，作用是寻找有多少相同的字符
			t += s[i] + to_string(j - i);
		}
		s = t;
	}

	cout << s << endl;

	system("pause");
	return 0;
}