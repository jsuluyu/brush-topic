#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	string str1, str2;
	getline(cin, str1);
	getline(cin, str2);
	unordered_set<char> hashSet;
	for (int i = 0; i < str1.length(); i++)
		hashSet.insert(str1[i]);
	for (int i = 0; i < str2.length(); i++)
		hashSet.insert(str2[i]);
	for (unordered_set<char>::iterator it = hashSet.begin(); it != hashSet.end(); it++)
		cout << *it;

	system("pause");
	return 0;
}