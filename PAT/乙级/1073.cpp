#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct title
{
	int fullScore, choiceNum, rightChoiceNum;
	int choice[5] = { 0 };
};

int main()
{
	int n, m;
	int mis[111][5] = {0}; // 记录第i道题目的第j个选项的选择情况
	int ti[5];
	title s[111];
	double score;
	char ch;
	scanf("%d%d", &n, &m);
	for (int i = 0; i < m; i++) {
		scanf("%d%d%d", &s[i].fullScore, &s[i].choiceNum, &s[i].rightChoiceNum);
		for (int j = 0; j < s[i].rightChoiceNum; j++) {
			scanf(" %c", &ch);
			s[i].choice[ch - 'a'] = 1;
		}
	}

	int nn; // 学生某题选的选项个数
	int rightNum, falseNum, maxNum = 0;
	for (int i = 0; i < n; i++) { // 遍历每一个学生的答案
		score = 0;
		for (int j = 0; j < m; j++) { // 遍历每一道题目
			rightNum = falseNum = 0;
			getchar(); getchar(); // 接收多余字符
			scanf("%d", &nn);
			memset(ti, 0, sizeof(ti));
			for (int k = 0; k < nn; k++) { // 遍历学生每道题目的答案
				scanf(" %c", &ch);
				ti[ch - 'a'] = 1;
				if (s[j].choice[ch - 'a']) { // 此选项是正确选项
					rightNum++;
				}
				else {
					falseNum++;
					maxNum = max(maxNum, ++mis[j][ch - 'a']); // ++mis[j][ch - 'a']错误选项加1
				}
			}
			getchar();
			if (!falseNum) {
				if (rightNum == s[j].rightChoiceNum)
					score += s[j].fullScore;
				else if (rightNum)
					score += s[j].fullScore / 2.0;
			}
			for (int k = 0; k < 5; k++) { // 这边记录漏选的
				if (s[j].choice[k] && !ti[k]) {
					maxNum = max(maxNum, ++mis[j][k]);
				}
			}
		}
		printf("%.1lf\n", score);
	}

	if (maxNum == 0) {
		printf("Too simple\n");
	}
	else {
		for (int i = 0; i < m; i++)
			for (int j = 0; j < 5; j++) {
				if (mis[i][j] == maxNum) {
					printf("%d %d-%c\n", maxNum, i + 1, j + 'a');
				}
			}
	}

	system("pause");
	return 0;
}