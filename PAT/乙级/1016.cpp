#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
using namespace std;

int main()
{
	string A, B;
	char tA, tB;

	cin >> A >> tA;
	cin >> B >> tB;

	int count1 = 0;
	for (int i = 0; i < A.length(); i++) {
		if (A[i] == tA)
			count1++;
	}
	int count2 = 0;
	for (int i = 0; i < B.length(); i++) {
		if (B[i] == tB)
			count2++;
	}

	int num1 = tA - '0'; // char->int:-'0'  int->char:+'0'
	int temp1 = 0;
	while (count1 > 0) {
		temp1 *= 10;
		temp1 += num1;
		count1--;
	}
	//cout << num1 << temp1 << endl;

	int num2 = tB - '0'; // char->int:-'0'  int->char:+'0'
	int temp2 = 0;
	while (count2 > 0) {
		temp2 *= 10;
		temp2 += num2;
		count2--;
	}
	//cout << num2 << temp2 <<endl;

	cout << temp1 + temp2 << endl;

	system("pause");
	return 0;
}
