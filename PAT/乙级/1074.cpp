#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	string str, ans = "";
	string s1, s2;
	cin >> str >> s1 >> s2;
	string s11(str.length() - s1.length(), '0');
	s1 = s11 + s1;
	string s22(str.length() - s2.length(), '0');
	s2 = s22 + s2;

	int carry = 0, flag = 0;
	for (int i = str.length() - 1; i >= 0; i--) {
		int mod = str[i] == '0' ? 10 : (str[i] - '0');
		ans += (s1[i] - '0' + s2[i] - '0' + carry) % mod + '0';
		carry = (s1[i] - '0' + s2[i] - '0' + carry) / mod;
	}
	if (carry != 0) ans += '1';
	// cout << ans << endl;
	for (int i = ans.length() - 1; i >= 0; i--) {
		if (ans[i] != '0' || flag == 1) {
			flag = 1;
			cout << ans[i];
		}
	}
	if (flag == 0)
		cout << 0;

	system("pause");
	return 0;
}