#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n, m;
	cin >> n >> m;
	vector<int> tScore(m);
	vector<int> answer(m);
	for (int i = 0; i < m; i++)
		cin >> tScore[i];
	for (int i = 0; i < m; i++)
		cin >> answer[i];
	while (n--) {
		int grade = 0;
		vector<int> stuAns(m);
		for (int i = 0; i < m; i++)
			cin >> stuAns[i];
		for (int i = 0; i < m; i++) {
			if (answer[i] == stuAns[i])
				grade += tScore[i];
		}
		cout << grade << endl;
	}
	
	system("pause");
	return 0;
}