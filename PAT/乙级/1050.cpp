#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

bool cmp(int x, int y) { return x > y; }

int main()
{
	int N, row, col;
	cin >> N;
	vector<int> a(N);
	for (col = sqrt((double)N); col >= 1; col--) {
		if (N % col == 0) {
			row = N / col;
			break;
		}
	}
	for (int i = 0; i < N; i++) cin >> a[i];
	sort(a.begin(), a.end(), cmp);
	vector<vector<int>> ans(row, vector<int>(col)); // 二维vector的创建方式

	int id = 0;
	int level = (row % 2 == 0) ? row / 2 : row / 2 + 1;
	for (int i = 0; i < level; i++) {
		for (int j = i; j <= col - i - 1 && id < N; j++) // 从左上到右上
			ans[i][j] = a[id++];
		for (int j = i+1; j <= row - i - 2 && id < N; j++) // 从右上到右下
			ans[j][col - i - 1] = a[id++];
		for (int j = col - i - 1; j >= i && id < N; j--) // 从右下到左下
			ans[row - i - 1][j] = a[id++];
		for (int j = row - i - 2; j >= i+1 && id < N; j--) // 从左下到右上
			ans[j][i] = a[id++];
	}

	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			if (j != 0) cout << " ";
			cout << ans[i][j];
		}
		cout << endl;
	}

	system("pause");
	return 0;
}