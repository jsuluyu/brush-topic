#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
	分析：要想知道构成多少个PAT，那么遍历字符串后对于每一A，它前面的P的个数和它后面的T的个数的乘积就是能构成的PAT的个数。
	然后把对于每一个A的结果相加即可～～辣么就简单啦，只需要先遍历字符串数一数有多少个T～～然后每遇到一个T呢～～countt-- ；
	每遇到一个P呢，countp++;然后一遇到字母A呢就countt * countp～～把这个结果累加到result中～～最后输出结果就好啦~~
	对了别忘记要对10000000007取余哦～～
*/

int main()
{
	string str1;
	cin >> str1;

	int len = str1.length();
	int countP = 0, countA = 0, countT = 0;

	// 第一轮循环保存全部T的数量
	for (char ch : str1) {
		if (ch == 'T') countT++;
	}

	// 第二轮循环分别统计A左右两侧P,T的数量，右侧T的数量就等于“总数-左边的数量”
	int ans = 0;
	for (char ch : str1) {
		if (ch == 'P') countP++;
		if (ch == 'T') countT--;
		if (ch == 'A') ans = (ans + countT * countP) % 1000000007;
	}

	cout << ans;

	system("pause");
	return 0;
}


