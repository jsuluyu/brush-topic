#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n;
	cin >> n;
	string answer;
	getchar();
	for (int i = 0; i < n; i++) {
		getline(cin, answer);
		string::size_type position = answer.find('T');
		int password = answer[position - 2] - 'A' + 1;
		cout << password;
	}

	system("pause");
	return 0;
}