#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
注：
核心思想：
整数转化为字符串：加 ‘0’ ，然后逆序。
字符串转化整数：减 ‘0’,乘以10累加。
注：整数加 ‘0’后会隐性的转化为char类型；字符减 ‘0’隐性转化为int类型
*/

int main()
{
	string n;
	string strArr[10] = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
	cin >> n;
	int num, ans = 0;
	for (char ch : n) {
		num = ch - '0';
		ans += num;
	}
	int id = 0, temp;
	stack<string> s1;
	while (ans) {
		temp = ans % 10;
		ans /= 10;
		s1.push(strArr[temp]);
	}
	if (s1.empty()) { // 不要忘记这个情况
		cout << strArr[0];
	}
	while (!s1.empty()) {
		if (id != 0) cout << " ";
		cout << s1.top();
		s1.pop();
		id++;
	}

	system("pause");
	return 0;
}