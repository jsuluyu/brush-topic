#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n, m;
	int t1;
	double t2;
	map<int, double> hashMap; // first: 系数 second:指数
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> t1 >> t2;
		hashMap[t1] += t2;
	}
	cin >> m;
	for (int i = 0; i < m; i++) {
		cin >> t1 >> t2;
		hashMap[t1] += t2;
	}
	int count = 0;
	for (auto it = hashMap.rbegin(); it != hashMap.rend(); it++) {
		if (it->second != 0)
			count++;;
	}
	cout << count;
	for (auto it = hashMap.rbegin(); it != hashMap.rend(); it++) {
		if(it->second != 0)
			printf(" %d %.1lf", it->first, it->second);
	}
		
	system("pause");
	return 0;
}