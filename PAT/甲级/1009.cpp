#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
注：
坑1： 测试用例0：系数为0的不输出且数量也不算
坑2： 测试用例3：最后结果必须按照指数顺序，从大到小输出
*/

struct poly
{
	int exp; // 指数
	double coef; // 系数
};

bool cmp(poly& x, poly& y)
{
	if (x.exp > y.exp) return true;
	else return false;
}

int main()
{
	int k1, k2;
	cin >> k1;
	poly temp;
	vector<poly> vec1, vec2;
	vector<poly> result, ans;
	for (int i = 0; i < k1; i++) {
		cin >> temp.exp >> temp.coef;
		vec1.push_back(temp);
	}
	cin >> k2;
	for (int i = 0; i < k2; i++) {
		cin >> temp.exp >> temp.coef;
		vec2.push_back(temp);
	}
	for (int i = 0; i < k1; i++) {
		for (int j = 0; j < k2; j++) {
			temp.coef = vec1[i].coef * vec2[j].coef;
			temp.exp = vec1[i].exp + vec2[j].exp;
			result.push_back(temp);
		}
	}
	/*for (int i = 0; i < result.size(); i++) {
		if (i != 0) cout << " ";
		cout << result[i].coef << " " << result[i].exp;
	}*/
	vector<int> flag(result.size(), 0);
	for (int i = 0; i < result.size(); i++) {
		if (flag[i]) continue;
		temp.exp = result[i].exp;
		temp.coef = result[i].coef;
		for (int j = i + 1; j < result.size(); j++) {
			if (result[i].exp == result[j].exp) {
				temp.coef += result[j].coef;
				flag[j] = 1;
			}
		}
		ans.push_back(temp);
	}
	int count = 0;
	for (int i = 0; i < ans.size(); i++) {
		if (ans[i].coef != 0) count++;
	}
	cout << count << " ";
	sort(ans.begin(), ans.end(), cmp);
	for (int i = 0; i < ans.size(); i++) {
		if (ans[i].coef != 0) {
			if (i != 0) cout << " ";
			printf("%d %.1f", ans[i].exp, ans[i].coef);
		}
	}
	
	system("pause");
	return 0;
}