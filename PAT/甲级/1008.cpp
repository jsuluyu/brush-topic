#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n;
	cin >> n;
	int now, past = 0, time = 0;
	for (int i = 0; i < n; i++) {
		cin >> now;
		if (now > past) {
			time += (now - past) * 6 + 5;
		}
		else {
			time += (past - now) * 4 + 5;
		}
		past = now;
	}
	cout << time << endl;

	system("pause");
	return 0;
}