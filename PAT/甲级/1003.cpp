#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

vector<int> teamNum;
int road[501][501] = { 0 };
int vis[501] = { 0 };
int pathNum = 0; // 最短路径的个数
int maxTeam = 0; // 最大的队伍数目
int minLength = 9999999; // 最短路径的长度

void dfs(int& n, int& start, int& end, int curDis, int curTeamNum)
{
	if (curDis > minLength) 
		return;
	if (start == end) {
		if (curDis < minLength) { // 更新最短路径的长度
			minLength = curDis;
			pathNum = 1;
			maxTeam = curTeamNum;
		}
		else if (curDis == minLength) { // 有找到一条最短路径
			pathNum++;
			if (curTeamNum > maxTeam) // 若此路不仅是最短路径，而且队伍数目更少，则更新队伍数目
				maxTeam = curTeamNum;
		}
		return;
	}

	for (int i = 0; i < n; i++) {
		if (vis[i] == 0 && road[start][i] != 0) { // 若该城市没有访问过，并且两城市间有路径
			vis[i] = 1;
			dfs(n, i, end, curDis+road[start][i], curTeamNum+teamNum[i]);
			vis[i] = 0;
		}
	}
}

int main()
{
	int n, m, start, end;
	cin >> n >> m >> start >> end;
	teamNum.resize(n);
	for (int i = 0; i < n; i++)
		cin >> teamNum[i];
	int t1, t2, d;
	for (int i = 0; i < m; i++) {
		cin >> t1 >> t2 >> d;
		road[t1][t2] = d;
		road[t2][t1] = d;
	}

	dfs(n, start, end, 0, teamNum[start]);
	cout << pathNum << " " << maxTeam << endl;

	system("pause");
	return 0;
}