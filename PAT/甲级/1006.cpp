#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct person
{
	string id;
	string sign_in;
	string sign_out;
};

int main()
{
	int n;
	cin >> n;
	person temp;
	vector<person> signTable;
	for (int i = 0; i < n; i++) {
		cin >> temp.id >> temp.sign_in >> temp.sign_out;
		signTable.push_back(temp);
	}
	int earlyId, lateId;
	string early = signTable[0].sign_in;
	string late = signTable[0].sign_out;
	for (int i = 1; i < n; i++) {
		if (signTable[i].sign_in < early) {
			earlyId = i;
			early = signTable[i].sign_in;
		}
		if (signTable[i].sign_out > late) {
			lateId = i;
			late = signTable[i].sign_out;
		}
	}
	cout << signTable[earlyId].id << " " << signTable[lateId].id << endl;


	system("pause");
	return 0;
}