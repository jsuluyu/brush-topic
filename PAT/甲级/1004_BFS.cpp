#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
测试用例
8 5
1 2 2 3
2 1 4
3 2 5 6
4 1 7
5 1 8
*/

vector<vector<int>> tree(101);
vector<int> ans;

void bfs(int start)
{
	queue<int> Q;
	Q.push(start);
	int last = start; // last记录每层最后一个节点，到了最后一个节点就可以计算下一层了
	int count = 0;
	while (!Q.empty()) {
		int nodeId = Q.front();
		if (tree[nodeId].size() == 0) count++;
		for (int i = 0; i < tree[nodeId].size(); i++) Q.push(tree[nodeId][i]);
		if (nodeId == last) {
			last = Q.back();
			ans.push_back(count);
			count = 0;
		}
		Q.pop();
	}
}

int main()
{
	int n, m;
	int key, num, val;
	cin >> n >> m;
	for (int i = 0; i < m; i++) {
		cin >> key >> num;
		for (int j = 0; j < num; j++) {
			cin >> val;
			tree[key].push_back(val);
		}
	}
	bfs(1);
	for (int i = 0; i < ans.size(); i++) {
		if (i != 0) cout << " ";
		cout << ans[i];
	}

	system("pause");
	return 0;
}