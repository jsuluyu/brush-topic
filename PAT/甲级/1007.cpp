#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
注：
分析：sum为要求的最大和，temp为临时最大和，leftindex和rightindex为所求的子序列的下标，tempindex标记left的临时下标～
temp = temp + v[i]，当temp比sum大，就更新sum的值、leftindex和rightindex的值；当temp < 0，那么后面不管来什么值，
都应该舍弃temp < 0前面的内容，因为负数对于总和只可能拉低总和，不可能增加总和，还不如舍弃～
舍弃后，直接令temp = 0，并且同时更新left的临时值tempindex。因为对于所有的值都为负数的情况要输出0，第一个值，最后一个值，
所以在输入的时候用flag判断是不是所有的数字都是小于0的，如果是，要在输入的时候特殊处理～
*/

int main()
{
	int n;
	cin >> n;
	vector<int> sequence(n);
	for (int i = 0; i < n; i++) {
		cin >> sequence[i];
	}
	int maxSum = -1;
	int start = 0, end = n - 1, temp = 0, tStart = 0;
	for (int i = 0; i < n; i++) {
		temp = temp + sequence[i];
		if (temp < 0) {
			temp = 0;
			tStart = i + 1;
		}
		else if(temp > maxSum) {
			maxSum = temp;
			start = tStart;
			end = i;
		}
	}
	if (maxSum < 0) {
		maxSum = 0;
	}
	cout << maxSum << " " << sequence[start] << " " << sequence[end] << endl;

	system("pause");
	return 0;
}