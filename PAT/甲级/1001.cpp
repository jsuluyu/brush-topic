#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int x, y;
	cin >> x >> y;
	int sum = x + y;
	stack<int> sta;
	int temp;
	int copyT = sum;
	if (sum < 0) sum *= -1;
	while (sum) {
		temp = sum % 10;
		sta.push(temp);
		sum /= 10;
	}
	if (copyT < 0)
		cout << "-";
	int id = 0;
	int len = sta.size();
	if (sta.empty())  // 测试点4：当和为0时，直接输出
		cout << copyT;
	while (!sta.empty()) {
		if ((len - id) % 3 == 0 && id != 0) cout << ",";
		temp = sta.top();
		cout << temp;
		sta.pop();
		id++;
	}
	
	system("pause");
	return 0;
}