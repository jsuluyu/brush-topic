#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
注：
分析：convert函数：给定一个数值和一个进制，将它转化为10进制。转化过程中可能产生溢出
find_radix函数：找到令两个数值相等的进制数。在查找的过程中，需要使用二分查找算法，
如果使用当前进制转化得到数值比另一个大或者小于0，说明这个进制太大～
*/

long long convert(string n, long long radix) {
	long long sum = 0;
	int index = 0, temp = 0;
	for (auto it = n.rbegin(); it != n.rend(); it++) {
		temp = isdigit(*it) ? *it - '0' : *it - 'a' + 10;
		sum += temp * pow(radix, index++);
	}
	return sum;
}
long long find_radix(string n, long long num) {
	char it = *max_element(n.begin(), n.end());
	long long low = (isdigit(it) ? it - '0' : it - 'a' + 10) + 1;
	long long high = max(num, low); // max(各位数最大值加1，第一位数的十进制值)
	while (low <= high) {
		long long mid = (low + high) / 2;
		long long t = convert(n, mid);
		if (t < 0 || t > num) high = mid - 1;
		else if (t == num) return mid;
		else low = mid + 1;
	}
	return -1;
}

int main()
{
	string n1, n2;
	long long tag = 0, radix = 0, result_radix;
	cin >> n1 >> n2 >> tag >> radix;
	result_radix = tag == 1 ? find_radix(n2, convert(n1, radix)) : find_radix(n1, convert(n2, radix));
	if (result_radix != -1) {
		printf("%lld", result_radix);
	}
	else {
		printf("Impossible");
	}
	
	system("pause");
	return 0;
}