#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
测试用例
8 5
1 2 2 3
2 1 4
3 2 5 6
4 1 7
5 1 8
*/

int maxDepth = -1;
vector<vector<int>> tree(101);
vector<int> ans(101, 0);

void dfs(int nodeId, int depth)
{
	if (tree[nodeId].size() == 0) { // 若该结点没有子节点，则该节点为叶子结点
		ans[depth]++; // 该层叶子节点数+1
		maxDepth = max(maxDepth, depth);
		return;
	}
	for (int i = 0; i < tree[nodeId].size(); i++) {
		dfs(tree[nodeId][i], depth + 1);
	}
}

int main()
{
	int n, m;
	int key, num, val;
	cin >> n >> m;
	for (int i = 0; i < m; i++) {
		cin >> key >> num;
		for (int j = 0; j < num; j++) {
			cin >> val;
			tree[key].push_back(val);
		}
	}
	dfs(1, 1);
	for (int i = 1; i <= maxDepth; i++) {
		if (i != 1) cout << " ";
		cout << ans[i];
	}

	system("pause");
	return 0;
}