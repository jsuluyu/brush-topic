/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

// 法1：利用栈的先入后出
class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        stack<ListNode*> stackA;
        stack<ListNode*> stackB;
        ListNode* pNodeA = headA;
        ListNode* pNodeB = headB;
        ListNode* sameNode = NULL;
        while(pNodeA) {
            stackA.push(pNodeA);
            pNodeA = pNodeA->next;
        }
        while(pNodeB) {
            stackB.push(pNodeB);
            pNodeB = pNodeB->next;
        }
        while(!stackA.empty() && !stackB.empty()) {
            pNodeA = stackA.top();
            pNodeB = stackB.top();
            stackA.pop();
            stackB.pop();
            if(pNodeA != pNodeB)
                break;
            sameNode = pNodeA;
        }
        return sameNode;
    }
};

// 双指针法（快慢指针）
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    int getLength(ListNode* head)
    {
        ListNode* pNode = head;
        int length = 0;
        while(pNode) {
            length++;
            pNode = pNode->next;
        }
        return length;
    }
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        int len1 = getLength(headA);
        int len2 = getLength(headB);
        int nrLen = fabs(len1 - len2);
        ListNode* pNodeLong = headA;
        ListNode* pNodeShort = headB;
        if(len2 > len1) {
            pNodeLong = headB;
            pNodeShort = headA;
        }
        for(int i=0; i<nrLen; i++) 
            pNodeLong = pNodeLong->next;
        while(pNodeLong && pNodeShort && pNodeLong != pNodeShort) {
            pNodeLong = pNodeLong->next;
            pNodeShort = pNodeShort->next;
        }
        ListNode* sameNode = pNodeLong;
        return sameNode;
    }
};