/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        ListNode* pMergeHead = new ListNode(0);
        ListNode* p1 = l1;
        ListNode* p2 = l2;
        ListNode* pTemp = pMergeHead;
        while(p1 && p2) {
            if(p1->val > p2->val) {
                pTemp->next = p2;
                p2 = p2->next;
            }
            else {
                pTemp->next = p1;
                p1 = p1->next;
            }
            pTemp = pTemp->next;
        }
        if(p1)
            pTemp->next = p1;
        if(p2)
            pTemp->next = p2; 
        return pMergeHead->next;
    }
};