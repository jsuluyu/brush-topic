/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> reversePrint(ListNode* head) {
        vector<int> ans;
        vector<int> temp;
        while(head) {
            temp.push_back(head->val);
            head = head->next;
        }
        for(int i=temp.size()-1; i>=0; i--) {
            ans.push_back(temp[i]);
        }
        return ans;
    }
};