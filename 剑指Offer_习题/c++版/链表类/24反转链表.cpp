/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        ListNode* pNode = head; // 指向当前结点
        ListNode* pPre = NULL; // 指向前一个结点，即next指针指向的结点
        ListNode* pNext; // 指向当前结点的下一个结点
        ListNode* pReverse = NULL; // 反转后的头结点
        while(pNode) {
            pNext = pNode->next;
            if(pNext == NULL)
                pReverse = pNode;
            pNode->next = pPre; // 反转
            // 遍历下一节点
            pPre = pNode;
            pNode = pNext;
        }
        return pReverse;
    }
};