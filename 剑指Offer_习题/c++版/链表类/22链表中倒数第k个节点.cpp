/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* getKthFromEnd(ListNode* head, int k) {
        if(head == NULL || k == 0)
            return NULL;
        ListNode* pBefore = head;
        ListNode* pAfter = head;
        // 双指针：前后两指针距离保持为k-1
        for(int i=0; i<k-1; i++) {
            pAfter = pAfter->next;
        }
        while(pAfter->next) {
            pAfter = pAfter->next;
            pBefore = pBefore->next;
        }
        return pBefore;
    }
};