/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* next;
    Node* random;
    
    Node(int _val) {
        val = _val;
        next = NULL;
        random = NULL;
    }
};
*/

// 法1：拼接+拆分法
class Solution {
public:
    void cloneNodes(Node* head)
    {
        Node *pNode = head;
        while(pNode)
        {
            Node *pCloned = new Node(0);
            pCloned->val = pNode->val;
            pCloned->next = pNode->next;
            pCloned->random = NULL;

            pNode->next = pCloned;
            pNode = pCloned->next;
        }
    }

    void connectSiblingNodes(Node* head)
    {
        Node *pNode = head;
        while(pNode)
        {
            Node *pCloned = pNode->next;
            if(pNode->random)
            {
                pCloned->random = pNode->random->next; // 因为复制的时候，复制的结点在当前结点的后面
            }
            pNode = pCloned->next;
        }
    }

    Node* reconnectNodes(Node* head)
    {
        Node *pNode = head;
        Node *pClonedHead = NULL;
        Node *pClonedNode = NULL;

        // 首先初始化pClonedHead,pClonedNode
        if(pNode)
        {
            pClonedHead = pClonedNode = pNode->next; // 第二个结点是复制结点的头结点
            pNode->next = pClonedHead->next;
            pNode = pNode->next;
        }

        // 分离两个链表，奇数为原始链表，偶数为复制后的链表
        while(pNode)
        {
            pClonedNode->next = pNode->next;
            pClonedNode = pClonedNode->next;
            pNode->next = pClonedNode->next;
            pNode = pNode->next;
        }
        return pClonedHead;
    }

    Node* copyRandomList(Node* head) {
        if(head == NULL)
            return NULL;
        cloneNodes(head); // 复制每个结点，并连接到每个结点的后面
        connectSiblingNodes(head); // 连接每个结点的random指针
        return reconnectNodes(head); // 将复制后的链表和原链表分开
    }
};


// 法2：map键值对法
class Solution {
public:
    Node* copyRandomList(Node* head) {
        if(head == nullptr) return nullptr;
        Node* cur = head;
        unordered_map<Node*, Node*> map;
        // 3. 复制各节点，并建立 “原节点 -> 新节点” 的 Map 映射
        while(cur != nullptr) {
            map[cur] = new Node(cur->val);
            cur = cur->next;
        }
        cur = head;
        // 4. 构建新链表的 next 和 random 指向
        while(cur != nullptr) {
            map[cur]->next = map[cur->next];
            map[cur]->random = map[cur->random];
            cur = cur->next;
        }
        // 5. 返回新链表的头节点
        return map[head];
    }
};