class MinStack {
public:
	int minArray(vector<int>& numbers) {
		if (numbers.empty())
			return 0;
		int start = 0;
		int end = numbers.size() - 1;
		int mid = 0;
		while (numbers[start] >= numbers[end]) {
			if (start == end - 1) {
				mid = end;
				break;
			}
			mid = (start + end) >> 1;
			if (numbers[start] == numbers[mid] && numbers[mid] == numbers[end])
				return linerSearch(numbers);
			if (numbers[mid] >= numbers[end])
				start = mid;
			else if (numbers[mid] <= numbers[start])
				end = mid;
		}
		return numbers[mid];
	}

	int linerSearch(vector<int>& numbers)
	{
		int minNum = 99999;
		for (int i = 0; i < numbers.size(); i++) {
			if (numbers[i] < minNum)
				minNum = numbers[i];
		}
		return minNum;
	}
};