class MinStack {
public:
	int search(vector<int>& nums, int target) {
		if (nums.empty())
			return 0;
		int ans = 0;
		int start = binarySearch1(nums, 0, nums.size() - 1, target);
		int end = binarySearch2(nums, 0, nums.size() - 1, target);
		if (start != -1 || end != -1)
			ans = end - start + 1;
		return ans;
	}

	int binarySearch1(vector<int>& nums, int start, int end, int target)
	{
		int mid;
		while (start <= end) {
			mid = (start + end) / 2;
			if (nums[mid] == target) {
				if ((mid > 0 && nums[mid - 1] != target) || mid == 0)
					return mid;
				else
					end = mid - 1;
			}
			else if (nums[mid] > target)
				end = mid - 1;
			else
				start = mid + 1;
		}
		return -1;
	}

	int binarySearch2(vector<int>& nums, int start, int end, int target)
	{
		int mid;
		while (start <= end) {
			mid = (start + end) / 2;
			if (nums[mid] == target) {
				if ((mid < nums.size() - 1 && nums[mid + 1] != target) || mid == nums.size() - 1)
					return mid;
				else
					start = mid + 1;
			}
			else if (nums[mid] > target)
				end = mid - 1;
			else
				start = mid + 1;
		}
		return -1;
	}
};
