class Solution {
public:
    bool validateStackSequences(vector<int>& pushed, vector<int>& popped) {
		stack<int> tStack;
		int id = 0;
		for (int num : pushed) {
			tStack.push(num);
			while (!tStack.empty() && tStack.top() == popped[id]) {
				tStack.pop();
				id++;
			}
		}
		return tStack.empty();
	}
};