class CQueue {
public:
	stack<int> stack1; // 用于入队的栈
	stack<int> stack2; // 用于出队的栈
	CQueue() {

	}

	void appendTail(int value) {
		stack1.push(value);
	}
	/*
	1.当栈 stack2 不为空： stack2中仍有已完成倒序的元素，因此直接返回 stack2 的栈顶元素。
	2.否则，当 stack1 为空： 即两个栈都为空，无元素，因此返回 -1。
	3.否则： 将栈 stack1 元素全部转移至栈 stack2 中，实现元素倒序，并返回栈 stack2 的栈顶元素。
	*/
	int deleteHead() {
		if (stack2.empty()) {
			while (!stack1.empty()) {
				stack2.push(stack1.top());
				stack1.pop();
			}
		}
		if (stack2.empty())
			return -1;
		else {
			int deleteItem = stack2.top();
			stack2.pop();
			return deleteItem;
		}
	}
};