class MinStack {
public:
	/** initialize your data structure here. */
	stack<int> stack1; // 正常出入栈
	stack<int> stack2; // 栈顶始终是stack1中的最小值
	MinStack() {

	}

	void push(int x) {
		stack1.push(x);
		// 注意这点必须是 >= 因为，最小值出栈后，还有一个与其相等的最小值
		// 而且要注意stack2.empty()要先判断，不然先执行stack2.top()会报访问越界错误
		if (stack2.empty() || stack2.top() >= x) 
			stack2.push(x);
	}

	void pop() {
		int t = stack1.top();
		stack1.pop();
		if (stack2.top() == t)
			stack2.pop();
	}

	int top() {
		return stack1.top();
	}

	int min() {
		return stack2.top();
	}
};