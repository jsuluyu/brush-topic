class Solution {
public:
	TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
		if (preorder.empty() || inorder.empty())
			return NULL;
		// 若只有根节点，则直接返回
		if (preorder.size() == 1) {
			TreeNode* root = new TreeNode(preorder[0]);
			return root;
		}
		// 前序遍历的第一个结点为根节点
		TreeNode* root = new TreeNode(preorder[0]);
		// 在中序遍历中寻找根节点的位置
		auto it = inorder.begin();
		while (*it != preorder[0])
			it++;
		// 左根树的大小
		int leftLen = it - inorder.begin();
		// 构造左子树的前序和中序序列
		vector<int> leftPre(preorder.begin()+1, preorder.begin() + leftLen + 1);
		vector<int> leftInor(inorder.begin(), inorder.begin() + leftLen);
		// 构造右子树的前序和中序序列
		vector<int> rightPre(preorder.begin() + leftLen + 1, preorder.end());
		vector<int> rightInor(it + 1, inorder.end());

		// 递归构造左右子树
		root->left = buildTree(leftPre, leftInor);
		root->right = buildTree(rightPre, rightInor);
		return root;
	}
};