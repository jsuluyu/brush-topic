class Solution {
public:
	vector<int> levelOrder(TreeNode* root) {
		if (root == NULL)
			return {};
		queue<TreeNode*> Q;
		vector<int> result;
		Q.push(root);
		while (!Q.empty()) {
			TreeNode* tNode = Q.front();
			Q.pop();
			result.push_back(tNode->val);
			if (tNode->left)
				Q.push(tNode->left);
			if (tNode->right)
				Q.push(tNode->right);
		}
		return result;
	}
};