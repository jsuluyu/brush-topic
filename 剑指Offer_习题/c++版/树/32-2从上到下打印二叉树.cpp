class Solution {
public:
	vector<vector<int>> levelOrder(TreeNode* root) {
		if (root == NULL)
			return {};
		int toBePrint = 1; // 当前行需要打印的结点数
		int nextLevel = 0; // 下一行的结点数
		vector<int> path;
		vector<vector<int>> result;
		queue<TreeNode*> Q;
		Q.push(root);
		while (!Q.empty()) {
			TreeNode* tNode = Q.front();
			Q.pop();
			path.push_back(tNode->val);
			toBePrint--;
			if (tNode->left) {
				Q.push(tNode->left);
				nextLevel++;
			}
			if (tNode->right) {
				Q.push(tNode->right);
				nextLevel++;
			}
			// 代表当前层的结点已经遍历结束
			if (toBePrint == 0) {
				toBePrint = nextLevel;
				nextLevel = 0;
				result.push_back(path);
				path.clear();
			}
		}
		return result;
	}
};