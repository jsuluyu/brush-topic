class Solution {
public:
	bool isSubStructure(TreeNode* A, TreeNode* B) {
		// 分两步：
		// step1: 在树A中找到和树B的根节点的值一样的结点R
		// step2: 判断树A中以R为根节点的子树是不是包含和树B一样的结构

		bool result = false;
		if (A != NULL && B != NULL) {
			if (A->val == B->val)
				result = checkSubTree(A, B);
			// 若A当前结点R的子结构与B不同，则递归查找A中与B相同的其他结点
			if (!result)
				result = isSubStructure(A->left, B);
			if (!result)
				result = isSubStructure(A->right, B);
		}
		return result;
	}

	bool checkSubTree(TreeNode* A, TreeNode* B) {
		if (B == NULL) // B遍历结束，说明都符合条件
			return true;
		if (A == NULL) // A为空了，B还未结束，肯定子结构不同
			return false;
		if (A->val != B->val)
			return false;
		return checkSubTree(A->left, B->left) && checkSubTree(A->right, B->right);
	}
};
