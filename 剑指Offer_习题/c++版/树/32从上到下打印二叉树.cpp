class Solution {
public:
	vector<vector<int>> levelOrder(TreeNode* root) {
		if (root == NULL)
			return {};
		int level = 1;
		vector<int> path;
		vector<vector<int>> result;
		stack<TreeNode*> Q1, Q2;
		Q1.push(root);
		while (!Q1.empty() || !Q2.empty()) {
			TreeNode* tNode;
			if (level % 2) {
				while (!Q1.empty()) {
					tNode = Q1.top();
					Q1.pop();
					path.push_back(tNode->val);
					if (tNode->left)
						Q2.push(tNode->left);
					if (tNode->right)
						Q2.push(tNode->right);
				}	
			}
			else {
				while (!Q2.empty()) {
					tNode = Q2.top();
					Q2.pop();
					path.push_back(tNode->val);
					if (tNode->right)
						Q1.push(tNode->right);
					if (tNode->left)
						Q1.push(tNode->left);
				}
			}
			level++;
			result.push_back(path);
			path.clear();
		}
		return result;
	}
};