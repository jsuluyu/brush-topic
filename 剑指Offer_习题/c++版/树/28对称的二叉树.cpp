class MinStack {
public:
	bool isSymmetric(TreeNode* root) {
		if (root == NULL)
			return true;
		return checkSubTree(root->left, root->right);
	}

	bool checkSubTree(TreeNode* lRoot, TreeNode* rRoot)
	{
		if (lRoot == NULL && rRoot == NULL)
			return true;
		if (lRoot == NULL || rRoot == NULL)
			return false;
		if (lRoot->val != rRoot->val)
			return false;
		return checkSubTree(lRoot->left, rRoot->right) && checkSubTree(lRoot->right, rRoot->left);
	}
};