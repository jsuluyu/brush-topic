class Solution {
public:
/* 思路:
  * 1.交换根节点
  * 2.递归交换左右子树
 */
	TreeNode* mirrorTree(TreeNode* root) {
		if (root == NULL)
			return NULL;
		TreeNode* temp = root->left;
		root->left = root->right;
		root->right = temp;

		if (root->left)
			mirrorTree(root->left);
		if (root->right)
			mirrorTree(root->right);
		return root;
	}
};