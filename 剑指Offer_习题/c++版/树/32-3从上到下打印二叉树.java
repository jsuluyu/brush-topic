class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        int level = 1;
        List<Integer> path = new ArrayList<>();
        List<List<Integer>> result = new ArrayList<>();
        if(root == null)
            return result;
        LinkedList<TreeNode> stack1 = new LinkedList<>();
        LinkedList<TreeNode> stack2 = new LinkedList<>();
        stack1.push(root);
        while(!stack1.isEmpty() || !stack2.isEmpty()) {
            TreeNode tNode;
            if(level % 2 != 0) {
                while(!stack1.isEmpty()) {
                    tNode = stack1.poll();
                    path.add(tNode.val);
                    if(tNode.left != null)
                        stack2.push(tNode.left);
                    if(tNode.right != null)
                        stack2.push(tNode.right);
                }
            }
            else {
                while(!stack2.isEmpty()) {
                    tNode = stack2.poll();
                    path.add(tNode.val);
                    if(tNode.right != null)
                        stack1.push(tNode.right);
                    if(tNode.left != null)
                        stack1.push(tNode.left);
                }
            }
            level++;
            result.add(path);
            path = new ArrayList<>();
        }
        return result;
    }
}