class Solution {
public:
    int singleNumber(vector<int>& nums) {
        // step1. 每个数占4个字节：32位，统计各个位出现的次数（每一位分别加起来）
        vector<int> bitSum(32, 0);
        for(int num : nums) {
            unsigned int bitMask = 1;
            for(int i=31; i>=0; i--) {
                int bit = num & bitMask;
                if(bit != 0)
                    bitSum[i] += 1;
                bitMask <<= 1; // 继续判断num的下一位
            }
        }

        // step2. 每一位对3取余，最终的余数之和就是结果，result每轮递增
        int result = 0;
        for(int i=0; i<32; i++) {
            result <<= 1;
            result += bitSum[i] % 3;
        }

        return result;
    }
};