// 法1：分治思想
class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        if(nums.empty())
            return 0;
        int curNum = 0;
        int maxNum = -9999;
        for(int i=0; i<nums.size(); i++) {
            if(curNum <= 0)
                curNum = nums[i];
            else
                curNum += nums[i];
            if(curNum > maxNum) {
                maxNum = curNum;
            }
        }
        return maxNum;
    }
};

// 法2：动态规划
class Solution {
public:
    int maxSubArray(vector<int>& nums) {
       vector<int> dp(nums.size(), 0);
       dp[0] = nums[0];
       int maxNum = dp[0];
       for(int i=1; i<nums.size(); i++) {
           dp[i] = nums[i] + max(dp[i - 1], 0);
           if(dp[i] > maxNum)
            maxNum = dp[i];
       }
       return maxNum;
    }
};