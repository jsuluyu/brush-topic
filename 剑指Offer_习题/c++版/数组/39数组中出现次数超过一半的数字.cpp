// 法1：hash表记录
class Solution {
public:
    int majorityElement(vector<int>& nums) {
        unordered_map<int, int> hashMap;
        for(int i=0; i<nums.size(); i++) {
            hashMap[nums[i]]++;
        }
        int threshold = nums.size() / 2;
        int ans;
        for(auto it=hashMap.begin(); it != hashMap.end(); it++) {
            if(it->second > threshold) {
                ans = it->first;
                break;
            }
        }
        return ans;
    }
};