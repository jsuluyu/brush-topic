class Solution {
public:
    int minArray(vector<int>& numbers) {
        if(numbers.size() < 0)
            return NULL;
        int start = 0;
        int end = numbers.size() - 1;
        int mid = start;
        while(numbers[start] >= numbers[end]) {
            if(start - end == 1) {
                mid = end;
                break;
            } 
            mid = (start + end) / 2;
            if(numbers[start] == numbers[mid] && numbers[mid] == numbers[end]) 
                return sequentialSearch(numbers, start, end);
            if(numbers[mid] >= numbers[start])
                start = mid;
            else if(numbers[mid] <= numbers[end])
                end = mid;
        }
        return numbers[mid];
    }

    int sequentialSearch(vector<int>& numbers, int start, int end)
    {
        int minNum = numbers[start];
        for(int i=start + 1; i <= end; i++) {
            if(minNum > numbers[i])
                minNum = numbers[i];
        }
        return minNum;
    }
};