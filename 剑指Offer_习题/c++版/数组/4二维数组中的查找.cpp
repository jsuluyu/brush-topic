class Solution {
public:
    bool findNumberIn2DArray(vector<vector<int>>& matrix, int target) {
        if(matrix.size() == 0)
            return 0;
        int row = 0;
        int col = matrix[0].size() - 1;
        while(row < matrix.size() && col >= 0) {
            if(matrix[row][col] == target)
                return true;
            // 若当前值大于traget，那么向左侧寻找
            if(matrix[row][col] > target) {
                col--;
            }
            // 若当前值小于target,那么就向下寻找
            else {
                row++;
            }
        }
        return false;
    }
};