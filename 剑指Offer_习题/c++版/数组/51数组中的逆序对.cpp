class Solution {
public:
    int reversePairs(vector<int>& nums) {
        if(nums.empty())
            return 0;
        vector<int> copy(nums.size());
        return mergeSort(nums, 0, nums.size()-1, copy);
    }

    int mergeSort(vector<int>& nums, int start, int end, vector<int>& temp)
    {
        if(start == end)
            return 0;
        int mid = (start + end) / 2;
        int maxLeft = mergeSort(nums, start, mid, temp); // 分裂过程
        int maxRight = mergeSort(nums, mid + 1, end, temp); // 分裂过程
        int maxLeftRight = merge(nums, start, end, temp); // 合并过程
        return maxLeft + maxRight + maxLeftRight;
    }

    int merge(vector<int>& nums, int start, int end, vector<int>& temp)
    {
        int i, j, k, m, n;
        int mid = (start + end) / 2;
        int sum = 0; // 不同子数组间的逆序对
        i = start; j = mid;
        m = mid + 1; n = end;
        k = 0;
        while(i <= j && m <= n) {
            if(nums[i] > nums[m]) {
                sum += (mid - i + 1); // 左边最小的大于右边的某个数，则左边全部的数都大于右边的那个数
                temp[k++] = nums[m];
                m++;
            }
            else {
                temp[k++] = nums[i];
                i++;
            }
        }
        while(i <= j)
            temp[k++] = nums[i++];
        while(m <= n)
            temp[k++] = nums[m++];
        for(i=0; i<k; i++)
            nums[start + i] = temp[i];
        return sum;
    }
};