// 法1：hash表记录
class Solution {
public:
    int findRepeatNumber(vector<int>& nums) {
        unordered_map<int, int> hashMap;
        for(int i=0; i<nums.size(); i++) {
            if(hashMap[nums[i]] == 0) 
                hashMap[nums[i]]++;
            else
                return nums[i];
        }
        return 0;
    }
};


// 法二：对应索引位置上的值应该等于索引
class Solution {
public:
    int findRepeatNumber(vector<int>& nums) {
        if(nums.size() == 0) {
            return 0;
        }
        for(int i=0; i<nums.size(); i++) {
            while(nums[i] != i) {
                if(nums[i] == nums[nums[i]])
                    return nums[i];
                swap(nums[i], nums[nums[i]]);
            }
        }
        return 0;
    }
};