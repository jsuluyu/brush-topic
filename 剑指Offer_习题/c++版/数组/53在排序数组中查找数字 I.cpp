// 法一：二分查找，起始位置和终点位置 复杂
class Solution {
public:
    int search(vector<int>& nums, int target) {
        if(nums.empty())
            return 0;
        int ans = 0;
        int len = nums.size();
        int start = getStart(nums, 0, len - 1, target);
        int end = getEnd(nums, 0, len - 1, target);
        if(start > -1 && end > -1)
            ans = end - start + 1;
        return ans;
    }

    int getStart(vector<int>& nums, int start, int end, int target)
    {
        int mid;
        while(start <= end) {
            mid = (start + end) / 2;
            if(nums[mid] == target) {
                if((mid > 0 && nums[mid - 1] != target) || mid == 0)
                    return mid;
                else
                    end = mid - 1;
            }
            else if(nums[mid] > target)
                end = mid - 1;
            else 
                start = mid + 1;
        }
        return -1;
    }

    int getEnd(vector<int>& nums, int start, int end, int target)
    {
        int mid;
        while(start <= end) {
            mid = (start + end) / 2;
            if(nums[mid] == target) {
                if((mid < nums.size() - 1 && nums[mid + 1] != target) || mid == nums.size() - 1)
                    return mid;
                else
                    start = mid + 1;
            }
            else if(nums[mid] > target)
                end = mid - 1;
            else 
                start = mid + 1;
        }
        return -1;
    }
};