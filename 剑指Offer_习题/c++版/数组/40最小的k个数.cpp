// 利用大根堆
class Solution {
public:
    vector<int> getLeastNumbers(vector<int>& arr, int k) {
        vector<int> ans;
        priority_queue<int> Q;
        if(arr.empty() || k <= 0)
            return ans;
        for(int i=0; i<k; i++) 
            Q.push(arr[i]);
        for(int i=k; i<arr.size(); i++) {
            if(Q.top() > arr[i]) {
                Q.pop();
                Q.push(arr[i]);
            }
        }
        for(int i=0; i<k; i++) {
            ans.push_back(Q.top());
            Q.pop();
        }
        return ans;
    }
};