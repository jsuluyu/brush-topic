class Solution {
public:
    vector<int> constructArr(vector<int>& a) {
        if(a.empty())
            return {};
        vector<int> B(a.size());
        B[0] = 1;
        // 计算上三角的值
        for(int i=1; i<a.size(); i++) {
            B[i] = B[i - 1] * a[i - 1];
        }
        // 计算下三角的值
        int temp = 1;
        for(int i=a.size() - 2; i>= 0; i--) {
            temp *= a[i + 1];
            B[i] *= temp;
        }
        return B;
    }
};