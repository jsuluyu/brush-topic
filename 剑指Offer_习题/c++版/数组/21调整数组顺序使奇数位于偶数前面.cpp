class Solution {
public:
    vector<int> exchange(vector<int>& nums) {
        if(nums.size() == 0)
            return nums;
        int start = 0;
        int end = nums.size() - 1;

        while(start < end) {
            while(start < end && nums[start] % 2 != 0)
                start++;
            while(start < end && nums[end] % 2 == 0)
                end--;
            if(start < end)
                swap(nums[start], nums[end]);
        }
        
        return nums;
    }
};