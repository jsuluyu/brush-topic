class Solution {
public:
    vector<int> singleNumbers(vector<int>& nums) {
        // step1. 将数组全部依次异或，得到异或后的结果
        int resultOr = 0;
        for(int num : nums) {
            resultOr ^= num;
        }

        // step2. 找到异或结果中的第一个1
        int mask = 1;
        while((mask & resultOr) == 0)
            mask <<= 1;

        // step3. 将原始数组，分成2个数组
        vector<int> ans(2, 0);
        for(int num : nums) {
            if((num & mask) == 0) // 注意单目运算符的优先级低于 == ,括号一定要加
                ans.at(0) ^= num;
            else
                ans.at(1) ^= num;
        }

        return ans;
    }
};