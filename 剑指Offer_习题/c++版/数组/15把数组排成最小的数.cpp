class Solution {
public:
    string minNumber(vector<int>& nums) {
        string ans = "";
        vector<string> strVec;
        for(int i=0; i<nums.size(); i++) {
            strVec.push_back(to_string(nums[i]));
        }
        sort(strVec.begin(), strVec.end(), cmp);
        for(int i=0; i<strVec.size(); i++)
            ans += strVec[i];
        return ans;
    }

    static bool cmp(string s1, string s2) 
    {
        string newStr12 = s1 + s2;
        string newStr21 = s2 + s1;
        if(newStr12 < newStr21)
            return true;
        else 
            return false;
    }
};