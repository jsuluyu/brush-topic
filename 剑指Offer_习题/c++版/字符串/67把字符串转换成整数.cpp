class Solution {
public:
	int strToInt(string str) {
		int id = 0;
		int flag = 1;
		int ans = 0;

		// 去除开头多于空格
		while (id < str.length() && str[id] == ' ') id++;
		if (str[id] == '-') flag = -1;
		if (str[id] == '-' || str[id] == '+') id++;

		while (id < str.length() && isdigit(str[id])) {
			int temp = str[id] - '0';
			if (ans > INT_MAX / 10 || (ans == INT_MAX / 10 && temp > 7))
				return flag == 1 ? INT_MAX : INT_MIN;
			ans = ans * 10 + temp;
			id++;
		}
		return ans * flag;
	}
};