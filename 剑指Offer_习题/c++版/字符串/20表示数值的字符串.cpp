class Solution {
public:
    // 正则表达式：[+|-A][.B][e|E][C]
    bool isNumber(string s) {
        if(s.empty())
            return false;
        int idx = 0; // 判断的索引
        // 去除s前面的空格
        while(s[idx] == ' ') idx++;

        // 判断s前面的整数部分A
        bool numeric = scanInteger(s, idx);

        // 判断小数点后面的的小数部分B
        if(s[idx] == '.') {
            // 1.有小数但是小数点前没有整数 对应 scanUnsignedInteger(&s)
            // 2.没有小数但是有整数 对应 numeric
            // 3.既有小数又有整数 对应 scanUnsignedInteger(&s) || numeric
            // 三者满足其一就好，所以是 ||
            idx++;
            numeric = scanUnsignedInteger(s, idx) || numeric; // 必须先判断sacn...因为要移动指针
        }

        // 检查C
        if(s[idx] == 'e' || s[idx] == 'E') {
            // 1.e或E前面必须有数字
            // 2.e或E后面也必须有数字
            // 二者必须同时满足，所以 &&
            idx++;
            numeric = numeric && scanInteger(s, idx);
        }

        // 去除s后面的字符串
        while(s[idx] == ' ') idx++;
        return numeric && s[idx] == '\0';
    }

    bool scanInteger(string& s, int& id) 
    {
        if(s[id] == '+' || s[id] == '-')
            id++;
        return scanUnsignedInteger(s, id);
    }

    bool scanUnsignedInteger(string& s, int& id)
    {
        int oldId = id;
        while(id <= s.size() && s[id] >= '0' && s[id] <= '9')
            id++;
        return id > oldId; // 索引移动了，代表存在数字
    }
};