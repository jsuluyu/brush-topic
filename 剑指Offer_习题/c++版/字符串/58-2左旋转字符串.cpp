// 法1：新建字符串
class Solution {
public:
	string reverseLeftWords(string s, int n) {
		string str;
		str = s.substr(n, s.length() - n);
		str += s.substr(0, n);
		return str;
	}
};