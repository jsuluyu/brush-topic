class Solution {
public:
	char firstUniqChar(string s) {
		unordered_map<char, int> hashMap;
		for (char ch : s) {
			hashMap[ch]++;
		}
		for (char ch : s) {
			if (hashMap[ch] == 1)
				return ch;
		}
		return ' ';
	}
};