class Solution {
public:
    int strToInt(string str) {
        if(str.size() == 0)
            return 0;
        int len = str.size();
        int flag = 1; // 正负数判断
        int start = 0;
        int ans = 0; // 最终的结果

        while(str[start] == ' ') start++; // 去除前面的空格
        if(str[start] == '-') flag = -1;
        if(str[start] == '-' || str[start] == '+') start++;

        while(start < len && isdigit(str[start])) {
            int temp = str[start] - '0';
            if(ans > INT_MAX/10 || (ans == INT_MAX/10 && temp >7)) // 判断是否溢出
                return flag == -1 ? INT_MIN : INT_MAX;
            ans = ans * 10 + temp;
            start++;
        } 
        return ans * flag;
    }
};