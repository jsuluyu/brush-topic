// 法1：开辟一个新的字符串
class Solution {
public:
    string replaceSpace(string s) {
        string str = "";
        for(int i=0; i<s.length(); i++) {
            if(s[i] != ' ') 
                str += s[i];
            else
                str += "%20";
        }
        return str;
    }
};


// 法2：在原数组上进行修改
class Solution {
public:
    string replaceSpace(string s) {
        int oldLen = s.length();
        int blackCount = 0;
        for(int i=0; i<oldLen; i++) {
            if(s[i] == ' ')
                blackCount++;
        }
        int newLen = oldLen + blackCount * 2;
        s.resize(newLen);
        for(int i=oldLen, j=newLen; i<j; i--, j--) {
            if(s[i] == ' ') {
                s[j] = '0';
                s[j-1] = '2';
                s[j-2] = '%';
                j -= 2;
            }
            else
                s[j] = s[i];
        }
        return s;
    }
};