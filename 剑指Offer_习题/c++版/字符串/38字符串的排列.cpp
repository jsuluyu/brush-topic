// 法1：回溯法
class Solution {
public:
    string result;
	vector<string> candidate;

	void dfs(string& s, vector<int>& vis)
	{
		if (result.size() == s.size()) {
			candidate.push_back(result);
			return;
		}
		for (int i = 0; i < s.size(); i++) {
			if (i > 0 && s[i - 1] == s[i] && vis[i - 1] == 0) // 去除同一层的相同元素
				continue;
			if (vis[i] == 0) {
				vis[i] = 1;
				result.push_back(s[i]);
				dfs(s, vis);
				result.pop_back();
				vis[i] = 0;
			}
		}
	}

	vector<string> permutation(string s) {
		vector<int> vis(s.size(), 0);
		sort(s.begin(), s.end());
		dfs(s, vis);
		return candidate;
	}
};