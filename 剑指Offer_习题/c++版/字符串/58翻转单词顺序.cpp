class Solution {
public:
    string reverseWords(string s) {
		int left = 0, right = s.length() - 1;
		while (right >= 0 && s[right] == ' ') right--;
		while (left <= right && s[left] == ' ') left++;
		//s = s.substr(left, right - left + 1);

		string str = "";
		stack<string> st;
		for (int i = left; i <= right; i++) {
			if (s[i] != ' ') {
				str += s[i];
			}
			else {
				while (s[i] == ' ') i++;
				i--;
				st.push(str);
				str = "";
			}
		}
        st.push(str);
		str = "";
		while (!st.empty()) {
			str += st.top();
			st.pop();
			if (!st.empty())
				str += ' ';
		}
		return str;
	}
};