class Solution {
public:
    vector<int> printNumbers(int n) {
       int maxNum = pow(10, n);
       vector<int> vArr(maxNum - 1);
       for(int i=1; i<maxNum; i++) {
           vArr[i-1] = i;
       }
       return vArr;
    }
};