/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode reverseList(ListNode head) {
        ListNode pNode = head;
        ListNode pNext = null;
        ListNode pPre = null;
        ListNode pReverse = null;
        while(pNode != null) {
            pNext = pNode.next;
            if(pNext == null) 
                pReverse = pNode;
            pNode.next = pPre;
            pPre = pNode;
            pNode = pNext;
        }
        return pReverse;
    }
}