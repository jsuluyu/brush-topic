/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */

// 法1：利用栈
public class Solution {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        Stack<ListNode> stackA = new Stack<>();
        Stack<ListNode> stackB = new Stack<>();
        ListNode pNodeA = headA;
        ListNode pNodeB = headB;
        while(pNodeA != null) {
            stackA.push(pNodeA);
            pNodeA = pNodeA.next;
        }
        while(pNodeB != null) {
            stackB.push(pNodeB);
            pNodeB = pNodeB.next;
        }
        ListNode sameNode = null;
        while(!stackA.empty() && !stackB.empty()) {
            pNodeA = stackA.pop();
            pNodeB = stackB.pop();
            if(pNodeA != pNodeB)
                break;
            sameNode = pNodeA;
        }
        return sameNode;
    }
}