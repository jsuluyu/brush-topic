/*
// Definition for a Node.
class Node {
    int val;
    Node next;
    Node random;

    public Node(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}
*/

// 法1：map存储
class Solution {
    public Node copyRandomList(Node head) {
        if(head == null)
            return null;
        HashMap<Node, Node> map = new HashMap<>();
        Node cur = head;
        while(cur != null) {
            map.put(cur, new Node(cur.val));
            cur = cur.next;
        }
        cur = head;
        while(cur != null) {
            map.get(cur).next = map.get(cur.next);
            map.get(cur).random = map.get(cur.random);
            cur = cur.next;
        }
        return map.get(head);
    }
}


// 法2：插入重排
/*
// Definition for a Node.
class Node {
    int val;
    Node next;
    Node random;

    public Node(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}
*/
class Solution {
    public void cloneNode(Node head) {
        Node pNode = head;
        while(pNode != null) {
            Node pClone = new Node(pNode.val);
            pClone.next = pNode.next;
            pClone.random = null;

            pNode.next = pClone;
            pNode = pClone.next;
        }
    }

    public void cloneRandom(Node head) {
        Node pNode = head;
        while(pNode != null) {
            Node pClone = pNode.next;
            if(pNode.random != null)
                pClone.random = pNode.random.next;
            pNode = pClone.next;
        }
    }

    public Node reconnectNodes(Node head) {
        Node pNode = head;
        Node pCloneNode = null;
        Node pCloneHead = null;

        if(pNode != null) {
            pCloneHead = pCloneNode = pNode.next;
            pNode.next = pCloneHead.next;
            pNode = pNode.next;
        }
        while(pNode != null) {
            pCloneNode.next = pNode.next;
            pCloneNode = pCloneNode.next;
            pNode.next = pCloneNode.next;
            pNode = pNode.next;
        }
        return pCloneHead;
    }

    public Node copyRandomList(Node head) {
        if(head == null)
            return null;
        cloneNode(head);
        cloneRandom(head);
        return reconnectNodes(head);
    }
}