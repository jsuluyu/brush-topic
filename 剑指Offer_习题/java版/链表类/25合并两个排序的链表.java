/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode pMergeHead = new ListNode(0);
        ListNode p1 = l1;
        ListNode p2 = l2;
        ListNode cur = pMergeHead;
        while(p1 != null && p2 != null) {
            if(p1.val > p2.val) {
                cur.next = p2;
                p2 = p2.next;
            }
            else {
                cur.next = p1;
                p1 = p1.next;
            }
            cur = cur.next;
        }
        cur.next = p1 != null ? p1 : p2;
        return pMergeHead.next;
    }
}