/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public int[] reversePrint(ListNode head) {
        Stack<ListNode> stack = new Stack();
        ListNode p = head;
        while(p != null) {
            stack.push(p);
            p = p.next;
        }
        int size = stack.size();
        int[] ans = new int[size];
        for(int i=0; i<size; i++) {
            ans[i] = stack.pop().val;
        }
        return ans;
    }
}