/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode getKthFromEnd(ListNode head, int k) {
        if(head == null || k == 0)
            return null;
        ListNode pBefore = head;
        ListNode pAfter = head;
        for(int i=0; i<k-1; i++) {
            pAfter = pAfter.next;
        }
        while(pAfter.next != null) {
            pAfter = pAfter.next;
            pBefore = pBefore.next;
        }
        return pBefore;
    }
}