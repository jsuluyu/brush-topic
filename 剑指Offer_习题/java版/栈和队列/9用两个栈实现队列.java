// Stack是线程安全的，性能较差
class CQueue {

    public Stack<Integer> stack1 = new Stack<>();
    public Stack<Integer> stack2 = new Stack<>();
    public CQueue() {

    }

    public void appendTail(int value) {
        stack2.push(value);
    }

    public int deleteHead() {
        if(stack1.isEmpty()) {
            while(!stack2.isEmpty()) {
                stack1.push(stack2.peek());
                stack2.pop();
            }
        }
        if(stack1.isEmpty())
            return -1;
        else {
            int deleteItem = stack1.peek();
            stack1.pop();
            return deleteItem;
        }
    }
}


// LinkedList可模拟队列和栈
class CQueue {

    public LinkedList<Integer> stack1 = new LinkedList<>();
    public LinkedList<Integer> stack2 = new LinkedList<>();
    public CQueue() {

    }

    public void appendTail(int value) {
        stack2.addLast(value);
    }

    public int deleteHead() {
        if(stack1.isEmpty()) {
            while(!stack2.isEmpty()) {
                stack1.addLast(stack2.peek());
                stack2.remove();
            }
        }
        if(stack1.isEmpty())
            return -1;
        else {
            int deleteItem = stack1.getFirst();
            stack1.remove();
            return deleteItem;
        }
    }
}