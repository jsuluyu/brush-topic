class MinStack {

    /** initialize your data structure here. */
    LinkedList<Integer> stack1;
    LinkedList<Integer> stack2;
    public MinStack() {
        stack1 = new LinkedList<>();
        stack2 = new LinkedList<>();
    }

    public void push(int x) {
        stack1.push(x);
        if(stack2.isEmpty() || stack2.peek() >= x)
            stack2.push(x);
    }

    public void pop() {
        int t = stack1.pop();
        if(t == stack2.peek())
            stack2.pop();
    }

    public int top() {
        return stack1.peek();
    }

    public int min() {
        return stack2.peek();
    }
}