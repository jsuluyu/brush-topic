class Solution {
    public boolean validateStackSequences(int[] pushed, int[] popped) {
        LinkedList<Integer> stack = new LinkedList<>();
        int id = 0;
        for(int num : pushed) {
            stack.push(num); // 入栈
            // 若当前栈顶元素==popped出栈元素，则出栈
            while(!stack.isEmpty() && stack.peek() == popped[id]) {
                stack.pop();
                id++;
            }
        }
        return stack.isEmpty(); // 若正常出栈完毕，则符合
    }
}