class Solution {
    public String reverseLeftWords(String s, int n) {
        StringBuilder str = new StringBuilder();
        for(int i=n; i<s.length(); i++) {
            str.append(s.charAt(i));
        }
        for(int i=0; i<n; i++) {
            str.append(s.charAt(i));
        }
        return str.toString();
    }
}