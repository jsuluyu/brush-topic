class Solution {
    private StringBuilder candidate = new StringBuilder();
    private ArrayList<String> result = new ArrayList<>();

    public String[] permutation(String s) {
        boolean[] vis = new boolean[s.length()];
        Arrays.fill(vis, false);
        char[] chars = s.toCharArray();
        Arrays.sort(chars);
        s = new String(chars);
        dfs(s, vis);
        return result.toArray(new String[result.size()]);
    }

    public void dfs(String s, boolean[] vis) {
        if (s.length() == candidate.length()) {
            result.add(candidate.toString());
            return;
        }
        for (int i = 0; i < s.length(); i++) {
            if (i > 0 && s.charAt(i) == s.charAt(i - 1) && vis[i - 1] == false)
                continue;
            if(vis[i] == false) {
                vis[i] = true;
                candidate.append(s.charAt(i));
                dfs(s, vis);
                candidate.deleteCharAt(candidate.length() - 1);
                vis[i] = false;
            }
        }
    }
}