class Solution {
    private int idx = 0; // 由于java对于基本数据类型只有值传递，因此只能通过设置成员变量的方式来改变该值

    public boolean isNumber(String s) {
        if(s.isEmpty())
            return false;
        while(idx < s.length() && s.charAt(idx) == ' ') idx++;

        boolean numeric = scanInteger(s);

        if(idx < s.length() && s.charAt(idx) == '.') {
            idx++;
            numeric = scanUnsignedInteger(s) || numeric;
        }

        if(idx < s.length() && (s.charAt(idx) == 'e' || s.charAt(idx) == 'E')) {
            idx++;
            numeric = numeric && scanInteger(s);
        }

        while(idx < s.length() && s.charAt(idx) == ' ') idx++;

        return numeric && idx == s.length();
    }

    boolean scanUnsignedInteger(String s) {
        Integer old = idx;
        while(idx < s.length() && s.charAt(idx) >= '0' && s.charAt(idx) <= '9')
            idx++;
        return idx > old;
    }

    boolean scanInteger(String s) {
        if(idx < s.length() && (s.charAt(idx) == '+' || s.charAt(idx) == '-'))
            idx++;
        return scanUnsignedInteger(s);
    }
}