class Solution {
    public char firstUniqChar(String s) {
        HashMap<Character, Integer> hashMap = new HashMap<>();
        for(int i=0; i<s.length(); i++) {
            char ch = s.charAt(i);
            hashMap.put(ch, hashMap.getOrDefault(ch, 0) + 1);
        }
        for(int i=0; i<s.length(); i++) {
            if(hashMap.get(s.charAt(i)) == 1)
                return s.charAt(i);
        }
        return ' ';
    }
}