class Solution {
    public int strToInt(String str) {
        int id = 0;
        int flag = 1;
        int len = str.length();
        int ans = 0;
        while (id < len && str.charAt(id) == ' ') id++;
        if (id < len && str.charAt(id) == '-') flag = -1;
        if (id < len && (str.charAt(id) == '-' || str.charAt(id) == '+')) id++;

        while (id < len && str.charAt(id) >= '0' && str.charAt(id) <= '9') {
            int temp = str.charAt(id) - '0';
            if (ans > Integer.MAX_VALUE / 10 || (ans == Integer.MAX_VALUE / 10 && temp > 7))
                return flag == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
            ans = ans * 10 + temp;
            id++;
        }
        return ans * flag;
    }
}