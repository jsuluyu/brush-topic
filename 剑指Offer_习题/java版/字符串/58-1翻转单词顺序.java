class Solution {
    public String reverseWords(String s) {
        int left = 0;
        int right = s.length() - 1;
        // 去除字符串前后的空格
        while(right >=0 && s.charAt(right) == ' ') right--;
        while(left <= right && s.charAt(left) == ' ') left++;

        // 开始翻转字符串
        StringBuilder str = new StringBuilder();
        Stack<String> stack = new Stack<>();
        for(int i=left; i<=right; i++) {
            char ch = s.charAt(i);
            if(ch != ' ') {
                str.append(ch);
            } else {
                while(s.charAt(i) == ' ') i++;
                i--;
                stack.push(str.toString());
                str = new StringBuilder();
            }
        }
        stack.push(str.toString());
        str = new StringBuilder();
        while(!stack.isEmpty()) {
            str.append(stack.peek());
            stack.pop();
            if(!stack.isEmpty())
                str.append(" ");
        }
        return str.toString();
    }
}

// 法2：思想一样，只是调用函数不一样
public String reverseWords(String s) {
        String[] strs = s.trim().split(" ");
        StringBuilder str = new StringBuilder();
        for (int i = strs.length - 1; i >= 0; i--) {
            if(strs[i].equals("")) continue;
            str.append(strs[i] + " ");
        }
        return str.toString().trim();
    }