public class Solution {
    public int[] exchange(int[] nums) {
        if(nums.length == 0)
            return nums;
        int start = 0;
        int end = nums.length - 1;

        while(start < end) {
            while(start < end && nums[start] % 2 != 0)
                start++;
            while(start < end && nums[end] % 2 == 0)
                end--;
            if(start < end) {
                int temp = nums[start];
                nums[start] = nums[end];
                nums[end] = temp;
            }
        }
        return nums;
    }
}