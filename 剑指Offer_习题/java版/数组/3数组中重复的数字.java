// 法1：哈希表记录
public class Solution {
    public int findRepeatNumber(int[] nums) {
        HashSet<Integer> hashSet = new HashSet<>();
        for(int i=0; i<nums.length; i++) {
            if(hashSet.contains(nums[i]))
                return nums[i];
            hashSet.add(nums[i]);
        }
        return 0;
    }
}

// 法二：对应索引位置上的值应该等于索引
class Solution {
    public int findRepeatNumber(int[] nums) {
        if(nums.length == 0)
            return 0;
        for(int i=0; i<nums.length; i++) {
            while(nums[i] != i) {
                if(nums[i] == nums[nums[i]])
                    return nums[i];
                int t = nums[i];
                nums[i] = nums[t];
                nums[t] = t;
            }
        }
        return 0;
    }
}