public class Solution {
    public int search(int[] nums, int target) {
        if(nums.length == 0)
            return 0;
        int ans = 0;
        int start = getStart(nums, 0, nums.length - 1, target);
        int end = getEnd(nums, 0, nums.length - 1, target);
        if(start > -1 && end > -1)
            ans = end - start + 1;
        return ans;
    }

    public int getStart(int[] nums, int start, int end, int target) {
        int mid;
        while(start <= end) {
            mid = (start + end) / 2;
            if(nums[mid] == target) {
                if((mid > 0 && nums[mid - 1] != target) || mid == 0)
                    return mid;
                else
                    end = mid - 1;
            }
            else if(nums[mid] > target)
                end = mid - 1;
            else
                start = mid + 1;
        }
        return -1;
    }

    public int getEnd(int[] nums, int start, int end, int target) {
        int mid;
        while (start <= end) {
            mid = (start + end) / 2;
            if(nums[mid] == target) {
                if((mid < nums.length - 1 && nums[mid + 1] != target) || mid == nums.length - 1)
                    return mid;
                else
                    start = mid + 1;
            }
            else if(nums[mid] > target)
                end = mid - 1;
            else
                start = mid + 1;
        }
        return -1;
    }
}