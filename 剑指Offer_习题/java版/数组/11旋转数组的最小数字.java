public class Solution {
    public int minArray(int[] numbers) {
        if (numbers.length == 0)
            return 0;
        int start = 0;
        int end = numbers.length - 1;
        int mid = start;
        while (numbers[start] >= numbers[end]) {
            if (end - start == 1) {
                mid = end;
                break;
            }
            mid = (start + end) / 2;
            if (numbers[start] == numbers[mid] && numbers[mid] == end)
                return search(numbers, start, end);
            if (numbers[mid] >= numbers[start])
                start = mid;
            else if (numbers[mid] <= numbers[end])
                end = mid;
        }
        return numbers[mid];
    }

    int search(int[] numbers, int start, int end) {
        int minNum = numbers[start];
        for (int i = start + 1; i <= end; i++) {
            if(minNum > numbers[i])
                minNum = numbers[i];
        }
        return minNum;
    }
}