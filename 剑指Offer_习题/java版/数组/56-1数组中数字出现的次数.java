public class Solution {
    public int[] singleNumbers(int[] nums) {
        // step1:全部异或，得到2个不同值的异或结果
        int sum = 0;
        for(int num : nums)
            sum ^= num;
        // step2:找到一个标志位，作为将数组分为2份的依据
        int mask = 1;
        while((mask & sum) == 0)
            mask <<= 1;
        // step3:将原数组分为2份，2个不同的数字分别在2份不同的数组中
        int[] ans = new int[2];
        for(int num : nums) {
            if((num & mask) == 0)
                ans[0] ^= num;
            else
                ans[1] ^= num;
        }
        return ans;
    }
}