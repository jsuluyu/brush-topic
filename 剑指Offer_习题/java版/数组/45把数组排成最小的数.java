public class Solution {
    public String minNumber(int[] nums) {
        StringBuilder ans = new StringBuilder();
        List<String> list = new ArrayList();
        for(int i=0; i<nums.length; i++)
            list.add(String.valueOf(nums[i]));
        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                String newStr12 = s1 + s2;
                String newStr21 = s2 + s1;
                return newStr12.compareTo(newStr21);
            }
        });
        for(String s : list)
            ans.append(s);
        return ans.toString();
    }
}