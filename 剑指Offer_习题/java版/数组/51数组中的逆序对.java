public class Solution {
    public int reversePairs(int[] nums) {
        if(nums.length == 0)
            return 0;
        int[] copy = new int[nums.length];
        return mergeSort(nums, 0, nums.length-1, copy);
    }

    int mergeSort(int[] nums, int start, int end, int[] copy) {
        if(start == end)
            return 0;
        int mid = (start + end) / 2;
        int maxLeft = mergeSort(nums, start, mid, copy);
        int maxRight = mergeSort(nums, mid + 1, end, copy);
        int maxLeftRight = merge(nums, start, end, copy);
        return maxLeft + maxRight + maxLeftRight;
    }

    int merge(int[] nums, int start, int end, int[] copy) {
        int i, j, k, m, n;
        int mid = (start + end) / 2;
        int sum = 0;
        k = 0;
        i = start; j = mid;
        m = mid + 1; n = end;
        while(i <= j && m <= n) {
            if(nums[i] > nums[m]) {
                sum += (mid - i + 1);
                copy[k++] = nums[m];
                m++;
            }
            else {
                copy[k++] = nums[i++];
            }
        }
        while(i <= j)
            copy[k++] = nums[i++];
        while (m <= n)
            copy[k++] = nums[m++];
        for(i=0; i<k; i++)
            nums[start + i] = copy[i];
        return sum;
    }
}