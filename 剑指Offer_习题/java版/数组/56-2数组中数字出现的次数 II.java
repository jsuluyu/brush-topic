public class Solution {
    public int singleNumber(int[] nums) {
        int[] bitSum = new int[32];
        for(int num : nums) {
            int bitMask = 1;
            for(int i=31; i>=0; i--) {
                int bit = num & bitMask;
                if(bit != 0)
                    bitSum[i] += 1;
                bitMask <<= 1;
            }
        }

        int result = 0;
        for(int i=0; i<32; i++) {
            result <<= 1;
            result += bitSum[i] % 3;
        }
        return result;
    }
}