// 法1：hash表记录
public class Solution {
    public int majorityElement(int[] nums) {
        int ans = 0;
        int threshold = nums.length / 2;
        HashMap<Integer, Integer> hashMap = new HashMap();
        for(int i=0; i<nums.length; i++) {
            if(!hashMap.containsKey(nums[i]))
                hashMap.put(nums[i], 1);
            else
                hashMap.put(nums[i], hashMap.get(nums[i]) + 1);
        }
        for(Integer key : hashMap.keySet()) {
            if(hashMap.get(key) > threshold) {
                ans = key;
                break;
            }
        }
        return ans;
    }
}