// 法1：分治思想
public class Solution {
    public int maxSubArray(int[] nums) {
        if(nums.length == 0)
            return 0;
        int curNum = 0;
        int maxNum = -99999;
        for(int i=0; i<nums.length; i++) {
            if(curNum <= 0)
                curNum = nums[i];
            else
                curNum += nums[i];
            if(curNum > maxNum)
                maxNum = curNum;
        }
        return maxNum;
    }
}

// 法2：动态规划
public class Solution {
    public int maxSubArray(int[] nums) {
        if(nums.length == 0)
            return 0;
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        int maxNum = dp[0];
        for(int i=1; i<nums.length; i++) {
            dp[i] = nums[i] + Math.max(dp[i - 1], 0);
            maxNum = maxNum > dp[i] ? maxNum : dp[i];
        }
        return maxNum;
    }
}