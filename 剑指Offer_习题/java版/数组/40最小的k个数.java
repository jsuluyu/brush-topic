public class Solution {
    public int[] getLeastNumbers(int[] arr, int k) {
        int[] ans = new int[k];
        if(arr.length == 0 || k <= 0)
            return ans;
        Queue<Integer> queue = new PriorityQueue<Integer>(new Comparator<Integer>() {
            @Override
            public int compare(Integer num1, Integer num2) {
                return num2 - num1; // 从大到小排序
            }
        });
        for(int i=0; i<k; i++) {
            queue.offer(arr[i]);
        }
        for(int i=k; i<arr.length; i++) {
            if(queue.peek() > arr[i]) {
                queue.poll();
                queue.offer(arr[i]);
            }
        }
        for(int i=0; i<k; i++) {
            ans[i] = queue.peek();
            queue.poll();
        }
        return ans;
    }
}