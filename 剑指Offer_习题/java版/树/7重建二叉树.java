class Solution {
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if(preorder.length == 0 || inorder.length == 0)
            return null;
        // 若只有根节点，则直接返回
        if(preorder.length == 1) {
            TreeNode root = new TreeNode(preorder[0]);
            return root;
        }
        // 前序遍历的第一个结点为根节点
        TreeNode root = new TreeNode(preorder[0]);
        // 在中序遍历中找到根节点的位置，划分左右子树
        int rootIdx = 0;
        for(int i=0; i<inorder.length; i++) {
            if(root.val == inorder[i]) {
                rootIdx = i;
                break;
            }
        }
        // 左子树的大小
        int leftLen = rootIdx;
        // 构造左子树的前序和中序序列
        int[] leftPre = Arrays.copyOfRange(preorder, 1, leftLen + 1);
        int[] leftInor = Arrays.copyOfRange(inorder, 0, leftLen);
        // 构造右子树的前序和中序遍历
        int[] rightPre = Arrays.copyOfRange(preorder, leftLen + 1, preorder.length);
        int[] rightInor = Arrays.copyOfRange(inorder, leftLen+1, inorder.length);

        // 递归构造左右子树
        root.left = buildTree(leftPre, leftInor);
        root.right = buildTree(rightPre, rightInor);
        return root;
    }
}