class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        int toBePrint = 1;
        int nextLevel = 0;
        ArrayList<Integer> path = new ArrayList<>();
        List<List<Integer>> result = new ArrayList<>();
        if(root == null)
            return result;
        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while(!queue.isEmpty()) {
            TreeNode tNode = queue.poll();
            path.add(tNode.val);
            toBePrint--;
            if(tNode.left != null) {
                queue.offer(tNode.left);
                nextLevel++;
            }
            if(tNode.right != null) {
                queue.offer(tNode.right);
                nextLevel++;
            }
            if(toBePrint == 0) {
                toBePrint = nextLevel;
                nextLevel = 0;
                result.add(path);
                path = new ArrayList<>();
            }
        }
        return result;
    }
}