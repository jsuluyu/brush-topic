public class Solution {
    public boolean isSymmetric(TreeNode root) {
        if(root == null)
            return true;
        return checkSubTree(root.left, root.right);
    }

    public boolean checkSubTree(TreeNode lRoot, TreeNode rRight) {
        if(lRoot == null && rRight == null)
            return true;
        if(lRoot == null || rRight == null)
            return false;
        if(lRoot.val != rRight.val)
            return false;
        return checkSubTree(lRoot.left, rRight.right) && checkSubTree(lRoot.right, rRight.left);
    }
}