class Solution {
    public boolean isSubStructure(TreeNode A, TreeNode B) {
        boolean result = false;
        if(A != null && B != null) {
            if(A.val == B.val)
                result = checkSubTree(A, B);
            if(!result)
                result = isSubStructure(A.left, B);
            if(!result)
                result = isSubStructure(A.right, B);
        }
        return result;
    }

    public boolean checkSubTree(TreeNode A, TreeNode B) {
        if(B == null)
            return true;
        if(A == null)
            return false;
        if(A.val != B.val)
            return false;
        return checkSubTree(A.left, B.left) && checkSubTree(A.right, B.right);
    }
}