class Solution {
    public int[] levelOrder(TreeNode root) {
        if(root == null)
            return new int[0];
        ArrayList<Integer> list = new ArrayList<>();
        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while(!queue.isEmpty()) {
            TreeNode tNode = queue.poll();
            list.add(tNode.val);
            if(tNode.left != null)
                queue.offer(tNode.left);
            if(tNode.right != null)
                queue.offer(tNode.right);
        }
        int[] result = new int[list.size()];
        for(int i=0; i<list.size(); i++)
            result[i] = list.get(i);
        return result;
    }
}