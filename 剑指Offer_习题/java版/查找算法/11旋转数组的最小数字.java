public class Solution {
    public int minArray(int[] numbers) {
        if(numbers.length == 0)
            return 0;
        int start = 0;
        int end = numbers.length - 1;
        int mid = start;
        while(numbers[start] >= numbers[end]) {
            if(start == end - 1) {
                mid = end;
                break;
            }
            mid = (start + end) >> 1;
            if(numbers[start] == numbers[mid] && numbers[mid] == numbers[end])
                return linerSearch(numbers);
            if(numbers[mid] >= numbers[end])
                start = mid;
            else if(numbers[mid] <= numbers[start])
                end = mid;
        }
        return numbers[mid];
    }

    public int linerSearch(int[] numbers) {
        int minNum = Integer.MAX_VALUE;
        for(int i=0; i<numbers.length; i++) {
            if(numbers[i] < minNum)
                minNum = numbers[i];
        }
        return minNum;
    }
}