class MinStack {
public:
	int missingNumber(vector<int>& nums) {
		if (nums.empty())
			return -1;
		int start = 0;
		int end = nums.size() - 1;
		int mid;
		while (start <= end) {
			mid = (start + end) / 2;
			if (nums[mid] == mid)
				start = mid + 1;
			else {
				if (mid == 0 || nums[mid - 1] == mid - 1)
					return mid;
				else
					end = mid - 1;
			}
		}
		if (start == nums.size()) // 只有一个数字的情况
			return nums.size();
		return -1;
	}
};