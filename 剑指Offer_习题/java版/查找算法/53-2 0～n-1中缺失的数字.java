public class Solution {
    public int missingNumber(int[] nums) {
        if(nums.length == 0)
            return -1;
        int start = 0;
        int end = nums.length - 1;
        int mid;
        while(start <= end) {
            mid = (start + end) >> 1;
            if(nums[mid] == mid)
                start = mid + 1;
            else {
                if(mid == 0 || nums[mid - 1] == mid - 1)
                    return mid;
                else
                    end = mid - 1;
            }
        }
        if(start == nums.length)
            return nums.length;
        return -1;
    }
}