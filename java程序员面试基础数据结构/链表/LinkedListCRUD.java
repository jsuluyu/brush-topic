package 链表相关;

public class LinkedListCRUD {
    public static void main(String[] args) {
        MyLinkedList list = new MyLinkedList();

        list.addNode(9);
        list.addNode(8);
        list.addNode(5);
        list.addNode(2);
        list.addNode(1);
        list.addNode(1);
        System.out.println("list length is:" + list.getLength());
        System.out.println("before order");
        list.printList();
        System.out.println("after order");
        list.orderList();
        list.printList();
        System.out.println("在指定位置插入数据后");
        list.addNode(6, 2);
        list.printList();
        System.out.println("删除指定位置元素后");
        list.deleteNode(7);
        list.printList();
    }
}
