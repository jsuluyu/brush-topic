package 链表相关;

public class MyLinkedList implements LinkedList_ {
    private Node head;

    public MyLinkedList() {
        head = null;
    }

    @Override
    public void addNode(int val) {
        Node newNode = new Node(val);
        // 若链表是空链表
        if(head == null) {
            head = newNode;
            return;
        }
        Node pNode = head;
        while(pNode.next != null) {
            pNode = pNode.next;
        }
        // 在末尾添加新节点
        pNode.next = newNode;
    }

    public boolean addNode(int val, int index) {
        if(index < 1 || index > this.getLength()) {
            return false;
        }
        int id = 0;
        Node newNode = new Node(val);
        Node virNode = new Node();
        virNode.next = head;
        Node preNode = virNode;
        Node curNode = preNode.next;
        while(curNode != null) {
            if(id == index - 1) {
                preNode.next = newNode;
                newNode.next = curNode;
                head = virNode.next;
                return true;
            }
            preNode = curNode;
            curNode = curNode.next;
            id++;
        }
        return false;
    }

    @Override
    public boolean deleteNode(int index) {
        if(index < 1 || index > this.getLength()) {
            return false;
        }
        int id = 1;
        Node virNode = new Node(); // 虚头结点
        virNode.next = head;
        Node preNode = virNode;
        Node curNode = virNode.next;
        while(curNode != null) {
            if(id == index) {
                preNode.next = curNode.next;
                head = virNode.next;
                return true;
            }
            preNode = curNode;
            curNode = curNode.next;
            id++;
        }
        return true;
    }

    @Override
    public int getLength() {
        int len = 0;
        Node pNode = head;
        while(pNode != null) {
            len++;
            pNode = pNode.next;
        }
        return len;
    }

    @Override
    public Node orderList() {
        Node nextNode = null;
        int temp;
        Node curNode = head;
        while(curNode.next != null) {
            nextNode = curNode.next;
            while(nextNode != null) {
                if(curNode.data > nextNode.data) {
                    temp = curNode.data;
                    curNode.data = nextNode.data;
                    nextNode.data = temp;
                }
                nextNode = nextNode.next;
            }
            curNode = curNode.next;
        }
        return head;
    }

    @Override
    public void printList() {
        Node pNode = head;
        while(pNode != null) {
            System.out.print(pNode.data + " ");
            pNode = pNode.next;
        }
        System.out.println();
    }
}
