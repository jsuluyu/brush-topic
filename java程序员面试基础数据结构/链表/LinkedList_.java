package 链表相关;

public interface LinkedList_ {
    /**
     * 向链表的末尾插入数据
     * @param d
     */
    public void addNode(int d);

    /**
     * 删除第index个结点
     * @param index
     * @return
     */
    public boolean deleteNode(int index);

    /**
     * 返回结点的长度
     * @return
     */
    public int getLength();

    /**
     * 对链表进行排序
     * @return
     */
    public Node orderList();

    /**
     * 打印链表
     */
    public void printList();
}
