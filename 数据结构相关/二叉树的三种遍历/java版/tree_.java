package question_tree;

import java.util.*;

public class tree_ {
    public static void main(String[] args) {
        Node node1 = new Node("a");
        Node node2 = new Node("b");
        Node node3 = new Node("c");
        Node node4 = new Node("d");
        Node node5 = new Node("e");
        Node node6 = new Node("f");
        Node node7 = new Node("g");
        Node node8 = new Node("h");
        node1.left = node2;
        node1.right = node3;

        node2.left = node4;
        node2.right = node5;

        node3.left = node6;
        node3.right = node7;

        node4.left = node8;

        // 前序遍历
        List<String> list1 = new ArrayList<>();
        preorderTraversal1(node1, list1);
        System.out.println("前序遍历(递归)" + list1);
        List<String> list2 = preorderTraversal2(node1);
        System.out.println("前序遍历(非递归)" + list2);

        // 中序遍历
        List<String> list3 = new ArrayList<>();
        inorderTraversal1(node1, list3);
        System.out.println("中序遍历(递归)" + list3);
        List<String> list4 = inorderTraversal2(node1);
        System.out.println("中序遍历(非递归)" + list3);

        // 后序遍历
        List<String> list5 = new ArrayList<>();
        postorderTraversal1(node1, list5);
        System.out.println("后序遍历(递归)" + list5);
        List<String> list6 = postorderTraversal2(node1);
        System.out.println("后序遍历(非递归)" + list6);

        // 层次遍历
        ArrayList<ArrayList<String>> list7 = new ArrayList<>();
        list7 = levelOrder(node1);
        System.out.println("层次遍历：" + list7);
    }

    // 前序遍历 递归
    public static void preorderTraversal1(Node node, List<String> list) {
        if(node == null)
            return;
        list.add(node.val);
        preorderTraversal1(node.left, list);
        preorderTraversal1(node.right, list);
    }

    // 前序遍历 非递归
    public static List<String> preorderTraversal2(Node node) {
        Stack<Node> stack = new Stack<>();
        List<String> list = new ArrayList<>();
        stack.push(node);
        while(!stack.empty()) {
            Node tNode = stack.pop();
            list.add(tNode.val);
            if(tNode.right != null)
                stack.push(tNode.right); // 先将右节点放入栈中！！！
            if(tNode.left != null)
                stack.push(tNode.left);
        }
        return list;
    }

    public static void inorderTraversal1(Node node, List<String> list) {
        if(node == null)
            return;
        inorderTraversal1(node.left, list);
        list.add(node.val);
        inorderTraversal1(node.right, list);
    }

    public static List<String> inorderTraversal2(Node node) {
        Stack<Node> stack = new Stack<>();
        List<String> list = new ArrayList<>();
        Node cur = node;
        while(cur != null || !stack.empty()) {
            if(cur != null) { // 指针来访问节点，访问到最底层
                stack.push(cur); // 将访问的节点放进栈
                cur = cur.left;
            }
            else {
                cur = stack.pop();
                list.add(cur.val);
                cur = cur.right;
            }
        }
        return list;
    }

    public static void postorderTraversal1(Node node, List<String> list) {
        if(node == null)
            return;
        postorderTraversal1(node.left, list);
        postorderTraversal1(node.right, list);
        list.add(node.val);
    }

    public static List<String> postorderTraversal2(Node node) {
        Stack<Node> stack = new Stack<>();
        List<String> list = new ArrayList<>();
        stack.push(node);
        while(!stack.empty()) {
            Node tNode = stack.pop();
            list.add(tNode.val);
            if(tNode.left != null)
                stack.push(tNode.left);
            if(tNode.right != null)
                stack.push(tNode.right);
        }
        Collections.reverse(list);
        return list;
    }

    public static ArrayList<ArrayList<String>> levelOrder(Node node) {
        Queue<Node> queue = new LinkedList<>();
        ArrayList<ArrayList<String>> result = new ArrayList<>();
        queue.offer(node);
        while(!queue.isEmpty()) {
            int size = queue.size();
            ArrayList<String> list = new ArrayList<>();
            for(int i=0; i<size; i++) {
                Node tNode = queue.poll();
                list.add(tNode.val);
                if(tNode.left != null)
                    queue.offer(tNode.left);
                if(tNode.right != null)
                    queue.offer(tNode.right);
            }
            result.add(list);
        }
        return result;
    }
}
