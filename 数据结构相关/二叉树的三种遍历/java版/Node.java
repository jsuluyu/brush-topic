package question_tree;

public class Node {
    public String val;
    public Node left;
    public Node right;

    public Node(String val) {
        this.val = val;
        this.left = null;
        this.right = null;
    }
}
