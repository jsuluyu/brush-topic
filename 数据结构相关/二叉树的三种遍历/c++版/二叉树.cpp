﻿#include<iostream>
#include<vector>
#include<stack>
#include<string>
#include<queue>
using namespace std;

struct Node {
	string val;
	Node* left;
	Node* right;
	Node(string val) {
		this->val = val;
		left = NULL;
		right = NULL;
	}
};

// 前序遍历
void preorderTraversal1(Node* node, vector<string>& result)
{
	if (node == NULL)
		return;
	result.push_back(node->val);
	preorderTraversal1(node->left, result);
	preorderTraversal1(node->right, result);
}

vector<string> preorderTraversal2(Node* node)
{
	stack<Node*> st;
	vector<string> list;
	st.push(node);
	while (!st.empty()) {
		Node* tNode = st.top();
		st.pop();
		list.push_back(tNode->val);
		if (tNode->right != NULL)
			st.push(tNode->right);
		if (tNode->left != NULL)
			st.push(tNode->left);
	}
	return list;
}

// 中序遍历
void inorderTraversal1(Node* node, vector<string>& result)
{
	if (node == NULL)
		return;
	inorderTraversal1(node->left, result);
	result.push_back(node->val);
	inorderTraversal1(node->right, result);
}

vector<string> inorderTraversal2(Node* node)
{
	stack<Node*> st;
	vector<string> list;
	Node* cur = node;
	while (cur != NULL || !st.empty()) {
		if (cur != NULL) {
			st.push(cur);
			cur = cur->left;
		}
		else {
			cur = st.top();
			st.pop();
			list.push_back(cur->val);
			cur = cur->right;
		}
	}
	return list;
}

// 后序遍历
void postorderTraversal1(Node* node, vector<string>& result)
{
	if (node == NULL)
		return;
	postorderTraversal1(node->left, result);
	postorderTraversal1(node->right, result);
	result.push_back(node->val);
}

vector<string> postorderTraversal2(Node* node)
{
	stack<Node*> st;
	vector<string> list;
	st.push(node);
	while (!st.empty()) {
		Node* tNode = st.top();
		st.pop();
		list.push_back(tNode->val);
		if (tNode->left)
			st.push(tNode->left);
		if (tNode->right)
			st.push(tNode->right);
	}
	reverse(list.begin(), list.end());
	return list;
}

// 层次遍历
vector<vector<string>> levelOrder(Node* node)
{
	queue<Node*> que;
	vector<vector<string>> result;
	que.push(node);
	while (!que.empty()) {
		int size = que.size();
		vector<string> vec;
		for (int i = 0; i < size; i++) {
			Node* tNode = que.front();
			que.pop();
			vec.push_back(tNode->val);
			if (tNode->left)
				que.push(tNode->left);
			if (tNode->right)
				que.push(tNode->right);
		}
		result.push_back(vec);
	}
	return result;
}

int main()
{
	Node* node1 = new Node("a");
	Node* node2 = new Node("b");
	Node* node3 = new Node("c");
	Node* node4 = new Node("d");
	Node* node5 = new Node("e");
	Node* node6 = new Node("f");
	Node* node7 = new Node("g");
	Node* node8 = new Node("h");
	
	node1->left = node2;
	node1->right = node3;

	node2->left = node4;
	node2->right = node5;

	node3->left = node6;
	node3->right = node7;

	node4->left = node8;

	vector<string> result1;
	preorderTraversal1(node1, result1);
	cout << "前序遍历(递归)：";
	for (string str : result1)
		cout << str << " ";
	cout << endl;
	vector<string> result2;
	result2 = preorderTraversal2(node1);
	cout << "前序遍历(非递归)：";
	for (string str : result2)
		cout << str << " ";
	cout << endl;

	vector<string> result3;
	inorderTraversal1(node1, result3);
	cout << "中序遍历(递归)：";
	for (string str : result3)
		cout << str << " ";
	cout << endl;
	vector<string> result4;
	result4 = inorderTraversal2(node1);
	cout << "中序遍历(非递归)：";
	for (string str : result4)
		cout << str << " ";
	cout << endl;

	vector<string> result5;
	postorderTraversal1(node1, result5);
	cout << "后序遍历(递归)：";
	for (string str : result5)
		cout << str << " ";
	cout << endl;
	vector<string> result6;
	result6 = postorderTraversal2(node1);
	cout << "后序遍历(非递归)：";
	for (string str : result6)
		cout << str << " ";
	cout << endl;

	vector<vector<string>> result7;
	result7 = levelOrder(node1);
	cout << "层次遍历的结果为：" << endl;
	for (vector<string> vec : result7) {
		for (string str : vec)
			cout << str << " ";
		cout << endl;
	}

	system("pause");
	return 0;
}