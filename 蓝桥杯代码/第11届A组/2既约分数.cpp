#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int gcd(int x, int y)
{
	int r = 1;
	while (r != 0) {
		r = x % y;
		x = y; 
		y = r;
	}
	return x;
}

int main()
{
	int num = 0;
	for (int i = 1; i <= 2020; i++)
		for (int j = 1; j <= 2020; j++) {
			int t = gcd(i, j);
			if (t == 1)
				num++;
		}
	cout << num << endl;

	system("pause");
	return 0;
}
