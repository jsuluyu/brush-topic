#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

// 七段码所有亮灯情况的存储容器
vector<string> candidate;
// 邻接矩阵存储，真香
int graph[7][7];
int vis[101] = { 0 };
int num1 = 0, num2 = 0;

// 基础并查集
struct DisjointSet {
	int* father;

	explicit DisjointSet(int n) {
		father = new int[n];
		for (int i = 0; i < n; i++) {
			father[i] = i;
		}
	}

	// 找爸爸
	int findRoot(int x) {
		return father[x] == x ? x : father[x] = findRoot(father[x]);
	}

	// 合并
	bool merge(int x, int y) {
		int root_x = findRoot(x);
		int root_y = findRoot(y);

		if (root_x != root_y) {
			father[root_x] = root_y;
		}
		return root_x != root_y;
	}
};


void init() {
	// a : 0, b : 1, c : 2
	// d : 3, e : 4, f : 5, g : 6
	memset(graph, 0, sizeof(graph));
	// connect with 'a'
	graph[0][1] = graph[0][5] = 1;
	// connect with 'b'
	graph[1][0] = graph[1][2] = graph[1][6] = 1;
	// connect with 'c'
	graph[2][1] = graph[2][3] = graph[2][6] = 1;
	// connect with 'd'
	graph[3][1] = graph[3][4] = 1;
	// connect with 'e'
	graph[4][3] = graph[4][5] = graph[4][6] = 1;
	// connect with 'f'
	graph[5][0] = graph[5][4] = graph[5][6] = 1;
	// connect with 'g'
	graph[6][1] = graph[6][2] = graph[6][4] = graph[6][5] = 1;
}

/**
 * 爆搜亮灯的所有情况，换句话说：是要找出 "abcdefg" 中
 * 你所需要全排列。非常经典的题了，推荐去 Bilibili 搜索 BV1qE411E7di
 * 并把这位 UP 主的 dfs系列视频看完，去做对应的练习，你也能很快学会！
 * @param result 搜索到的答案
 * @param end 搜索边界条件
 * @param start 当前搜索的开始位置
 */
void dfs(string& result, int end, int start)
{
	if (result.size() == end) {
		num1++;
		candidate.push_back(result);
		return;
	}
	for (int i = start; i < 3; i++) {
		result.push_back('a' + i);
		dfs(result, end, i + 1);
		result.pop_back();
	}
}

void dfs2(string& result, int index, int start, int len)
{
	if (index == len) {
		num2++;
		candidate.push_back(result);
		return;
	}
	for (int i = start; i < 3; i++) {
		if (!vis[i]) {
			vis[i] = 1;
			result.push_back(i + 'a');
			dfs2(result, index + 1, start + 1, len);
			result.pop_back();
			vis[i] = 0;
		}
	}
}

/**
 * 使用并查集去检查每一种情况是否符合题目条件
 * 即判断这个字符串对应的图是否是连通图
 * 一开始，我也不太理解这道题为啥可以用并查集做
 * 想了一会，回想起写 dfs 判断图连通的“痛苦回忆”
 * 啊，原来并查集在这里只是用来辅助我们检查图是否连通的啊
 * 那没事了，暴力就完了
 */
void process() {
	int ans = 0;

	// 遍历每一种候选情况
	for (auto str : candidate) {
		// 初始化并查集
		DisjointSet disj{ 7 };
		int len = str.length();
		// 这个双重暴力就不用解释了吧~
		// 用来检查 str[i] 和 str[j] 对应的点是否是连通的
		// 如果连通，我们就把它们合并
		for (int i = 0; i < len - 1; i++) {
			for (int j = i + 1; j < len; j++) {
				if (graph[str[i] - 'a'][str[j] - 'a']) {
					disj.merge(str[i] - 'a', str[j] - 'a');
				}
			}
		}
		// 如果这个图是连通图，那么对应这张图的所有点
		// 将只会存在一个点是 disj.father[ch - 'a'] == ch - 'a'
		// 即只有一个点是初始值状态，若不是，则表示这张图不连通
		int cnt = 0;
		for (auto ch : str) {
			if (disj.father[ch - 'a'] == ch - 'a') {
				cnt++;
			}
		}
		if (cnt == 1) {
			ans++;
		}
	}

	printf("%d\n", ans);
}

int main()
{
	string result;
	init();
	// 全排列长度 1 - 7 的所有情况~
	// eg :
	// len = 1 -> "a" "b" "c" "d" "e" "f" "g"
	// len = 2 -> "ac" "ad" "ae" "ag" "bd" "be"...
	//for (int i = 1; i <= 7; i++) {
	//	dfs(result, i, 0);
	//}

	//for (int i = 1; i <= 7; i++) {
	//	dfs2(result, 0, 0, i);
	//}
	//dfs(result, 3, 0);
	dfs2(result, 0, 0, 3);

	cout << num1 << " " << num2 << endl;
	for (string str : candidate) {
		cout << str << endl;
	}

	process();

	system("pause");
	return 0;
}
