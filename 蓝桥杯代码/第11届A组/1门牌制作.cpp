#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int num = 0;
	for (int i = 1; i <= 2020; i++) {
		string s = to_string(i);
		for (char c : s) {
			if (c == '2')
				num++;
		}
	}
	printf("%d\n", num);

	system("pause");
	return 0;
}
