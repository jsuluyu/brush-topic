#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

// 可以将图形顺时针旋转45度，我们可以发现20行20列应该位于第39层的中间一个，然后模拟就可以算出来了

int main()
{
	int sum = 0;
	int deep = 1;
	int d2 = 0;
	while (d2 != 20) {
		if (deep % 2 != 0) {
			d2++;
		}
		sum += deep;
		
		if (d2 == 20) {
			cout << deep << endl;
			cout << sum - (deep - d2) << endl;
			break;
		}
		deep++;
	}

	system("pause");
	return 0;
}

// 法二：模拟
int main()
{
	int num = 1;
	for (int i = 1; i <= 38; i++) {
		for (int j = 1; j <= i; j++) {
			num++;
		}
	}
	int ans = num + 39 / 2;
	cout << ans << endl;

	system("pause");
	return 0;
}