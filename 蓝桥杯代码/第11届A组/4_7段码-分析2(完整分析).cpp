#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
	思路：
		1.先找出所有的组合方式
		2.依次判断这些哪些是不连通的，并去除这些不连通的
*/

int num = 0;
vector<string> candidate;
int graph[7][7]; // 邻接矩阵

// 记住！！ 基础的并查集表 可用于判断图的连通性
struct DisjoinSet
{
	int *father; // 每个集合的主节点  相当于树的根
	DisjoinSet(int n) {
		father = new int[n];
		for (int i = 0; i < n; i++) { // 并查集的初始：每个结点自成一个集合
			father[i] = i;
		}
	}

	// 找父亲结点
	int findRoot(int x) {
		if (x != father[x]) {
			father[x] = findRoot(father[x]);
		}
		return father[x];
	}

	//int findRoot(int x) {
	//	return father[x] == x ? x : father[x] = findRoot(father[x]);
	//}

	// 合并集合
	bool merge(int x, int y) {
		int root_x = findRoot(x);
		int root_y = findRoot(y);

		if (root_x != root_y) {
			father[root_x] = root_y;
		}
		return root_x != root_y; // 这步相当于return false
	}
};


void init()
{
	// a : 0, b : 1, c : 2
	// d : 3, e : 4, f : 5, g : 6
	memset(graph, 0, sizeof(graph));
	// connect with 'a'
	graph[0][1] = graph[0][5] = 1;
	// connect with 'b'
	graph[1][0] = graph[1][2] = graph[1][6] = 1;
	// connect with 'c'
	graph[2][1] = graph[2][3] = graph[2][6] = 1;
	// connect with 'd'
	graph[3][1] = graph[3][4] = 1;
	// connect with 'e'
	graph[4][3] = graph[4][5] = graph[4][6] = 1;
	// connect with 'f'
	graph[5][0] = graph[5][4] = graph[5][6] = 1;
	// connect with 'g'
	graph[6][1] = graph[6][2] = graph[6][4] = graph[6][5] = 1;
}

void dfs(string& result, int end, int start) // 这里面的end是表示有多少个坑
{
	if (result.size() == end) {
		num++;
		candidate.push_back(result);
		return;
	}
	for (int i = start; i < 7; i++) { // 这里的i循环表示的是每个坑目前有多少个选择 
		result.push_back(i + 'a');
		dfs(result, end, i + 1); // 因为这里每次开始的位置不一样，所以不会出现重复的，就不需要使用vis数组，其实两者意思一样
		result.pop_back();
	}
}

void process()
{
	int ans = 0; // 最终的答案

	for (string str : candidate) { // 遍历每一种候选情况
		DisjoinSet disj{ 7 }; // 每一次都初始化一个结构体
		int len = str.length();
		for (int i = 0; i < len - 1; i++) {
			for (int j = i + 1; j < len; j++) {
				if (graph[str[i] - 'a'][str[j] - 'a'])
					disj.merge(str[i] - 'a', str[j] - 'a');
			}
		}

		int cnt = 0;
		for (auto ch : str) {
			if (disj.father[ch - 'a'] == ch - 'a') {
				cnt++;
			}	
		}
		if (cnt == 1) {
			ans++;
		}
			
	}
	cout << ans << endl;
}

int main()
{
	string result;
	init();
	for (int i = 1; i <= 7; i++) {
		dfs(result, i, 0);
	}

	process();
	cout << num << endl;

	system("pause");
	return 0;
}
