#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

bool check(int x) // 检查日期是否合法
{
	int a[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	string s = to_string(x);
	string s1 = s.substr(0, 4);
	string s2 = s.substr(4, 4);
	int year = stoi(s1);
	int month = (s2[0] - 48) * 10 + (s2[1] - 48);
	int day = (s2[2] - 48) * 10 + (s2[3] - 48);
	if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
		a[2]++;
	if (day > a[month] || month > 12)
		return false;
	else
		return true;
}

bool check1(int x) // 检查是否是回文数
{
	string str = to_string(x);
	int len = str.size() - 1;
	for (int i = 0; i <= len; i++) {
		if (str[i] != str[len - i])
			return false;
	}
	return true;
}

bool check2(int x) // 检查是否是ABAB BABA型
{
	set<char> hashSet;
	string s = to_string(x);
	string s1 = s.substr(0, 4);
	string s2 = s.substr(4, 4);
	for (char ch : s)
		hashSet.insert(ch);
	if (hashSet.size() != 2)
		return false;
	if (s1[0] != s1[2] || s1[1] != s1[3])
		return false;
	for (int i = 0; i < 4; i++) {
		if (s1[i] != s2[3 - i])
			return false;
	}
	return true;
}

int main()
{
	int n;
	cin >> n;
	int flag[2] = {0};
	for (int i = n + 1; i <= 89991231; i++) {
		//i = 20222202;
		if (check(i)) {
			if (check1(i) && !flag[0]) {
				flag[0] = 1;
				cout << i << endl;
			}
			if (check1(i) && check2(i) && !flag[1]) {
				flag[1] = 1;
				cout << i << endl;
			}
		}
		if (flag[0] && flag[1])
			break;
	}

	system("pause");
	return 0;
}
