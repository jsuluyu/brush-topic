#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;


int main()
{
	int n;
	cin >> n;
	vector<int> arr(n);
	for (int i = 0; i < n; i++) {
		cin >> arr[i];
	}

	int maxScore = -1, minScore = 101;
	double avgScore = 0.0;
	for (int i = 0; i < n; i++) {
		if (arr[i] > maxScore)
			maxScore = arr[i];
		if (arr[i] < minScore)
			minScore = arr[i];
		avgScore += arr[i];
	}
	printf("%d\n%d\n%.2f\n", maxScore, minScore, avgScore / n);

	system("pause");
	return 0;
}
