#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int f(string& str)
{
	int sum = 0;
	unordered_map<char, int> hashMap;
	for (char c : str) {
		hashMap[c]++;
	}
	for (auto it = hashMap.begin(); it != hashMap.end(); it++) {
		if (it->second == 1) {
			sum++;
		}
	}
	return sum;
}

int main()
{
	int sum = 0;
	string s;
	cin >> s;
	vector<string> candidate;
	int len = s.size();
	string tStr;
	for (int i = 0; i < len; i++) {
		for (int j = i; j < len; j++) {
			int tLen = j - i + 1;
			tStr = s.substr(i, tLen);
			candidate.push_back(tStr);
			// cout << tStr << endl;
		}
	}
	for (string str : candidate) {
		sum += f(str);
	}
	cout << sum << endl;

	system("pause");
	return 0;
}
