#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int check(int n, vector<int>& arr)
{
	int flag = 0;
	for (int i = 0; i < n; i++) {
		if (arr[i] % 2) // 存在奇数
			flag = 1;
	}
	return flag == 1 ? 0 : 1;
}

int main()
{
	int n;
	cin >> n;
	vector<int> arr(n);
	for (int i = 0; i < n; i++)
		cin >> arr[i];

	if (check(n, arr)) {
		printf("INF\n");
		return 0;
	}
	vector<int> dp(100002);
	dp[0] = 1;
	for (int i = 0; i < n; i++) { // 对于每笼包子
		for (int j = 1; j <= 100001; j++) { // 对于每种重量
			if (arr[i] > j) continue;
			if (dp[j - arr[i]]) dp[j] = 1;// 若3能凑，那么3的倍数都能凑
		}
	}

	int ans = 0;
	for (int i = 0; i <= 10001; i++) {
		if (!dp[i])
			ans++;
	}
	cout << ans << endl;
	
	system("pause");
	return 0;
}