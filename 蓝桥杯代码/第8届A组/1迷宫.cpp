#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int ans = 0;
string s[101];
int vis[11][11];

void initVis()
{
	for (int i = 0; i < 11; i++)
		for (int j = 0; j < 11; j++)
			vis[i][j] = 0;
}

void dfs(int x, int y) 
{
	if (x < 0 || x > 9 || y < 0 || y > 9) {
		ans++;
		return;
	}
	if (vis[x][y]) // 表示该地走过了，该角色陷入了死循环，即不能走回头路
		return;
	vis[x][y] = 1;
	switch (s[x][y]) {
	case 'U' :
		dfs(x - 1, y);
		break;
	case 'D':
		dfs(x + 1, y);
		break;
	case 'L':
		dfs(x, y - 1);
		break;
	case 'R':
		dfs(x, y + 1);
		break;
	default:
		break;
	}
}

int main()
{
	for (int i = 0; i < 10; i++)
			cin>>s[i];
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			initVis();
			dfs(i, j);
		}
	}
	cout << ans << endl;

	system("pause");
	return 0;
}