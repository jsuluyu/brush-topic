#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
深度优先遍历更适合目标比较明确，以找到目标为主要目的的情况。
广度优先遍历更适合在不断扩大范围时找到最优解的情况。如下图1239的bfs情况，一层一层的向下搜索，
当搜索到已访问过的数据时的步数一定大于（上下层关系）或等于（在同一层）初次访问该数据时的步数，
所以是对于步数来说是最短访问路径。
*/

struct Node
{
	int pos; // 当前位置
	int ans; // 当前的步数
	string status; // 当前的状态
};
queue<Node> Q;
set<string> vis; // 表示已经出现过得状态

void bfs()
{
	Node head, tail;
	head.pos = head.ans = 0;
	head.status = "012345678";
	Q.push(head);
	while (!Q.empty()) {
		head = Q.front(); // 取出队首元素
		Q.pop(); // 出队
		if (head.status == "087654321") { // 判断是否到达指定状态
			cout << head.ans << endl;
			return;
		}
		for (int i = -2; i <= 2; i++) { // 开始入队  即遍历4种跳法，也就是树的4个儿子
			if (i == 0) continue;
			string temp = head.status;
			int tIdx = head.pos;
			swap(temp[tIdx], temp[(tIdx + i + 9) % 9]); // 交换位置
			if (vis.count(temp) == 0) { // 当前局面没有出现过
				vis.insert(temp);
				tail.pos = (tIdx + i + 9) % 9;
				tail.ans = head.ans + 1;
				tail.status = temp;
				Q.push(tail);
			}
		}
	}
}

int main()
{
	bfs();

	system("pause");
	return 0;
}