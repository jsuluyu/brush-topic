#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int ans, n;
struct coordinate
{
	int x1, y1;
	int x2, y2;
};
vector<vector<int>> maze;
vector<coordinate> arr;
int vis[10001][10001] = { 0 };
int dir[4][2] = { {-1, 0}, {0, 1}, {1, 0}, {0, -1} };

void dfs(int x1, int y1, int x2, int y2)
{
	if (x1 == x2 && y1 == y2) {
		return;
	}

	for (int i = 0; i < 4; i++) {
		int tx = x1 + dir[i][0];
		int ty = y1 + dir[i][1];
		if (tx >= x1 && tx < x2 && ty >= y1 && ty < y2 && !vis[tx][ty]) {
			vis[tx][ty] = 1;
			maze[tx][ty] = 1;
			dfs(tx, ty, x2, y2);
		}
	}
}

int main()
{
	cin >> n;
	coordinate coor;
	int maxX = 0, maxY = 0;
	for (int i = 0; i < n; i++) {
		cin >> coor.x1 >> coor.y1 >> coor.x2 >> coor.y2;
		arr.push_back(coor);
		if (coor.x2 > maxX) maxX = coor.x2;
		if (coor.y2 > maxY) maxY = coor.y2;
	}
	maze = vector<vector<int>>(maxX+1, vector<int>(maxY+1, 0));

	for (int i = 0; i < n; i++) {
		memset(vis, 0, sizeof(vis));
		vis[arr[i].x1][arr[i].y1] = 1;
		maze[arr[i].x1][arr[i].y1] = 1;
		dfs(arr[i].x1, arr[i].y1, arr[i].x2, arr[i].y2);
	}
	
	int ans = 0;
	for(int i=0; i<maxX; i++)
		for (int j = 0; j < maxY; j++) {
			if (maze[i][j] == 1)
				ans++;
		}
	cout << ans << endl;

	for (int i = 0; i <= maxX; i++) {
		for (int j = 0; j <= maxY; j++) {
			cout << maze[i][j] << " ";
		}
		cout << endl;
	}
		
	
	system("pause");
	return 0;
}