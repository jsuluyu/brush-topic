#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int ans = 0;
int vis[7][7] = { 0 };
int dir[4][2] = { {-1, 0}, {0, 1}, {1, 0}, {0, -1} }; // 顺时针

void dfs(int x, int y)
{
	if (x == 0 || x == 6 || y == 0 || y == 6) {
		ans++;
		return;
	}
	for (int i = 0; i < 4; i++) {
		int tx = x + dir[i][0];
		int ty = y + dir[i][1];
		if (tx >= 0 && tx < 7 && ty >= 0 && ty < 7 && !vis[tx][ty]) {
			vis[tx][ty] = 1; // 左侧开始走
			vis[6 - tx][6 - ty] = 1; // 右侧开始走
			dfs(tx, ty);
			vis[tx][ty] = 0; // 回溯
			vis[6 - tx][6 - ty] = 0;
		}
	}
}

int main()
{
	vis[3][3] = 1;
	dfs(3, 3); // 因为(3,3)是中心点，因此必须是出现的，且得从这点开始同时从两侧出发
	cout << ans / 4 << endl; // 最后得出的结果要除以4，因为旋转堆成的属于同一种分割法。

	system("pause");
	return 0;
}