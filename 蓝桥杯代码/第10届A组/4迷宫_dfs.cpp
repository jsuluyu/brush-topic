#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int row = 30, col = 50;
int minStep = 999999999;
int minDic = 99999999;
char maze[31][51];
int vis[31][51] = { 0 };
int dir[4][2] = { {0, 1}, {0, -1}, {1, 0}, {-1, 0} };
string ans;

void check(string& route)
{
	int sum = 0;
	int step = route.size();
	if (step <= minStep) {
		minStep = step;
		for (char c : route) {
			if (c == 'D') sum += 1;
			else if (c == 'L') sum += 2;
			else if (c == 'R') sum += 3;
			else sum += 4;
		}
		if (sum < minDic)
			ans = route;
	}
}

void dfs(string& route, int x, int y)
{
	char c;
	if (x == row - 1 && y == col - 1) {
		// cout << route << endl;
		check(route);
		return;
	}
	for (int i = 0; i < 4; i++) {
		int tx = x + dir[i][0];
		int ty = y + dir[i][1];
		if (dir[i][0] == 0 && dir[i][1] == 1) c = 'R';
		if (dir[i][0] == 0 && dir[i][1] == -1) c = 'L';
		if (dir[i][0] == 1 && dir[i][1] == 0) c = 'D';
		if (dir[i][0] == -1 && dir[i][1] == 0) c = 'U';
		if (tx >= 0 && tx < row && ty >= 0 && ty < col && !vis[tx][ty] && maze[tx][ty] != '1') {
			vis[tx][ty] = 1;
			route.push_back(c);
			dfs(route, tx, ty);
			route.pop_back();
			vis[tx][ty] = 0;
		}
	}
}

int main()
{
	string route;
	vis[0][0] = 1;
	for (int i = 0; i < row; i++)
		for (int j = 0; j < col; j++)
			cin >> maze[i][j];
			//scanf_s("%d", &maze[i][j]);

	/*for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			cout << maze[i][j] << " ";
		}
		cout << endl;
	}*/
		

	dfs(route, 0, 0);
	cout << "-----------" << endl;
	cout << ans << endl;

	system("pause");
	return 0;
}
