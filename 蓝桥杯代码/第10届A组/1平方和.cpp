#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

bool check(int x)
{
	while (x) {
		int y = x % 10;
		x /= 10;
		if (y == 0 || y == 1 || y == 2 || y == 9)
			return true;
	}
	return false;
}

int main()
{
	long long sum = 0;
	for (int i = 1; i <= 2019; i++) {
		if (check(i)) {
			sum += pow(i, 2);
		}
	}
	cout << sum << endl;

	system("pause");
	return 0;
}
