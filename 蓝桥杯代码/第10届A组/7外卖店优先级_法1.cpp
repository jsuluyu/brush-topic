#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct order
{
	int ts; // 时间
	int id; // 店铺id
};

bool cmp(order x, order y)
{
	if (x.ts != y.ts) return x.ts < y.ts;
	else return x.id < y.id;
}

int main()
{
	int ans = 0;
	int n, m, t;
	cin >> n >> m >> t;
	vector<order> arr(m);
	for (int i = 0; i < m; i++)
		cin >> arr[i].ts >> arr[i].id;
	sort(arr.begin(), arr.end(), cmp);
	unordered_map<int, int> priority;
	unordered_map<int, int> hashMap;
	set<int> hashSet; // 存储店铺id
	for (int i = 0; i < m; i++)
		hashSet.insert(arr[i].id);
	for (auto it = hashSet.begin(); it != hashSet.end(); it++) {
		hashMap[*it] = 0;
		priority[*it] = 0;
	}
		
	for (int i = 0; i < m; i++) {
		int index = arr[i].id;
		for (auto it = hashMap.begin(); it != hashMap.end(); it++) {
			if(it->first == index)
				hashMap[index] += 2;
			if (it->first != index && it->second > 0)
				it->second--;
		}
		for (auto it = hashMap.begin(); it != hashMap.end(); it++) {
			if (it->second > 5)
				priority[it->first] = 1;
			if (it->second <= 3 && priority[it->first] == 1) {
				priority[it->first] = 0;
			}
		}
	}
	for (auto it = priority.begin(); it != priority.end(); it++) {
		if (it->second == 1)
			ans++;
	}
	cout << ans << endl;

	system("pause");
	return 0;
}
