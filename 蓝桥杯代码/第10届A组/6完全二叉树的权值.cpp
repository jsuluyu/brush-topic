#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;



int main()
{
	int n;
	cin >> n;
	vector<int> arr(n);
	for (int i = 0; i < n; i++)
		cin >> arr[i];
	int start = 0, end = 0;
	int maxSum = -999999;
	int sum = 0;
	int height = 0;
	int maxHeight;
	while (start < n) {
		height++;
		int tEnd = pow(2, height - 1);
		end += tEnd;
		sum = 0;
		for (int i = start; i < end; i++)
			sum += arr[i];
		if (sum > maxSum) {
			maxSum = sum;
			maxHeight = height;
		}	
		start = end;
	}
	cout << maxHeight << endl;


	system("pause");
	return 0;
}
