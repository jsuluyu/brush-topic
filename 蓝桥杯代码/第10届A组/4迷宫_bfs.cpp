#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

// bfs  适合求解图和树的最短路径问题
// 此题：在bfs中，多加一步，按照字典序来走

int row = 30, col = 50;
char maze[61][61];
int vis[61][61] = { 0 };
int dir[4][2] = { {1, 0}, {0, -1}, {0, 1}, {-1, 0} };
char dirc[4] = { 'D','L','R','U' };

struct Node
{
	int x, y,  step;
	string str;
	Node(int x1, int y1, int s1, string str1) {
		x = x1;
		y = y1; 
		step = s1;
		str = str1;
	}
};

void bfs(int x, int y)
{
	queue<Node> Q;
	Q.push(Node(0, 0, 0, "")); // 起点入队
	vis[0][0] = 1;

	while (!Q.empty()) {
		Node now = Q.front();
		if (now.x == row - 1 && now.y == col - 1) {
			cout << now.step << endl;
			cout << now.str << endl;
			break;
		}

		Q.pop();

		for (int i = 0; i < 4; i++) {
			int tx = now.x + dir[i][0];
			int ty = now.y + dir[i][1];
			if (tx >= 0 && tx < row && ty >= 0 && ty < col && maze[tx][ty] != '1' && !vis[tx][ty]) {
				Q.push(Node(tx, ty, now.step + 1, now.str + dirc[i]));
				vis[tx][ty] = 1;
			}
		}
	}
}

int main()
{
	for (int i = 0; i < row; i++)
		for (int j = 0; j < col; j++)
			cin >> maze[i][j];
	bfs(0, 0);

	system("pause");
	return 0;
}
