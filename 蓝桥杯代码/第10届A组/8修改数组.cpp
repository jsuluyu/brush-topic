#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int main()
{
	int n;
	cin >> n;
	vector<int> arr(n);
	unordered_map<int, int> hashMap;
	for (int i = 0; i < n; i++) {
		cin >> arr[i];
		if (!hashMap[arr[i]])
			hashMap[arr[i]] = 1;
		else {
			while (hashMap[arr[i]] == 1) {
				arr[i]++;
			}
			hashMap[arr[i]] = 1;
		}
	}
	for (int i = 0; i < n; i++) {
		if (hashMap[arr[i]])
			cout << arr[i] << " ";
	}

	system("pause");
	return 0;
}
