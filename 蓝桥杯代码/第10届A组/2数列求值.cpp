#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

// 递归深度超了
int fabi(int x)
{
	if (x == 1 || x == 2 || x == 3)
		return 1;
	return fabi(x - 1) + fabi(x - 2) + fabi(x - 3);
}

int main()
{
	long long t1 = 1, t2 = 1, t3 = 1, next;
	// ans = fabi(6); //20190324
	// cout << ans << endl;

	for (int i = 4; i <= 20190324; i++) {
		// next = ((t1 + t2) % 10000 + t3) % 10000; // 记得会超，以后这种类型的累加题一定要记得取余
		next = (t1 + t2 + t3) % 10000;
		t1 = t2;
		t2 = t3;
		t3 = next;
	}
	cout << next << endl;

	system("pause");
	return 0;
}
