#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

// 计算一下一共有多少天，然后除以7算出一共有多少周即可。

int main()
{
	int sum = 0;
	for (int i = 1901; i <= 2000; i++) {
		if ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0)
			sum += 366;
		else
			sum += 365;
	}
	cout << sum / 7 << endl;

	system("pause");
	return 0;
}