#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

// 大数阶乘

int main()
{
	int n;
	int digit = 1; // 表示位数，刚开始位数为1
	int carry; // 表示进位
	int i, j, temp;
	int a[100000] = { 1 }; // 存储结果的各位
	cin >> n;

	for (i = 2; i <= n; i++) {
		for (j = 1, carry = 0; j <= digit; j++) {
			temp = a[j - 1] * i + carry;
			a[j - 1] = temp % 10;
			carry = temp / 10;
		}
		while (carry) {
			a[++digit - 1] = carry % 10;
			carry = carry / 10;
		}
	}
	for (int i = digit - 1; i >= 0; i--) {
		cout << a[i];
	}

	system("pause");
	return 0;
}