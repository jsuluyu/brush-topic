#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;



int main()
{
	int sum = 0;
	for (int i = 1; i <= 19; i++) {
		sum += pow(2, i);
	}
	cout << sum + 1 << " / " << pow(2, 19) << endl;

	system("pause");
	return 0;
}