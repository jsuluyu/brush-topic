#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int getTime()
{
	int h1, m1, s1, h2, m2, s2, d = 0;
	string str;
	getline(cin, str);
	if (str.size() == 17) {
		h1 = stoi(str.substr(0, 2)); m1 = stoi(str.substr(3, 2)); s1 = stoi(str.substr(6, 2));
		h2 = stoi(str.substr(9, 2)); m2 = stoi(str.substr(12, 2)); s2 = stoi(str.substr(15, 2));
	}
	else {
		h1 = stoi(str.substr(0, 2)); m1 = stoi(str.substr(3, 2)); s1 = stoi(str.substr(6, 2));
		h2 = stoi(str.substr(9, 2)); m2 = stoi(str.substr(12, 2)); s2 = stoi(str.substr(15, 2));
		d = stoi(str.substr(20, 1));
	}
	int time = h2 * 3600 + m2 * 60 + s2 + d * 24 * 3600 - h1 * 3600 - m1 * 60 - s1;
	return time;
}

int main()
{
	int n;
	cin >> n;
	getchar();
	for (int i = 0; i < n; i++) {
		int time1 = getTime();
		int time2 = getTime();
		int t = (time1 + time2) / 2;
		printf("%02d:%02d:%02d\n", t / 3600, t / 60 % 60, t % 60);
	}

	system("pause");
	return 0;
}