#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
思路：1，59084709587505可以使用long long类型来表示。

           2，所有的幸运数字都是由3/5/7构成因此所有的数字都可以表示成3^a * 5^b * 7^c 的形式。

           3，求出^a * 5^b * 7^c 比59084709587505小时abc的组合方式再加1即是真确答案。
原文链接：https://blog.csdn.net/qq_36561697/article/details/80752392
*/

int main()
{
	int ans = 0;
	long long maxNum = 59084709587505;
	for(int i=0; pow(3, i)<maxNum; i++) 
		for(int j=0; pow(5, j) < maxNum; j++)
			for (int k = 0; pow(7, k) < maxNum; k++) {
				if (pow(3, i) * pow(5, j) * pow(7, k) < maxNum) {
					ans++;
				}
			}
	cout << ans << endl;

	system("pause");
	return 0;
}