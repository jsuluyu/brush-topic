#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

// 大数阶乘

int ans[100000000] = { 1 };

int main()
{
	int a[101];
	for (int i = 0; i < 100; i++) {
		cin >> a[i];
	}

	int sum = 0;
	int digit = 4;
	int carry = 0;
	int i, j, temp;
	ans[0] = 0, ans[1] = 5, ans[2] = 6, ans[3] = 5;
	for (i = 1; i < 100; i++) {
		for (j = 1, carry = 0; j <= digit; j++) {
			temp = ans[j - 1] * a[i] + carry;
			ans[j - 1] = temp % 10;
			carry = temp / 10;
		}
		while (carry) {
			ans[++digit - 1] = carry % 10;
			carry = carry / 10;
		}
	}
	for (i = digit - 1; i >= 0; i--) {
		cout << ans[i];
	}
	for (i = 0; i < digit; i++) {
		if (ans[i] == 0)
			sum++;
		else
			break;
	}
	cout << endl << sum << endl;

	system("pause");
	return 0;
}