#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

int n, k;
vector<vector<int>> candidate;

int check()
{
	int sum, maxNum = -9999;
	for (int i = 0; i < candidate.size(); i++) {
		sum = 0;
		for (int j = 0; j < candidate[i].size(); j++) {
			sum += candidate[i][j];
		}		
		if (sum % k == 0 && sum > maxNum) {
			maxNum = sum;
		}
	}
	return sum;
}

void dfs(vector<int>& result, vector<int>& arr, int start, int end)
{
	if (result.size() == end) {
		candidate.push_back(result);
		return;
	}
	for (int i = start; i < n; i++) {
		result.push_back(arr[i]);
		dfs(result, arr, i + 1, end);
		result.pop_back();
	}
}

int main()
{
	cin >> n >> k;
	vector<int> result;
	vector<int> arr(n);
	for (int i = 0; i < n; i++)
		cin >> arr[i];
	dfs(result, arr, 0, 3);
	int ans = check();
		
	for (int i = 0; i < candidate.size(); i++) {
		for (int j = 0; j < candidate[i].size(); j++)
			cout << candidate[i][j];
		cout << endl;
	}
	cout << ans << endl;

	system("pause");
	return 0;
}