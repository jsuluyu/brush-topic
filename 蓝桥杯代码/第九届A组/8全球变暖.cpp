#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

/*
思路：
	1.先找到一个陆地，然后使用dfs遍历和这个陆地相连的所有陆地，构成的岛屿
	2.检查每一个陆地的四周是否有海，若有将其变为海，若没有则不变
	3.通过dfs已经将所有可能变成海的都已标记为‘.’，最后检查地图中有多少'#',即为答案

测试用例：
	.......
	.##....
	.##....
	....##.
	..####.
	...###.
	.......

	岛屿=2
	ans = 1

	...###..
	###.#...
	.###...#
	.###...#
	.#.#..##
	....#...
	...###..
	..####..

	岛屿=4
	ans = 2
*/

int n, ans = 0;
char maze[101][101];
int vis[101][101] = { 0 };
int dir[4][2] = { {0, 1}, {0, -1}, {1, 0}, {-1, 0} };

int check(int x, int y)
{
	for (int i = 0; i < 4; i++) {
		int tx = x + dir[i][0];
		int ty = y + dir[i][1];
		if ((maze[tx][ty] == '.' && vis[tx][ty] == 0) || (tx == 0 || tx == n + 1 || ty == 0 || ty == n + 1))
			return 1;
	}
	return 0;
}

void dfs(int x, int y)
{
	for(int i=0; i<4; i++) {
		int tx = x + dir[i][0];
		int ty = y + dir[i][1];
		if (tx >= 1 && tx <= n && ty >= 1 && ty <= n && !vis[tx][ty] && maze[tx][ty] == '#') {
			if (check(tx, ty))
				maze[tx][ty] = '.';
			vis[tx][ty] = 1;
			dfs(tx, ty);
		}
	}
}

int main()
{
	cin >> n;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			cin >> maze[i][j];
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++) {
			if (maze[i][j] == '#') {
				vis[i][j] = 1;
				if (check(i, j))
					maze[i][j] = '.';
				dfs(i, j);
			}
		}
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++) {
			if (maze[i][j] == '#')
				ans++;
		}
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++)
			cout << maze[i][j] << " ";
		cout << endl;
	}
		
	cout << ans << endl;

	system("pause");
	return 0;
}