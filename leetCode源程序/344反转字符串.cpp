// 法1：双指针
class Solution {
public:
    void reverseString(vector<char>& s) {
        int slowIdx = 0, fastIdx = s.size() - 1;
        char c;
        while(slowIdx <= fastIdx) {
            c = s[slowIdx];
            s[slowIdx] = s[fastIdx];
            s[fastIdx] = c;
            slowIdx++;
            fastIdx--;
        }
    }
};

// 库函数
class Solution {
public:
    void reverseString(vector<char>& s) {
        reverse(s.begin(), s.end());
    }
};