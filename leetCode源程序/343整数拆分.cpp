class Solution {
public:

    /*
    法1：记忆化搜索
    vector<int> memo;

    int max3(int x, int y, int z) 
    {
        return max(x, max(y, z));
    }

    int dfs(int n) 
    {
        if(n == 1)
            return 1;
        if(memo[n] != -1)
            return memo[n];
        
        int res = -1;
        for(int i = 1; i <= n-1; i++) { // 依次拆分当前的数（对于儿子节点的各种情况）
            res = max3(i * (n - i), res, i * dfs(n - i));
        }
        memo[n] = res;
        return res;
    }

    int integerBreak(int n) {
        memo = vector<int>(n+1, -1);
        int ans = dfs(n);
        return ans;
    }
    */
    // 法2：动态规划
    int max3(int x, int y, int z)
    {
        return max(x, max(y, z));
    }

    int integerBreak(int n) {
        vector<int> dp(n+1, -1); // dp[i]表示数字i分割(至少分割成2部分)后得到的最大乘积
        dp[1] = 1;
        for(int i=2; i<=n; i++)
            // 求解dp[i]
            for(int j=1; j<=i-1; j++) // 求将i分为几部分的最大值
                // 当前最大值，仅分割为2部分的最大值，一部分为j，另一部分为i-j的最大分割结果
                dp[i] = max3(dp[i], j * (i - j), j * dp[i-j]); 
        return dp[n];
    }
    
};



自底向上DP
class Solution {
public:
    vector<int> memo;

    int max3(int x, int y, int z) 
    {
        return max(x, max(y, z));
    }


    int integerBreak(int n) {
        memo = vector<int>(n+1, -1);
        memo[1] = 1;
        for(int i=2; i<=n; i++) {
            for(int j=1; j<=i-1; j++) {
                memo[i] = max3(memo[i], j * (i - j), j * memo[i - j]);
            }
        }
        return memo[n];
    }
};