#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

class Solution {
public:
	void moveZeroes(vector<int>& nums) {
		vector<int> noneZeroArr;
		for (int num : nums) {
			if (num != 0) noneZeroArr.push_back(num);
		}
		for (int i = 0; i < noneZeroArr.size(); i++) {
			nums[i] = noneZeroArr[i];
		}
		for (int i = noneZeroArr.size(); i < nums.size(); i++) {
			nums[i] = 0;
		}
	}
};

int main()
{
	vector<int> arr{ 0,1,0,3,12 };
	//arr = { 0,1,0,3,12 };
	//arr[0] = 0, arr[1] = 1, arr[2] = 0, arr[3] = 3, arr[4] = 12;
	Solution s1;
	s1.moveZeroes(arr);
	Solution *s2 = new Solution();
	for (int num : arr) {
		cout << num << " ";
	}

	system("pause");
	return 0;
}
