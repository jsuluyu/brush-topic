class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        while(head != NULL && head->val == val) {
            ListNode *delNode = head;
            head = delNode->next;
            delete delNode;
        }

        if(head == NULL) 
            return NULL;

        ListNode *cur = head;
        while(cur->next != NULL) {
            if(cur->next->val == val) {
                ListNode *delNode = cur->next;
                cur->next = delNode->next;
                delete delNode;
            } else {
                cur = cur->next;
            }
        }
        return head;
    }
};