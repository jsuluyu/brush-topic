class Solution {
public:
    bool canPartition(vector<int>& nums) {
        int sum=0;
        for(int num : nums)
            sum += num;
        if(sum % 2 != 0)
            return false;
        // step1:定义dp数组
        vector<vector<int>> memo(nums.size()+1, vector<int>(sum+1, -1)); // 将n个物品填满容量为C的背包
        // step2:确定递推公式 dp[i][j] = dp[i-1][j] || dp[i-1][j-w[i]]
        // step3:初始化dp数组
        for(int i=0; i<sum; i++) // 1个物品无法分成2个集合
            memo[0][i] = 0;
        for(int i=0; i<nums.size(); i++) // 不选取任何数，和为0
            memo[i][0] = 1;
        memo[0][nums[0]] = 1; // 选择和为0，只有nums[0]
        // step4:确定递推顺序
        for(int i=1; i<nums.size(); i++)
            for(int j=1; j<=sum/2; j++) {
                memo[i][j] = memo[i-1][j];
                if(j >= nums[i])
                    memo[i][j] = (memo[i-1][j] || memo[i-1][j-nums[i]]) ? 1 : 0;
            }
        return memo[nums.size()-1][sum/2] == 1;
    }
};


class Solution {
public:
/*
    F(n,C)考虑将n个物品填满容量为C的背包
    F(i,c) = F(i-1,c) || F(i-1,c-w[i]) 只考虑前i-1个物品时，剩余的重量可以拿第i个物品
*/
    // 法1：记忆化搜索
    vector<vector<int>> memo;
    // memo[i][c]表示使用索引为[0,i]的这些元素，是否可以完全填充一个容量为c的背包
    // -1:未计算 0：不可填充 1：可填充

    // 使用nums[0,index]，是否可以完全填充一个容量为sum的背包
    bool dfs(vector<int>& nums, int index, int sum)
    {
        if(sum == 0)
            return true;
        if(index < 0 || sum < 0)
            return false;
        if(memo[index][sum] != -1)
            return memo[index][sum] == 1;
        
        memo[index][sum] = (dfs(nums, index - 1, sum) ||
                             dfs(nums, index - 1, sum - nums[index])) ? 1 : 0;
        return memo[index][sum] == 1;
    }

    bool canPartition(vector<int>& nums) {
        int sum = 0;
        for(int num : nums)
            sum += num;
        memo = vector<vector<int>>(nums.size()+1, vector<int>(sum+1, -1));

        if(sum % 2 != 0 )
            return false;
        return dfs(nums, nums.size() - 1, sum/2);
    }

    /*bool canPartition(vector<int>& nums) {
        int sum = 0;
        for(int num : nums)
            sum += num;
        sum /= 2;
        memo = vector<vector<int>>(nums.size()+1, vector<int>(sum+1, -1));

        if(sum % 2 != 0 )
            return false;

        for(int i=0; i<=sum; i++)
            memo[0][i] = 0;
        for(int i=0; i<nums.size(); i++)
            memo[i][0] = 1;
        for(int i=1;)
    }*/
};