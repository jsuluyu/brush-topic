class Solution {
public:
    vector<vector<int>> candidata;

    // end:需要凑的个数，start:从哪部分开始选数 n:候选的总数
    void dfs(vector<int>& result, int end, int start, int n) 
    {
        if(result.size() == end) {
            candidata.push_back(result);
            return;
        }

        for(int i=start; i<=n; i++) {
            result.push_back(i);
            dfs(result, end, i+1, n);
            result.pop_back();
        }
    }

    vector<vector<int>> combine(int n, int k) {
        vector<int> result;
        dfs(result, k, 1, n);
        return candidata;
    }
};