class Solution {
public:
    // unordered_map<出发机场, map<到达机场, 航班次数>> targets
    unordered_map<string, map<string, int>> targets;

    bool dfs(int ticketNum, vector<string>& result)
    {
        if(result.size() == ticketNum + 1) {
            return true;
        }

        for(pair<const string, int>& target : targets[result[result.size()-1]]) {
            if(target.second > 0) { // 若小于0 说明该机场已经飞过
                result.push_back(target.first);
                target.second--;
                if(dfs(ticketNum, result)) return true;
                result.pop_back();
                target.second++;
            }
        }
        return false;
    }

    vector<string> findItinerary(vector<vector<string>>& tickets) {
        targets.clear();
        vector<string> result;
        for(vector<string>& vec : tickets) { // 记录映射关系
            targets[vec[0]][vec[1]]++; // 出发机场-->到达机场 路线++
        }
        result.push_back("JFK");
        dfs(tickets.size(), result);
        return result;
    }
};