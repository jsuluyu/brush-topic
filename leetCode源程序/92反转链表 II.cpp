/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int m, int n) {
        if(head == NULL) return NULL;
        ListNode *pre = NULL;
        ListNode *cur = head;
        ListNode *pNext;
        while(m > 1) {
            pre = cur;
            cur = cur->next;
            m--;
            n--;
        }
        ListNode *con = pre, *tail = cur;
        while(n > 0) {
            pNext = cur->next;
            cur->next = pre;
            pre = cur;
            cur = pNext;
            n--;
        }
        if(con != NULL) {
            con->next = pre;
        } else {
            head = pre;
        }
        tail->next = cur;
        return head;
    }
};

/*
注意我们要引入两个额外指针，分别称为 tail 和 con。tail 指针指向从链表头起的第mm个结点，此结点是反转后链表的尾部，故称为 tail。con 指针指向第 mm 个结点的前一个结点，此结点是新链表的头部。下图可以帮助你更好的理解这两个指针。
*/
