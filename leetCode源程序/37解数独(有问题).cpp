#include <stdio.h>

int a[9][9];

int judge(int a1,int b,int c)
{ int i,j,tx,ty;
  for(i=0;i<9;i++)
   if(a[a1][i]==c)
	return 0;
  for(i=0;i<9;i++)
   if(a[i][b]==c)
	return 0;
  tx=a1/3*3;
  ty=b/3*3;
  for(i=tx;i<tx+3;i++)
   for(j=ty;j<ty+3;j++)
	if(a[i][j]==c)
	 return 0;
 return 1;
}

void dfs(int x,int y)
{ int i,j,k,m;
  if(x==9&&y==0)
  {
	  printf("数独的答案是：\n");
	  for(j=0;j<9;j++)
	  {
		  for(k=0;k<9;k++)
		   printf("%d ",a[j][k]);
		  printf("\n");
	  }
	  return;
  }
  if(a[x][y]!=0)
   dfs(x,y+1);
  if(y==9)
   dfs(x+1,0);
  if(a[x][y]==0)
  {
	  for(i=1;i<=9;i++)
	  {
		  if(judge(x,y,i))
		  {
			  a[x][y]=i;
		      dfs(x,y+1);
		      a[x][y]=0;
		  }
	  }
  }
}

int main()
{ int i,j;
  for(i=0;i<9;i++)
   for(j=0;j<9;j++)
	scanf("%d",&a[i][j]);
  dfs(0,0);
  return 0;
}


/*
53..7....
6..195...
.98....6.
8...6...3
4..8.3..1
7...2...6
.6....28.
...419..5
....8..79

530070000
600195000
098000060
800060003
400809001
700020006
060000280
000419005
000419005
000080079

*/