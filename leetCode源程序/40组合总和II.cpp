class Solution {
public:
    vector<vector<int>> ans;
    vector<int> vis;

    void dfs(vector<int>& candidates, vector<int>& result, int target, int start, int sum)
    {
        if(sum > target)
            return;
        if(sum == target) {
            ans.push_back(result);
            return;
        }

        for(int i=start; i<candidates.size(); i++) {
            if(i > 0 && vis[i-1] == 0 && candidates[i] == candidates[i-1])
                continue;

            result.push_back(candidates[i]);
            sum += candidates[i];
            vis[i] = 1;
            dfs(candidates, result, target, i+1, sum);
            vis[i] = 0;
            sum -= candidates[i];
            result.pop_back();
        }
    }

    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {
        sort(candidates.begin(), candidates.end());
        vis = vector<int>(candidates.size(), 0);
        vector<int> result;
        dfs(candidates, result, target, 0, 0);
        return ans;
    }
};