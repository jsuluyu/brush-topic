class Solution {
public:
    vector<vector<int>> ans;

    void dfs(vector<int>& candidates, vector<int>& result, int target, int sum, int start)
    {
        if(sum > target)
            return;
        if(sum == target) {
            ans.push_back(result);
            return;
        }

        for(int i=start; i<candidates.size(); i++) {
            result.push_back(candidates[i]);
            sum += candidates[i];
            dfs(candidates, result, target, sum, i);
            sum -= candidates[i];
            result.pop_back();
        }
    }

    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        vector<int> result;
        dfs(candidates, result, target, 0, 0);
        return ans;
    }
};