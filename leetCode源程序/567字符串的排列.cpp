#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

class Solution {
public:
	bool checkInclusion(string s1, string s2) {
		unordered_map<char, int> need, window;
		for (char c : s1) need[c]++;
		int left = 0, right = 0, valid = 0;
		char ch;
		while (right < s2.size()) {
			ch = s2[right];
			right++;
			if (need.count(ch)) {
				window[ch]++;
				if (window[ch] == need[ch])
					valid++;
			}
			while (right - left == s1.size()) {
				if (valid == need.size())
					return true;
				ch = s2[left];
				left++;
				if (need.count(ch)) {
					if (window[ch] == need[ch])
						valid--;
					window[ch]--;
				}
			}
		}
		return false;
	}
};

int main()
{
	string str1 = "ab";
	string str2 = "aidbaooo";
	Solution s1;
	bool ans = s1.checkInclusion(str1, str2);
	/*Solution *s2 = new Solution();
	for (int num : arr) {
		cout << num << " ";
	}*/
	cout << ans << endl;

	system("pause");
	return 0;
}
