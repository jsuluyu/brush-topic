class Solution {
public:
/*
    F(n,C)考虑将n个物品填满容量为C的背包
    F(i,c) = F(i-1,c) || F(i-1,c-w[i]) 只考虑前i-1个物品时，剩余的重量可以拿第i个物品
*/
    // 法1：记忆化搜索
    vector<vector<int>> memo;
    // memo[i][c]表示使用索引为[0,i]的这些元素，是否可以完全填充一个容量为c的背包
    // -1:未计算 0：不可填充 1：可填充

    // 使用nums[0,index]，是否可以完全填充一个容量为sum的背包
    bool dfs(vector<int>& nums, int index, int sum)
    {
        if(sum == 0)
            return true;
        if(index < 0 || sum < 0)
            return false;
        if(memo[index][sum] != -1)
            return memo[index][sum] == 1;
        
        memo[index][sum] = (dfs(nums, index - 1, sum) ||
                             dfs(nums, index - 1, sum - nums[index])) ? 1 : 0;
        return memo[index][sum] == 1;
    }

    bool canPartition(vector<int>& nums) {
        int sum = 0;
        for(int num : nums)
            sum += num;
        memo = vector<vector<int>>(nums.size()+1, vector<int>(sum+1, -1));

        if(sum % 2 != 0 )
            return false;
        // return dfs(nums, nums.size() - 1, sum/2);
        return tryPartition(nums, nums.size()-1, sum / 2);
    }

    // 法二：动态规划
    bool tryPartition(vector<int>& nums, int index, int sum)
    {
        for(int i=0; i<=sum; i++)
            memo[0][i] = 0;
        for(int i=0; i<nums.size(); i++)
            memo[i][0] = 1;
        
        for(int i=1; i<=index; i++) { // 对于每个物品
            for(int j=1; j<=sum; j++) { // 对于每个重量
                memo[i][j] = memo[i-1][j];
                if(j >= nums[i])
                    memo[i][j] = memo[i][j] || memo[i-1][j-nums[i]];
            }
        }
        return memo[index][sum] == 1;
    }

    /*bool canPartition(vector<int>& nums) {
        int sum = 0;
        for(int num : nums)
            sum += num;
        sum /= 2;
        memo = vector<vector<int>>(nums.size()+1, vector<int>(sum+1, -1));

        if(sum % 2 != 0 )
            return false;

        for(int i=0; i<=sum; i++)
            memo[0][i] = 0;
        for(int i=0; i<nums.size(); i++)
            memo[i][0] = 1;
        for(int i=1;)
    }*/
};