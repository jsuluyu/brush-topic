class Solution {
public:
    void sortColors(vector<int>& nums) {
        // 三路排序 思路
        int zero = -1; // nums[0,zero]为0
        int two = nums.size(); // nums[two, n-1]为2
        for(int i=0; i<two;) { // 注意i循环到最后一个2的索引就截止，因为后面都是2了
            if(nums[i] == 1) {
                i++;
            }
            else if(nums[i] == 2) {
                two--;
                swap(nums[two], nums[i]);
            }
            else {
                zero++;
                swap(nums[i], nums[zero]);
                i++;
            }
        }
    }
};