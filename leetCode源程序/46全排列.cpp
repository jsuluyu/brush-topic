class Solution {
public:
    vector<vector<int>> candidate;
    vector<int> vis;

    void dfs(vector<int>& result, vector<int>& nums)
    {
        if(result.size() == nums.size()) {
            candidate.push_back(result);
            return;
        }
        
        for(int i=0; i<nums.size(); i++) {
            if(vis[i]) continue;
            result.push_back(nums[i]);
            vis[i] = 1;
            dfs(result, nums);
            vis[i] = 0;
            result.pop_back();
        }
    }

    vector<vector<int>> permute(vector<int>& nums) {
        vis = vector<int>(nums.size()+1, 0);
        vector<int> result;
        dfs(result, nums);
        return candidate;
    }
};