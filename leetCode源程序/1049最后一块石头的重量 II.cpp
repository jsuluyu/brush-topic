class Solution {
public:
    int lastStoneWeightII(vector<int>& stones) {
        int sum = 0;
        for(int stone : stones)
            sum += stone;
        int capacity = sum / 2; // capacity是向上取整的，因此最终memo[i][j]的最大值小于sum的一半
        // dp[i,j] 当背包的容量为j时，前i个物品最佳组合对应的价值。
        vector<vector<int>> memo(stones.size()+1, vector<int>(capacity+1, 0));
        for(int i=0; i<stones.size(); i++)
            memo[i][0] = 0;
        for(int i=0; i<=capacity; i++)
            memo[0][i] = (i >= stones[0]) ? stones[0] : 0;
        for(int i=1; i<stones.size(); i++)
            for(int j=1; j<=capacity; j++) {
                memo[i][j] = memo[i-1][j]; // 不拿
                if(j >= stones[i]) 
                    memo[i][j] = max(memo[i-1][j], memo[i-1][j-stones[i]] + stones[i]);
            }
        return sum - 2 * memo[stones.size()-1][capacity];
    }
};