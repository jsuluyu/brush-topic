class Solution {
public:
    int binarySearch(int id, int num, vector<int>& numbers)
    {
        int left = id, right = numbers.size() - 1;
        int mid;
        while(left <= right) {
            mid = left + (right - left) / 2;
            if(numbers[mid] > num)
                right = mid - 1;
            else if(numbers[mid] < num)
                left = mid + 1;
            else {
                return mid;
            }
        }
        return -1;
    }

    vector<int> twoSum(vector<int>& numbers, int target) {
        int end;
        vector<int> ans;
        for(int i=0; i<numbers.size(); i++) {
            end = binarySearch(i + 1, target - numbers[i], numbers);
            if(end != -1) {
                ans.push_back(i + 1);
                ans.push_back(end + 1);
                break;
            }
        } 
        return ans;
    }
};

/*当遇到的是有序数组时，第一时间想到--二分查找*/