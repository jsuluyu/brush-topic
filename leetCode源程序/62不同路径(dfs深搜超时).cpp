// 法1：动态规划
class Solution {
public:
    int uniquePaths(int m, int n) {
        // step1.确定dp数组以及下标的含义 memo[i][j]:到达(i,j)时的方法
        vector<vector<int>> memo(m, vector<int>(n, 0));
        // step2.确定递推公式 memo[i][j] = memo[i-1][j] + memo[i][j-1]
        // step3.dp数组的初始化
        for(int i=0; i<m; i++)
            memo[i][0] = 1;
        for(int i=0; i<n; i++)
            memo[0][i] = 1;
        // step4.确定遍历顺序
        for(int i=1; i<m; i++) {
            for(int j=1; j<n; j++)
                memo[i][j] = memo[i-1][j] + memo[i][j-1];
        }
        // step5.举例推导dp数组

        return memo[m-1][n-1];
    }
};

// 法二：深度优先搜索 超时
class Solution {
public:
    int ans = 0;
    int dir[2][2] = {{0, 1}, {1, 0}};
    int vis[101][101] = {0};

    void dfs(int m, int n, int x, int y)
    {
        if(x == m - 1 && y == n - 1) {
            ans++;
            return;
        }

        for(int i=0; i<2; i++) {
            int tx = x + dir[i][0];
            int ty = y + dir[i][1];
            if(tx >= 0 && tx < m && ty >= 0 && ty < n && !vis[tx][ty]) {
                vis[tx][ty] = 1;
                dfs(m, n, tx, ty);
                vis[tx][ty] = 0;
            }
        }
    }

    int uniquePaths(int m, int n) {
        vis[0][0] = 1;
        dfs(m, n, 0, 0);
        return ans;
    }
};