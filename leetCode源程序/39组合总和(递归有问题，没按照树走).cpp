#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

class Solution {
public:
	set<vector<int>> hashSet;
	vector<vector<int>> ans;

	int getSum(vector<int>& result)
	{
		int sum = 0;
		for (int i = 0; i < result.size(); i++) {
			sum += result[i];
		}
		return sum;
	}

	void dfs(vector<int>& candidates, vector<int>& result, int target)
	{
		for(int num : result)
			cout << num << " ";
		cout << endl;
		int sum = getSum(result);
		if (sum > target)
			return;
		if (sum == target) {
			sort(result.begin(), result.end());
			hashSet.insert(result);
			return;
		}

		for (int i = 0; i < candidates.size(); i++) {
			result.push_back(candidates[i]);
			dfs(candidates, result, target);
			result.pop_back();
		}
	}

	vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
		vector<int> result;
		dfs(candidates, result, target);
		for (auto it = hashSet.begin(); it != hashSet.end(); it++) {
			ans.push_back(*it);
		}
		return ans;
	}
};

int main()
{
	//string str1 = "barfoothefoobarman";
	//vector<string> words;
	//words.push_back("foo");
	//words.push_back("bar");
	//vector<int> nums = {1, 2, 3};
	Solution s1;
	//vector<vector<int>> ans = s1.permute(nums);
	//Solution *s2 = new Solution();
	//vector<vector<int>> nums = { {2}, {3, 4}, {6, 5, 7}, {4, 1, 8, 3} };
	//vector<vector<int>> nums = { {-1}, {2, 3}, {1, -1, -3} };
	vector<int> nums = { 1, 2 };
	int t = 4;
	vector<vector<int>> ans = s1.combinationSum(nums, t);
	for (vector<int> num : ans) {
		for(int dig : num)
			cout << dig << " ";
		cout << endl;
	}
	//cout << ans << endl;

	// cout << (-1 || 0) << endl;


	system("pause");
	return 0;
}
