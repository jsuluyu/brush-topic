class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        unordered_map<char, int> window;
        int left = 0, right = 0;
        int res = 0;
        char ch;
        while(right < s.length()) {
            ch = s[right];
            right++;
            window[ch]++;
            while(window[ch] > 1) { // while里面的ch和下面的d不是同一个字符！小心
                char d = s[left];
                left++;
                window[d]--;
            }
            res = max(res, right - left);
        }
        return res;
    }
};


// 错误代码：注意比较缩小区间中的区别
class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        unordered_map<char, int> window;
        int left = 0, right = 0;
        int res = 0;
        char ch;
        while(right < s.length()) {
            ch = s[right];
            right++;
            window[ch]++;
            while(window[ch] > 1) { // while里面的ch和下面的d不是同一个字符！小心
                ch = s[left];
                left++;
                window[ch]--;
            }
            res = max(res, right - left);
        }
        return res;
    }
};