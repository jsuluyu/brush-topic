class Solution {
public:
    vector<vector<int>> candidate;
    vector<int> vis; // 判断某一根树枝是否重复
    vector<int> used; // 判断某一层是否重复

    void dfs(vector<int>& result, vector<int>& nums)
    {
        if(result.size() == nums.size()) {
            candidate.push_back(result);
            return;
        }

        for(int i=0; i<nums.size(); i++) {
            if(vis[i]) // 判断某一根树枝是否重复
                continue;
            if(i > 0 && nums[i-1] == nums[i] && used[i-1] == 0) //判断某一层是否重复，使用这句必须先排序
                continue;
            result.push_back(nums[i]);
            vis[i] = 1;
            used[i] = 1;
            dfs(result, nums);
            used[i] = 0;
            vis[i] = 0;
            result.pop_back();
        }
    }

    vector<vector<int>> permuteUnique(vector<int>& nums) {
        vis = vector<int>(nums.size()+1, 0);
        used = vector<int>(nums.size()+1, 0);
        vector<int> result;
        sort(nums.begin(), nums.end());
        dfs(result, nums);
        return candidate;
    }
};

// 当然可以直接使用一个数组进行判重
class Solution {
private:
    vector<vector<int>> result;
    vector<int> path;
    void backtracking (vector<int>& nums, vector<bool>& used) {
        // 此时说明找到了一组
        if (path.size() == nums.size()) {
            result.push_back(path);
            return;
        }
        for (int i = 0; i < nums.size(); i++) {
            // used[i - 1] == true，说明同一树支nums[i - 1]使用过
            // used[i - 1] == false，说明同一树层nums[i - 1]使用过
            // 如果同一树层nums[i - 1]使用过则直接跳过
            if (i > 0 && nums[i] == nums[i - 1] && used[i - 1] == false) {
                continue;
            }
            if (used[i] == false) {
                used[i] = true;
                path.push_back(nums[i]);
                backtracking(nums, used);
                path.pop_back();
                used[i] = false;
            }
        }
    }
public:
    vector<vector<int>> permuteUnique(vector<int>& nums) {
        result.clear();
        path.clear();
        sort(nums.begin(), nums.end()); // 排序
        vector<bool> used(nums.size(), false);
        backtracking(nums, used);
        return result;
    }
};
