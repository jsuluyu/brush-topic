class Solution {
public:
    bool isAnagram(string s, string t) {
        unordered_map<char, int> hashMap;
        for(int i=0; i<26; i++) {
            char t = 'a' + i;
            hashMap[t] = 0;
        }
        for(char c : s) {
            hashMap[c]++;
        }
        for(char c : t) {
            hashMap[c]--;
        }
        for(int i=0; i<26; i++) {
            if(hashMap['a' + i] != 0)
                return false;
        }
        return true;
    }
};

// 数组映射
class Solution {
public:
    bool isAnagram(string s, string t) {
        int hashTable[26] = {0};
        for(char c : s) {
            int id = c - 'a';
            hashTable[id]++;
        }
        for(char c : t) {
            int id = c - 'a';
            hashTable[id]--;
        }
        for(int i=0; i<26; i++) {
            if(hashTable[i] != 0)
                return false;
        }
        return true;
    }
};