class Solution {
public:
	// 模拟
	vector<vector<int>> generateMatrix(int n) {
		vector<vector<int>> matrix(n, vector<int>(n, 0));
		int powN = pow(n, 2), num = 1;
		int row = 0, col = 0;
		int flag = 0;
		while (num <= powN) {
			while (col < n - flag && num <= powN) {
				matrix[row][col] = num++;
				col++;
			}
			row++;
			col--;
			while (row < n - flag && num <= powN) {
				matrix[row][col] = num++;
				row++;
			}
			row--;
			col--;
			while (col >= flag && num <= powN) {
				matrix[row][col] = num++;
				col--;
			}
			row--;
			col++;
			while (row > flag && num <= powN) {
				matrix[row][col] = num++;
				row--;
			}
			row++;
			col++;
			flag++;
		}
		return matrix;
	}
};