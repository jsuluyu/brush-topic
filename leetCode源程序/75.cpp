class Solution {
public:
    void sortColors(vector<int>& nums) {
        int count[3] = {0};
        for(int i=0; i<nums.size(); i++) {
            count[nums[i]]++;
        }
        int id=0;
        for(int i=0; i<count[0]; i++) {
            nums[id++] = 0;
        }
        for(int i=0; i<count[1]; i++) {
            nums[id++] = 1;
        }
        for(int i=0; i<count[2]; i++) {
            nums[id++] = 2;
        }
    }
};