class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) { // 虚拟头结点的使用
        ListNode *dummyHead = new ListNode(0);
        dummyHead->next = head;

        ListNode *cur = dummyHead;
        while(cur->next != NULL) {
            if(cur->next->val == val) {
                ListNode *delNode = cur->next;
                cur->next = delNode->next;
                delete delNode;
            } else {
                cur = cur->next;
            }
        }
        return dummyHead->next;
    }
};