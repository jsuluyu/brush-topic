// 法一：通过多次调用dfs，来获得每一种组合情况
class Solution {
public:
    vector<vector<int>> candidate;
    vector<int> vis;

    void dfs(vector<int>& result, vector<int>& nums, int end, int start)
    {
        if(result.size() == end) {
            candidate.push_back(result);
            return;
        }

        for(int i=start; i<nums.size(); i++) {
            if(i > 0 && vis[i-1] == 0 && nums[i] == nums[i-1]) {
                continue;
            }
            result.push_back(nums[i]);
            vis[i] = 1;
            dfs(result, nums, end, i+1);
            vis[i] = 0;
            result.pop_back();
        }
    }

    vector<vector<int>> subsetsWithDup(vector<int>& nums) {
        int n = nums.size();
        vis = vector<int>(n+1, 0);
        sort(nums.begin(), nums.end());
        for(int i=0; i<=n; i++) {
            vector<int> result;
            dfs(result, nums, i, 0);
        }
        return candidate;
    }
};


// 法二：仅调用1次dfs，但是dfs的每一次执行中间结果全部保留
class Solution {
public:
    vector<vector<int>> candidate;
    vector<int> vis;

    void dfs(vector<int>& result, vector<int>& nums, int end, int start)
    {
        candidate.push_back(result); // 每一种组合情况都加入到结果集中
        if(result.size() > end) {
            return;
        }

        for(int i=start; i<end; i++) {
            if(i > 0 && vis[i-1] == 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            result.push_back(nums[i]);
            vis[i] = 1;
            dfs(result, nums, end, i+1);
            vis[i] = 0;
            result.pop_back();
        }
    }

    vector<vector<int>> subsetsWithDup(vector<int>& nums) {
        vector<int> result;
        vis = vector<int>(nums.size() + 1, 0);
        sort(nums.begin(), nums.end());
        dfs(result, nums, nums.size(), 0);
        return candidate;
    }
};