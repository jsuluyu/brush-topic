// 双指针c语言
char* replaceSpace(char* s){
    if(s == NULL)
        return NULL;
    int orginalLen = 0;
    int newLen = 0;
    int blackNum = 0;
    int p1, p2;
    int i = 0;

    while(s[i] != '\0')
    {
        if(s[i] == ' ')
            blackNum++;
        orginalLen++;
        i++;
    }

    newLen = orginalLen + 2 * blackNum + 1;
    p1 = orginalLen + 1;
    p2 = newLen;
    s = (char*)realloc(s, sizeof(char)*(newLen+1));

    while(p1 >= 0)
    {
        if(s[p1] == ' ')
        {
            s[p2--] = '0';
            s[p2--] = '2';
            s[p2--] = '%';
        }
        else
        {
            s[p2--] = s[p1];
        }
        p1--;
    }
    return s;
}

// 双指针c++
class Solution {
public:
    string replaceSpace(string s) {
        int oldLen = s.size();
        int blackCount = 0;
        for(int i=0; i<oldLen; i++) {
            if(s[i] == ' ')
                blackCount++;
        }
        int newLen = oldLen + blackCount * 2;
        s.resize(newLen);
        for(int i=oldLen-1, j = newLen-1; i<j; i--, j--) {
            if(s[i] == ' ') {
                s[j] = '0';
                s[j-1] = '2';
                s[j-2] = '%';
                j -= 2;
            } else {
                s[j] = s[i];
            }   
        }
        return s;
    }
};

// 新开辟空间
class Solution {
public:
    string replaceSpace(string s) {
        string str = "";

        for(int i=0; i<s.size(); i++) {
            if(s[i] != ' ')
                str += s[i];
            else
                str += "%20";
        }

        return str;
    }
};