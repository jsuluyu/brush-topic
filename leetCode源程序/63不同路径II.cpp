class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        int n = obstacleGrid.size();
        int m = obstacleGrid[0].size();
        vector<vector<int>> memo(n, vector<int>(m, 0));
        for(int i=0; i<n; i++) {
            if(obstacleGrid[i][0] == 1) // 有障碍的位置之后都过不去，因此赋值为0
                break;
            memo[i][0] = 1;
        }
        for(int i=0; i<m; i++) {
            if(obstacleGrid[0][i] == 1)
                break;
            memo[0][i] = 1;
        }
        for(int i=1; i<n; i++) {
            for(int j=1; j<m; j++) {
                if(obstacleGrid[i][j] == 1)
                    memo[i][j] = 0;
                else
                    memo[i][j] = memo[i-1][j] + memo[i][j-1];
            }
        }
        return memo[n-1][m-1];
    }
};