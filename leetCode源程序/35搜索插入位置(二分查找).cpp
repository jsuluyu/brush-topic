class Solution {
public:
    // 二分查找一定要注意边界的设置！！！
    int binarySearch(vector<int>& nums, int target) {
        int end = nums.size() - 1; // 范围为[start, end]
        int start = 0;
        int mid;
        while(start < end) {
            mid = (start + end) / 2;
            if(target > nums[mid]) {
                start = mid + 1; 
            } else if(target < nums[mid]) {
                end = mid; // 注意这点！
            } else {
                return mid;
            }
        }
        return end;
    }

    int searchInsert(vector<int>& nums, int target) {
        int len = nums.size();
        if(target > nums[len - 1])
            return len;
        if(target < nums[0])
            return 0;
        int ans = binarySearch(nums, target);
        return ans;
    }
};