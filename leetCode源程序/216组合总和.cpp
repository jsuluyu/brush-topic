class Solution {
public:
    vector<vector<int>> candidate;

    int check(vector<int>& result, int sum)
    {
        int t = 0;
        for(int i=0; i<result.size(); i++) {
            t += result[i];
        }
        if(t == sum) return 1;
        else return 0;
    }

    void dfs(vector<int>& result, int end, int start, int sum)
    {
        if(result.size() == end) {
            if(check(result, sum))
                candidate.push_back(result);
            return;
        }

        for(int i=start; i<=9; i++) {
            result.push_back(i);
            dfs(result, end, i+1, sum);
            result.pop_back();
        }
    }

    vector<vector<int>> combinationSum3(int k, int n) {
        vector<int> result;
        dfs(result, k, 1, n);
        return candidate;
    }
};