/*
    经典例题：注意与“40组合总和II”中的判重做对比
    这里的判重不能先排序，导致后面可能存在重复的元素，因此可以使用set集合进行判重
*/
class Solution {
public:
    vector<vector<int>> candidate;
    vector<int> vis;

    void dfs(vector<int>& result, vector<int> nums, int end, int start)
    {
        unordered_set<int> uSet;
        if(result.size() >= 2) {
            candidate.push_back(result);
        }
        if(result.size() > end) {
            return;
        }

        for(int i=start; i<end; i++) {
            if(uSet.find(nums[i]) != uSet.end()) // 判断去重
                continue;
            if(!result.empty() && result.back() > nums[i]) // 判读递增
                continue;
            uSet.insert(nums[i]);
            result.push_back(nums[i]);
            dfs(result, nums, end, i+1);
            result.pop_back();
        }
    }

    vector<vector<int>> findSubsequences(vector<int>& nums) {
        vector<int> result;
        vis = vector<int>(nums.size()+1, 0);
        dfs(result, nums, nums.size(), 0);
        return candidate;
    }
};