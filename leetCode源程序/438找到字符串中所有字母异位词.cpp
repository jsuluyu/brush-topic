class Solution {
public:
    vector<int> findAnagrams(string s, string p) {
        unordered_map<char, int> need, window;
        for(char c : p) need[c]++;
        int left = 0, right = 0;
        int valid = 0;
        char ch;
        vector<int> ans;
        while(right < s.length()) {
            ch = s[right];
            right++;
            if(need.count(ch)) {
                window[ch]++;
                if(window[ch] == need[ch]) 
                    valid++;
            }

            while(right - left == p.length()) {
                if(valid == need.size())
                    ans.push_back(left);
                ch = s[left];
                left++;
                if(need.count(ch)) {
                    if(window[ch] == need[ch])
                        valid--;
                    window[ch]--;
                }
            }
        }
        return ans;
    }
};