class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        int left = 0, right = -1;
        int ans = 0;
        vector<int> freq(256, 0);
        while(left < s.size()) {
            if(right + 1 < s.size() && freq[s[right + 1]] == 0) {
                right++;
                freq[s[right]]++;
            }
            else {
                freq[s[left]]--;
                left++;
            }
            ans = max(ans, right - left + 1);
        }
        return ans;
    }
};