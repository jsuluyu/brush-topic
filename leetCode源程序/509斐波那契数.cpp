// 法1：记忆化搜索 自顶向下
class Solution {
public:
    vector<int> memo;

    int dfs(int n) 
    {
        if(n == 0 )
            return 0;
        if( n == 1 || n == 2)
            return 1;
        if(memo[n] != -1)
            return memo[n];

        memo[n] = dfs(n-1) + dfs(n-2);
        return memo[n];
    }

    int fib(int n) {
        memo = vector<int>(n+1, -1);
        return dfs(n);
    }
};

// 法2：动态规划 自底向上
class Solution {
public:
    int fib(int n) {
        if(n <= 1) return n;
        vector<int> memo(n+1);
        memo[0] = 0;
        memo[1] = 1;
        for(int i=2; i<=n; i++) {
            memo[i] = memo[i-1] + memo[i-2];
        }
        return memo[n];
    }
};