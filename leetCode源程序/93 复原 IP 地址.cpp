#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

// 本题类似于 K个数的和 其核心思想是将字符串分为4部分，每一次递归完成一部分

class Solution {
private:
	static const int NUM = 4;
	vector<string> ans;
	vector<int> segments;
		
public:
	void dfs(string& s, int segId, int segStart)
	{
		if (segId == NUM) {
			if (segStart == s.size()) {
				string ip = "";
				for (int i = 0; i < 4; i++) {
					ip += to_string(segments[i]);
					if (i != NUM - 1) ip += '.';
				}
				ans.push_back(ip);
			}
			return;
		}

		if (segStart == s.size()) {
			return;
		}

		if (s[segStart] == '0') {
			segments[segId] = 0;
			dfs(s, segId + 1, segStart + 1);
		}

		int addr = 0;
		for (int i = segStart; i < s.size(); i++) {
			addr = addr * 10 + (s[i] - '0');
			if (addr > 0 && addr <= 255) {
				segments[segId] = addr;
				dfs(s, segId + 1, i + 1);
			}
			else
				break;
		}
	}

	vector<string> restoreIpAddresses(string s) {
		segments.resize(NUM);
		dfs(s, 0, 0);
		return ans;
	}
		
		
};

int main()
{
	string digits = "101023";
	Solution s1;
	vector<string> ans = s1.restoreIpAddresses(digits);
	//Solution *s2 = new Solution();
	for (string str : ans) {
		cout << str << endl;
	}

	system("pause");
	return 0;
}


// 法2
class Solution {
public:
    vector<string> candidate;

    int check(string& s, int start, int end) {
        if(start > end)
            return 0;
        if(start != end && s[start] == '0')
            return 0;
        int num = 0;
        for(int i=start; i<=end; i++) {
            if(s[i] < '0' || s[i] > '9')
                return 0;
            num = num * 10 + (s[i] - '0');
            if(num > 255)
                return 0;
        }
        return 1;
    }

    void dfs(string& s, int start, int posNum) {
        if(posNum == 3) {
            if(check(s, start, s.size() - 1))
                candidate.push_back(s);
            return;
        }
        for(int i=start; i<s.size(); i++) {
            if(check(s, start, i)) {
                s.insert(s.begin()+i+1, '.');
            } else 
                continue;
            posNum++;
            dfs(s, i+2, posNum);
            posNum--;
            s.erase(s.begin()+i+1);
        }
    }

    void dfs(string& s, int start, int posNum) {
    	if(posNum == 3) {
    		if(check(s, start, s.size()-1))
    			candidate.push_back(s);
    		return;
    	}
    	for(int i=start; i<s.size(); i++) {
    		if(check(s, start, i)) {
    			s.insert(s.begin()+i+1);
    			posNum++;
    			dfs(s, i+1, posNum);
    			posNum--;
    			s.erase(s.begin() + i + 1);
    		} else {
    			break;
    		}
    	}
    }

    vector<string> restoreIpAddresses(string s) {
        dfs(s, 0, 0);
        return candidate;
    }
};