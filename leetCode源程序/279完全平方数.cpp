// 版本一
class Solution {
public:
    int numSquares(int n) {
        vector<int> dp(n + 1, INT_MAX);
        dp[0] = 0;
        for (int i = 0; i <= n; i++) { // 遍历背包
            for (int j = 1; j * j <= i; j++) { // 遍历物品
                dp[i] = min(dp[i - j * j] + 1, dp[i]);
            }
        }
        return dp[n];
    }
};

// 版本二
class Solution {
public:
    int numSquares(int n) {
        vector<int> dp(n + 1, INT_MAX);
        dp[0] = 0;
        for (int i = 1; i * i <= n; i++) { // 遍历物品
            for (int j = 1; j <= n; j++) { // 遍历背包
                if (j - i * i  >= 0 && dp[j - i * i] != INT_MAX) {
                    dp[j] = min(dp[j - i * i ] + 1, dp[j]);
                }
            }
        }
        return dp[n];
    }
};

作者：carlsun-2
链接：https://leetcode-cn.com/problems/perfect-squares/solution/279-wan-quan-ping-fang-shu-wan-quan-bei-htyas/
来源：力扣（LeetCode）
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
