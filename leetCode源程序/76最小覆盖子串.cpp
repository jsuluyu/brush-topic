#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

class Solution {
public:
	string minWindow(string s, string t) {
		unordered_map<char, int> need; // t中字符对应的个数
		unordered_map<char, int> window; // 滑动窗口对应的字符的个数
		for (char c : t) need[c]++;
		int left = 0, right = 0;
		int start = 0; // 结果子串的开始位置
		int len = 1000000; // 结果子串的长度
		int valid = 0; // 当前满足条件的字符的个数
		while (right < s.length()) {
			char temp = s[right];
			right++; // 右移窗口 
			if (need.count(temp)) {
				window[temp]++;
				if (window[temp] == need[temp]) // 若该字符满足条件了
					valid++;
			}

			// 判断左侧窗口是否要收缩
			while (valid == need.size()) {
				if (right - left < len) {
					start = left;
					len = right - left;
				}
				temp = s[left];
				left++;
				if (need.count(temp)) {
					if (window[temp] == need[temp])
						valid--;
					window[temp]--;
				}
			}
		}
		return len == 1000000 ? "" : s.substr(start, len);
	}
};

int main()
{
	string str1 = "EBBANCF";
	string str2 = "ABC";
	Solution s1;
	string ans = s1.minWindow(str1, str2);
	/*Solution *s2 = new Solution();
	for (int num : arr) {
		cout << num << " ";
	}*/
	cout << ans << endl;

	system("pause");
	return 0;
}
