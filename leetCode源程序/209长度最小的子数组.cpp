class Solution {
public:
    int minSubArrayLen(int s, vector<int>& nums) {
        int left = 0, right = -1;
        int sum = 0;
        int ans = nums.size() + 1;
        while(left < nums.size()) {
            if(right + 1 < nums.size() && sum < s) {
                right++;
                sum += nums[right];
            }
            else {
                sum -= nums[left];
                left++;
            }
            // 窗口滑动结束后，判断是否更新长度
            if(sum >= s)
                ans = min(ans, right - left + 1);
        }
        if(ans == nums.size() + 1) return 0;
        else return ans;
    }
};

// 滑动窗口方式2
class Solution {
public:
    int minSubArrayLen(int target, vector<int>& nums) {
        int result = INT32_MAX;
        int start = 0, end;
        int window;
        int sum = 0;
        for(end = 0; end < nums.size(); end++) {
            sum += nums[end];
            while(sum >= target) { // 当窗口满足条件时，开始缩小窗口
                window = end - start + 1;
                result = window < result ? window : result;
                sum -= nums[start++]; // 尝试缩小窗口
            }
        }
        return result == INT32_MAX ? 0 : result;
    }
};