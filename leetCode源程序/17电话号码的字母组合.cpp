#include <iostream>
#include <cstdio>
#include <cmath>
#include <cctype>
#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
using namespace std;

class Solution {
public:
	vector<string> res;
	string letterMap[10] = { " ", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };

	void findComBination(string& digits, int index, const string& s)  // 注意这里的index标识的开始位置是digits中的
	{
		if (index == digits.size()) {
			res.push_back(s);
			return;
		}
		char ch = digits[index];
		string letters = letterMap[ch - '0'];
		for (int i = 0; i < letters.size(); i++) {
			findComBination(digits, index + 1, s + letters[i]);
		}
		return;
	}

	vector<string> letterCombinations(string digits) {
		res.clear();
		if (digits == "")
			return res;
		findComBination(digits, 0, "");
		return res;
	}
};

int main()
{
	//string str1 = "barfoothefoobarman";
	//vector<string> words;
	//words.push_back("foo");
	//words.push_back("bar");
	string digits = "23";
	Solution s1;
	vector<string> ans = s1.letterCombinations(digits);
	//Solution *s2 = new Solution();
	for (string str : ans) {
		cout << str << " ";
	}


	system("pause");
	return 0;
}
