class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        set<int> resultSet;
        for(int id1 : nums1) {
            for(int id2 : nums2) {
                if(id1 == id2)
                    resultSet.insert(id1);
            }
        }
        return vector<int>(resultSet.begin(), resultSet.end());
    }
};

// 方法2
class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        vector<int> ans;
        unordered_set<int> set1;
        unordered_set<int> set2;
        for(int i=0; i<nums1.size(); i++) 
            set1.insert(nums1[i]);
        for(int i=0; i<nums2.size(); i++) 
            set2.insert(nums2[i]);
        for(auto it=set1.begin(); it != set2.end(); it++) {
            if(set2.find(*it) != set2.end())
                ans.push_back(*it);
        }
        return ans;
    }
};