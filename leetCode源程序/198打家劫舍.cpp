class Solution {
public:
    /*
    // 法1：记忆化搜索
    vector<int> memo; // 考虑抢劫nums[idx, nums.size())范围房子的最大收益

    int dfs(vector<int>& nums, int idx) // idx:从第几家开始偷,即考虑抢劫nums[idx, nums.size())范围房子
    {
        if(idx >= nums.size())
            return 0;
        if(memo[idx] != -1)
            return memo[idx];

        int res = -1;
        for(int i=idx; i < nums.size(); i++) {
            res = max(res, nums[i] + dfs(nums, i+2));
        }
        memo[idx] = res;
        return res;
    }

    int rob(vector<int>& nums) {
        memo = vector<int>(nums.size()+1, -1);
        int ans = dfs(nums, 0);
        return ans;
    }
    */

    // 法2：动态规划
    int rob(vector<int>& nums) {
        int n = nums.size();
        vector<int> memo(n+1, -1);
        memo[n-1] = nums[n-1];
        for(int i=n-2; i>=0; i--) // 考虑抢劫nums[i, n-1]范围的房子
            for(int j=i; j<n; j++) { // 从i-n-1，即i：开始索引 j:遍历到最后的序号
                if(j + 2 < n)
                    memo[i] = max(memo[i], nums[j] + memo[j + 2]); 
                else
                    memo[i] = max(memo[i], nums[j] + 0);
            // memo[i]；j不拿 nums[j] + memo[j + 2]:j拿，再加上从nums[j+2,n-1]的最大收益
            }
        return memo[0];
    }
};