class Solution {
public:
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
        unordered_map<int, int> hashMap1;
        unordered_map<int, int> hashMap2;
        vector<int> ansVrr;
        for(int id : nums1) hashMap1[id]++;
        for(int id : nums2) hashMap2[id]++;
        for(auto it1 = hashMap1.begin(); it1 != hashMap1.end(); it1++) {
            //int num = hashMap2.count(it1->first);
            if(hashMap2.count(it1->first)) {
                int num = it1->second < hashMap2[it1->first] ? it1->second : hashMap2[it1->first];
                for(int i=0; i<num; i++) ansVrr.push_back(it1->first);
            }
        }
        return ansVrr;
    }
};