class Solution {
public:
    vector<vector<string>> candidate;

    int check(string& result)
    {
        int len = result.size();
        for(int i=0; i<len/2; i++) {
            if(result[i] != result[len - i - 1])
                return 0;
        }
        return 1;
    }

    void dfs(vector<string>& result, string& s, int start)
    {
        if(start >= s.size()) {
            candidate.push_back(result);
            return;
        }

        for(int i=start; i<s.size(); i++) {
            string str = s.substr(start, i - start + 1);
            if(check(str)) {
                result.push_back(str);
            } else {
                continue;
            }
            dfs(result, s, i+1);
            result.pop_back();
        }
    }

    vector<vector<string>> partition(string s) {
        vector<string> result;
        dfs(result, s, 0);
        return candidate;
    }
};