class Solution {
public:
    int getSum(int n) {
        int sum = 0;
        int t;
        while(n != 0) {
            t = n % 10;
            n /= 10;
            sum += pow(t, 2);
        }
        return sum;
    }

    bool isHappy(int n) {
        int num;
        unordered_set<int> hashSet;
        while(1) {
            num = getSum(n);
            if(num == 1)
                return true;
            if(hashSet.find(num) != hashSet.end())
                return false;
            else
                hashSet.insert(num);
            n = num;
        }
    }
};