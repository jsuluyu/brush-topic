class Solution {
public:
    int fourSumCount(vector<int>& nums1, vector<int>& nums2, vector<int>& nums3, vector<int>& nums4) {
        int ans = 0;
        unordered_map<int, int> hashMap;
        for(int a : nums1)
            for(int b : nums2)
                hashMap[a + b]++;
        for(int c : nums3)
            for(int d : nums4) {
                if(hashMap.find(0-(c+d)) != hashMap.end())
                    ans += hashMap[0-(c+d)];
            }
        return ans;
    }
};