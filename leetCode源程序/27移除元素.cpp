// 法1：暴力法
class Solution {
public:
    void deleteVal(vector<int>& nums, int val, int idx, int len) {
        for(int i=idx+1; i<len; i++) {
            nums[i-1] = nums[i];
        }
    }

    int removeElement(vector<int>& nums, int val) {
        int len = nums.size();
        for(int i=0; i<len; i++) {
            if(nums[i] == val) {
                deleteVal(nums, val, i, len);
                len--; // 此时数组的大小-1
                i--; // 因为下表i以后的数值都向前移动了一位，所以i也向前移动一位
            }    
        }
        return len;
    }
};


// 法2：双指针法
class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
        int slowIndex = 0;
        for(int fastIndex = 0; fastIndex < nums.size(); fastIndex++) {
            if(nums[fastIndex] != val)
                nums[slowIndex++] = nums[fastIndex];
        }
        return slowIndex;
    }
};