class Solution {
public:
    vector<vector<int>> candidate;

    void dfs(vector<int>& result, vector<int>& nums, int end, int start)
    {
        if(result.size() == end) {
            candidate.push_back(result);
            return;
        }

        for(int i=start; i<nums.size(); i++) {
            result.push_back(nums[i]);
            dfs(result, nums, end, i+1);
            result.pop_back();
        }
    }

    vector<vector<int>> subsets(vector<int>& nums) {
        for(int i=0; i<=nums.size(); i++) {
            vector<int> result;
            dfs(result, nums, i, 0);
        }
        return candidate;
    }
};