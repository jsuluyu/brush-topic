package question_单例模式;

/**
 * 饿汉式——特点是应用中尚未需要用到此单一实例的时候即先实例化
 * 常见经典的写法如下：
 */

public class HungerSingleton {
    // 静态实例变量--直接初始化
    private static HungerSingleton instance = new HungerSingleton();

    // 私有化构造函数
    private HungerSingleton() {
        System.out.println(Thread.currentThread().getName() + "\t进入构造方法");
    }

    // 静态public方法，向整个应用提供单例获取方式
    public static HungerSingleton getInstance() {
        return instance;
    }
}
