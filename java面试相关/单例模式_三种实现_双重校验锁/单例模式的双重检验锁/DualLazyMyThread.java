package question_单例模式_双重校验锁;

/**
 * 查看双重检验锁方式实现单例模式在多线程环境下线程是否安全
 */

public class DualLazyMyThread extends Thread{
    @Override
    public void run() {
        System.out.println(DualLazySingleTon.getInstance().hashCode());
    }

    public static void main(String[] args) {
        DualLazyMyThread[] mts = new DualLazyMyThread[10];
        for (int i=0; i<10; i++)
            mts[i] = new DualLazyMyThread();

        for(int i=0; i<10; i++) {
            mts[i].start();
        }
    }
}
