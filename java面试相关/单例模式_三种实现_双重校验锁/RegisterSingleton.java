package question_单例模式;

import java.util.HashMap;
import java.util.Map;

/**
 * 登记式单例模式——特点是通过一个专门的类对各单例模式的此单一实例进行管理和维护
 * 通过Map方式可以方便的实现此中目的
 * 常见代码写法如下：
 **/

public class RegisterSingleton {
    private static Map singleTonMap = new HashMap();

    public static void main(String[] args) {
        A a = (A) getInstance(A.class.getName()); // 获取A类的单例
        B b = (B) getInstance(B.class.getName()); // 获取B类的单例
    }

    // 根据类型获取单例
    public static Object getInstance(String className) {
        // 判断singleTonMap中是否有此单例，有则取得后返回，无则添加单例后返回
        if(!singleTonMap.containsKey(className)) {
            try {
                singleTonMap.put(className, Class.forName(className).newInstance());
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return singleTonMap.get(className);
    }
}

class A {

}

class B {

}