package question_单例模式;

/**
 * 懒汉式—— 其特点是延迟加载，即当需要用到此单一实例的时候，才去初始化此单一实例
 * 常见经典的写法如下：
 */

public class LazySingleton {
    // 静态实例变量
    private static LazySingleton instance;

    // 私有化构造函数
    private LazySingleton() {
        System.out.println(Thread.currentThread().getName() + "\t进入构造方法");
    }

    // 静态public方法，向整个应用提供单例获取方式
    public static LazySingleton getInstance() {
        if(instance == null) {
            instance = new LazySingleton();
        }
        return instance;
    }
}
