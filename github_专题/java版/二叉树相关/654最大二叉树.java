public class Solution {
    public TreeNode constructMaximumBinaryTree(int[] nums) {
        TreeNode node = new TreeNode(0);
        if(nums.length == 1) {
            node.val = nums[0];
            return node;
        }

        int maxVal = 0;
        int maxIdx = 0;
        for(int i=0; i<nums.length; i++) {
            if(nums[i] > maxVal) {
                maxVal = nums[i];
                maxIdx = i;
            }
        }
        node.val = nums[maxIdx];

        if(maxIdx > 0) {
            int[] newArr = Arrays.copyOfRange(nums, 0, maxIdx);
            node.left = constructMaximumBinaryTree(newArr);
        }
        if(maxIdx < nums.length - 1) {
            int[] newArr = Arrays.copyOfRange(nums, maxIdx + 1, nums.length);
            node.right = constructMaximumBinaryTree(newArr);
        }

        return node;
    }
}