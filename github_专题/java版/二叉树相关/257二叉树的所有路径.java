/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    private List<String> candidate = new ArrayList<>();

    public List<String> binaryTreePaths(TreeNode root) {
        String path = "";
        findPath(root, path);
        return candidate;
    }

    public void findPath(TreeNode cur, String path ) {
        path += Integer.toString(cur.val);
        if(cur.left == null && cur.right == null) {
            candidate.add(path.toString());
            return;
        }

        if(cur.left != null)
            findPath(cur.left, path + "->");
        if(cur.right != null)
            findPath(cur.right, path + "->");
    } 
}