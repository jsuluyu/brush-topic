/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */

// 法1：层次遍历
class Solution {
    public int maxDepth(TreeNode root) {
        if(root == null)
            return 0;
        int depth = 0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while(!queue.isEmpty()) {
            int size = queue.size();
            for(int i=0; i<size; i++) {
                TreeNode node = queue.poll();
                if(node.left != null) 
                    queue.offer(node.left);
                if(node.right != null)
                    queue.offer(node.right);
            }
            depth++;
        }
        return depth;
    }
}


// 法2：后序遍历 递归法
class Solution {
    public int maxDepth(TreeNode root) {
        return getDepth(root);
    }
    
    public int getDepth(TreeNode root) {
        if(root == null)
            return 0;
        int leftDepth = getDepth(root.left);
        int rightDepth = getDepth(root.right);
        int maxDepth = 1 + Math.max(leftDepth, rightDepth);
        return maxDepth;
    }
}