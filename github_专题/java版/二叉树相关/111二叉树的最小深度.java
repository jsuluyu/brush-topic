/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */

// 法1：递归法
class Solution {
    public int minDepth(TreeNode root) {
        if(root == null)
            return 0;
        return getDepth(root);
    }
    
    public static int getDepth(TreeNode root) {
        if(root == null)
            return 0;
        int leftDepth = getDepth(root.left);
        int rightDepth = getDepth(root.right);
        
        if(root.left == null && root.right != null)
            return 1 + rightDepth;
        if(root.left != null && root.right == null)
            return 1 + leftDepth;
        int maxDepth = 1 + Math.min(leftDepth, rightDepth);
        return maxDepth;
    }
}

// 法二：层次遍历
public class Solution {
    public int minDepth(TreeNode root) {
        if(root == null)
            return 0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        int depth = 0;
        while(!queue.isEmpty()) {
            int size = queue.size();
            boolean flag = false;
            depth++;
            for(int i=0; i<size; i++) {
                TreeNode node = queue.poll();
                if(node.left != null) queue.offer(node.left);
                if(node.right != null) queue.offer(node.right);
                if(node.left == null && node.right == null) {
                    flag = true;
                    break;
                }
            }
            if(flag) break;
        }
        return depth;
    }
}
