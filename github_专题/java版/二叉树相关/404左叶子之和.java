/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */

// 法一：后序遍历
class Solution {
    public int sumOfLeftLeaves(TreeNode root) {
        if(root == null)
            return 0;
        int leftValue = sumOfLeftLeaves(root.left);
        int rightValue = sumOfLeftLeaves(root.right);
        
        int leftLeaf = 0;
        if(root.left != null && root.left.left == null && root.left.right == null)
            leftLeaf = root.left.val;
        int sum = leftLeaf + rightValue + leftValue;
        return sum;
    }
}


// 法二：dfs
class Solution {
    public int sumOfLeftLeaves(TreeNode root) {
        if(root == null)
            return 0;
        return dfs(root);
    }

    int dfs(TreeNode node) {
        int ans = 0;
        if(node.left != null) {
            ans += isLeaf(node.left) ? node.left.val : dfs(node.left);
        }
        if(node.right != null && !isLeaf(node.right)) {
            ans += dfs(node.right);
        }
        return ans;
    }

    boolean isLeaf(TreeNode node) {
        if(node.left == null && node.right == null)
            return true;
        else
            return false;
    }
}