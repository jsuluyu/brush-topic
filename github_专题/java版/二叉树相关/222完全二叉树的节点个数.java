/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */

// 法一：后序遍历 递归
class Solution {
    public int countNodes(TreeNode root) {
        if(root == null)
            return 0;
        return getCount(root);
    }
    
    public static int getCount(TreeNode root) {
        if(root == null)
            return 0;
        int leftCount = getCount(root.left);
        int rightCount = getCount(root.right);
        int nodeCount = 1 + leftCount + rightCount;
        return nodeCount;
    }
}


// 法2：层次遍历
class Solution {
    public int countNodes(TreeNode root) {
        if (root == null)
            return 0;
        int nodeCount = 0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while(!queue.isEmpty()) {
            int size = queue.size();
            for(int i=0; i<size; i++) {
                nodeCount++;
                TreeNode node = queue.poll();
                if(node.left != null) queue.offer(node.left);
                if(node.right != null) queue.offer(node.right);
            }
        }
        return nodeCount;
    }
}