// 法1：利用搜索二叉树性质，构造数组
class Solution {
    private List<Integer> list = new ArrayList<>();

    public int getMinimumDifference(TreeNode root) {
        traversal(root);

        if(list.size() < 2)
            return 0;
        int ans = Integer.MAX_VALUE;
        for(int i=1; i<list.size(); i++) {
            ans = Math.min(ans, list.get(i) - list.get(i-1));
        }
        return ans;
    }

    public void traversal(TreeNode root) {
        if(root == null)
            return;
        traversal(root.left);
        list.add(root.val);
        traversal(root.right);
    }
}

// 法二：利用搜索二叉树性质，利用双指针比较大小
class Solution {
    private int ans = Integer.MAX_VALUE;
    private TreeNode pre = null;
    
    public int getMinimumDifference(TreeNode root) {
        traversal(root);
        return ans;
    }
    
    public void traversal(TreeNode cur) {
        if(cur == null)
            return;
        traversal(cur.left);
        if(pre != null) {
            ans = Math.min(ans, cur.val - pre.val);
        }
        pre = cur;
        traversal(cur.right);
    }
}