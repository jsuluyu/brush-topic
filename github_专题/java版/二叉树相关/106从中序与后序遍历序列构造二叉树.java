public TreeNode buildTree(int[] inorder, int[] postorder) {
        if(inorder.length == 0 || postorder.length == 0)
            return null;
        return traversal(inorder, postorder);
    }

    public TreeNode traversal(int[] inorder, int[] postorder) {
        if(postorder.length == 0)
            return null;

        int rootValue = postorder[postorder.length-1];
        TreeNode node = new TreeNode(rootValue);

        if(postorder.length == 1)
            return node;

        int delimeterIndex;
        for(delimeterIndex = 0; delimeterIndex < inorder.length; delimeterIndex++) {
            if(inorder[delimeterIndex] == rootValue)
                break;
        }

        int[] leftInorder = Arrays.copyOfRange(inorder, 0, delimeterIndex);
        int[] rightInorder = Arrays.copyOfRange(inorder, delimeterIndex+1, inorder.length);

        postorder = Arrays.copyOfRange(postorder, 0, postorder.length-1);

        int[] leftPostorder = Arrays.copyOfRange(postorder, 0, leftInorder.length);
        int [] rightPostorder = Arrays.copyOfRange(postorder, leftInorder.length, postorder.length);
        
        node.left = traversal(leftInorder, leftPostorder);
        node.right = traversal(rightInorder, rightPostorder);
        
        return node;
    }