class Solution {
    private int count;
    private int maxCount;
    private TreeNode pre = null;
    private List<Integer> result = new ArrayList<>();

    public int[] findMode(TreeNode root) {
        count = 0;
        maxCount = 0;
        searchBST(root);

        int[] ans = new int[result.size()];
        for(int i=0; i<result.size(); i++) {
            ans[i] = result.get(i);
        }
        return ans;
    }

    public void searchBST(TreeNode cur) {
        if(cur == null)
            return;
        searchBST(cur.left);

        if(pre == null) { // 第一个结点
            count = 1;
        }
        else if(pre.val == cur.val) { // 若当前的结点值和之前的结点值相等
            count++;
        }
        else { // 若当前结点值和之前的结点值不相等
            count = 1;
        }
        pre = cur; // 更新前一结点
        
        if(count == maxCount) {
            result.add(cur.val);
        }
        if(count > maxCount) {
            maxCount = count;
            result.clear();
            result.add(cur.val);
        }
        
        searchBST(cur.right);
    }
}