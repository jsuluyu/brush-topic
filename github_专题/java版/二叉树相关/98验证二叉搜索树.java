// 法1：利用二叉树中序遍历的特性
public class Solution {
    private List<Integer> list = new ArrayList();

    public boolean isValidBST(TreeNode root) {
        if(root == null)
            return true;
        // 中序遍历树，获得该树对应的中序遍历数组
        inorderTraver(root);
        
        // 检查数组是否递增
        for(int i=1; i<list.size(); i++) {
            if(list.get(i) <= list.get(i - 1))
                return false;
        }
        return true;
    }

    void inorderTraver(TreeNode root) {
        if(root == null)
            return;
        inorderTraver(root.left);
        list.add(root.val);
        inorderTraver(root.right);
    }
}

// 法二：递归遍历，如果该二叉树的左子树不为空，则左子树上所有节点的值均小于它的根节点的值； 
//                若它的右子树不空，则右子树上所有节点的值均大于它的根节点的值；它的左右子树也为二叉搜索树。
public class Solution {
    public boolean isValidBST(TreeNode root) {
        return traverse(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    public boolean traverse(TreeNode root, long lower, long upper) {
        if(root == null)
            return true;
        if(root.val <= lower || root.val >= upper)
            return false;
        return traverse(root.left, lower, root.val) && traverse(root.right, root.val, upper);
    }
}
