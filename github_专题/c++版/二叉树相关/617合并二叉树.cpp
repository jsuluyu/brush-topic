/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
// 法1：前序遍历，修改原树
class Solution {
public:
   TreeNode* mergeTrees(TreeNode* root1, TreeNode* root2) {
       // 单颗树为空时
       if(root1 == NULL) return root2;
       if(root2 == NULL) return root1;

        // 重叠时
        root1->val += root2->val; // 中

        // 递归
        root1->left = mergeTrees(root1->left, root2->left);  // 左
        root1->right = mergeTrees(root1->right, root2->right); // 右

        return root1;
    }
};


// 法2：前序遍历，创造新树
class Solution {
public:
   TreeNode* mergeTrees(TreeNode* root1, TreeNode* root2) {
       if(root1 == nullptr) return root2;
       if(root2 == nullptr) return root1;

       TreeNode* node = new TreeNode(0);
       node->val = root1->val + root2->val;
       node->left = mergeTrees(root1->left, root2->left);
       node->right = mergeTrees(root1->right, root2->right);

       return node;
   }
};