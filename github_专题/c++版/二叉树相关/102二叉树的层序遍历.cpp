/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        queue<TreeNode*> que;
        vector<vector<int>> result;
        if(root == nullptr)
            return result;
        que.push(root);
        while(!que.empty()) {
            int size = que.size();
            vector<int> list;
            for(int i=0; i<size; i++) {
                TreeNode* tNode = que.front();
                que.pop();
                list.push_back(tNode->val);
                if(tNode->left != nullptr)
                    que.push(tNode->left);
                if(tNode->right)
                    que.push(tNode->right);
            }
            result.push_back(list);
        }
        return result;
    }
};