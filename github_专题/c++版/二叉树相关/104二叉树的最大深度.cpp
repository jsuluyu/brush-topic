/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

// 法一：层次遍历 （一般求深度、最短路径问题都用层次遍历(宽度优先搜索)）
class Solution {
public:
    int maxDepth(TreeNode* root) {
        if(root == NULL)
            return 0;
        int depth = 0;
        queue<TreeNode*> que;
        que.push(root);
        while(!que.empty()) {
            int size = que.size();
            for(int i=0; i<size; i++) {
                TreeNode* node = que.front(); 
                que.pop();
                if(node->left)
                    que.push(node->left);
                if(node->right)
                    que.push(node->right);
            }
            depth++;
        }
        return depth;
    }
};


// 法二：后序遍历的递归法
class Solution {
public:
    int getDepth(TreeNode* root) 
    {
        if(root == NULL)
            return 0;
        int leftDepth = getDepth(root->left); // 递归搜索左子树
        int rightDept = getDepth(root->right); // 递归搜索右子树
        int maxDepth = 1 + max(leftDepth, rightDept);
        return maxDepth;
    }

    int maxDepth(TreeNode* root) {
        return getDepth(root);
    }
};