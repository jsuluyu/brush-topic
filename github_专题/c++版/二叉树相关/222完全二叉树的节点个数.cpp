/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

// 法1：递归 后序遍历
class Solution {
public:
    int getCount(TreeNode* root)
    {
        if(root == NULL)
            return 0;
        int leftCount = getCount(root->left);
        int rightCount = getCount(root->right);
        int nodeCount = 1 + leftCount + rightCount;
        return nodeCount;
    }

    int countNodes(TreeNode* root) {
        if(root == NULL)
            return 0;
        return getCount(root);
    }
};


// 法2：层次遍历
class Solution {
public:
    int countNodes(TreeNode* root) {
        if(root == NULL)
            return 0;
        queue<TreeNode*> que;
        que.push(root);
        int nodeCount = 0;
        while(!que.empty()) {
            int size = que.size();
            for(int i=0; i<size; i++) {
                nodeCount++;
                TreeNode* node = que.front();
                que.pop();
                if(node->left) que.push(node->left);
                if(node->right) que.push(node->right);
            }
        }
        return nodeCount;
    }
};