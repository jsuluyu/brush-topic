/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

/*
这里可能会疑惑，在递归遍历的时候，什么时候直接return 递归函数的返回值，什么时候不用加这个 return呢。

我们在二叉树：递归函数究竟什么时候需要返回值，什么时候不要返回值？中讲了，如果要搜索一条边，递归函数就要加返回值，这里也是一样的道理。

「因为搜索到目标节点了，就要立即return了，这样才是找到节点就返回（搜索某一条边），如果不加return，就是遍历整棵树了。」
*/

// 法1：递归法
class Solution {
public:
    TreeNode* searchBST(TreeNode* root, int val) {
        if(root == NULL || root->val == val) return root;
        if(val < root->val) return searchBST(root->left, val);
        if(val > root->val) return searchBST(root->right, val);
        return NULL;
    }
};


// 法2：迭代法
class Solution {
public:
    TreeNode* searchBST(TreeNode* root, int val) {
        while(root != NULL) {
            if(val > root->val)
               root = root->right;
            else if(val < root->val)
                root = root->left;
            else
                break;
       }
       return root;
    }
};