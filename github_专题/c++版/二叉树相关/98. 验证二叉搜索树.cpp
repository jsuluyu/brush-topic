// 法1：利用二叉搜索树的性质：中序遍历下，输出的二叉搜索树节点的数值是有序序列
class Solution {
public:
    vector<int> arr;
    bool isValidBST(TreeNode* root) {
        inorderTraver(root);
        // 判断该数组是否递增
        for(int i=1; i<arr.size(); i++) {
            if(arr[i] <= arr[i-1])
                return false;
        }
        return true;
    }

    void inorderTraver(TreeNode* root) {
        if(root == NULL)
            return;
        inorderTraver(root->left);
        arr.push_back(root->val);
        inorderTraver(root->right);
    }
};


// 法二：递归遍历，如果该二叉树的左子树不为空，则左子树上所有节点的值均小于它的根节点的值； 
//                若它的右子树不空，则右子树上所有节点的值均大于它的根节点的值；它的左右子树也为二叉搜索树。

class Solution {
public:
    bool isValidBST(TreeNode* root) {
        return traverse(root, LONG_MIN, LONG_MAX);
    }

    bool traverse(TreeNode* root, long long lower, long long upper) 
    {
        if(root == NULL)
            return true;
        if(root->val >= upper || root->val <= lower) 
            return false;
        return traverse(root->left, lower, root->val) && traverse(root->right, root->val, upper);
    }
};