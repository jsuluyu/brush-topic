/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
// 法1：递归，与求树的最深还有区别，注意
class Solution {
public:
    int getDepth(TreeNode* root)
    {
        if(root == NULL)
            return 0;
        int leftDepth = getDepth(root->left); // 左
        int rightDepth = getDepth(root->right); // 右
                                                // 中
        // 当一个左子树为空，右不为空，这时并不是最低点
        if(root->left == NULL && root->right != NULL)
            return 1 + rightDepth;
        // 当一个右子树为空，左不为空，这时并不是最低点
        if(root->left != NULL && root->right == NULL)
            return 1 + leftDepth;
        int minDepth = 1 + min(leftDepth, rightDepth);
        return minDepth;
    }

    int minDepth(TreeNode* root) {
        if(root == NULL)
            return 0;
        return getDepth(root);
    }
};


// 法2：层次遍历。注意当遇到第一个叶子结点停止遍历
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int minDepth(TreeNode* root) {
        if(root == NULL)
            return 0;
        queue<TreeNode*> que;
        que.push(root);
        int depth = 0;
        while(!que.empty()) {
            int size = que.size();
            depth++;
            int flag = 0;
            for(int i=0; i<size; i++) {
                TreeNode* node = que.front();
                que.pop();
                if(node->left)
                    que.push(node->left);
                if(node->right)
                    que.push(node->right);
                if(node->left == NULL && node->right == NULL) {
                    flag = 1;
                    break;
                }
            }
            if(flag) break;
        }
        return depth;
    }
};