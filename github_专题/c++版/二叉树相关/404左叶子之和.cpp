/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

// 法一： 后序遍历
class Solution {
public:
    int sumOfLeftLeaves(TreeNode* root) {
        if(root == NULL)
            return 0;
        int leftValue = sumOfLeftLeaves(root->left); // 递归求左子树的左叶结点和
        int rightValue = sumOfLeftLeaves(root->right); // 递归求右子树的左叶子结点和

        int leftLeafValue = 0;
        if(root->left != NULL && root->left->left == NULL && root->left->right == NULL)
            leftLeafValue = root->left->val;
        int sum = leftLeafValue + rightValue + leftValue;
        return sum;
    }
};


// 法二： dfs
class Solution {
public:
    int sumOfLeftLeaves(TreeNode* root) {
        if(root == NULL)
            return 0;
        return dfs(root);
    }

    int dfs(TreeNode* root)
    {
        int ans = 0;
        if(root->left) {
            ans += isLeafNode(root->left) ? root->left->val : dfs(root->left);
        }
        if(root->right && !isLeafNode(root->right)) {
            ans += dfs(root->right);
        }
        return ans;
    }

    bool isLeafNode(TreeNode* node) 
    {
        if(node->left == NULL && node->right == NULL)
            return true;
        else 
            return false;
    }
};