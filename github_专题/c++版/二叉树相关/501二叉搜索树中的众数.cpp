// 法1：针对所有的二叉树通用，先遍历二叉树，然后通过map记录各个结点值的频率，最后按频率大小获得最大的值
class Solution {
public:
    vector<int> result;
    unordered_map<int, int> hashMap;

    bool static cmp(pair<int, int>& x, pair<int, int>& y)
    {
        return x.second > y.second;
    }

    vector<int> findMode(TreeNode* root) {
        if(root == NULL)
            return result;
        searchBST(root);
        
        // 由于map只能对key排序，不能对value排序,因此需要先转化为vector按value进行排序
        vector<pair<int, int>> vec(hashMap.begin(), hashMap.end());
        sort(vec.begin(), vec.end(), cmp);
        result.push_back(vec[0].first);
        for(int i=1; i<vec.size(); i++) {
            if(vec[i].second == vec[0].second)
                result.push_back(vec[i].first);
        }
        return result;
    }

    void searchBST(TreeNode* root)
    {
        if(root == NULL)
            return;
        hashMap[root->val]++;
        searchBST(root->left);
        searchBST(root->right);
    }
};


// 法2：思想--遍历有序数组的元素出现频率，从头遍历，那么一定是相邻两个元素作比较，然后就把出现频率最高的元素输出就可以了
class Solution {
public:
    int maxCount; // 最大频率
    int count; // 频率
    vector<int> result;
    TreeNode* pre = NULL; // 指向前一个结点

    vector<int> findMode(TreeNode* root) {
        if(root == NULL)
            return result;

        maxCount = 0;
        count = 0;
        searchBST(root);
        return result;
    }

    void searchBST(TreeNode* cur) {
        if(cur == NULL)
            return;
        searchBST(cur->left); // 左
        if(pre == NULL) { // 第一个结点
            count = 1;
        } 
        else if(pre->val == cur->val) { // 与前一个结点相同
            count++;
        }
        else {  // 与前一个结点不同
            count = 1;
        }
        pre = cur; // 更新上一个结点

        if(count == maxCount) { // 如果此值和最大频率对应的值相等，则加入result
            result.push_back(cur->val);
        }
        if(count > maxCount) {
            maxCount = count;
            result.clear(); // 更新了最大频率，所以之前最大频率对应的值该舍弃
            result.push_back(cur->val);
        }

        searchBST(cur->right); // 右

        return;
    }
};