/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

// 法1：递归法
class Solution {
public:
    bool compareNode(TreeNode* left, TreeNode* right) 
    {
        // 首先排除空节点的情况
        if((left == NULL && right != NULL) || (left != NULL && right == NULL))
            return false;
        else if(left == NULL && right == NULL)
            return true;
        // 排除了空节点，再排除数值不相同的情况
        else if(left->val != right->val)
            return false;
        
        // 此时就是：左右节点都不为空，且数值相同的情况
        // 此时才做递归，做下一层的判断
        bool outSide = compareNode(left->left, right->right);
        bool inSide = compareNode(left->right, right->left);
        bool isSame = outSide && inSide;
        return isSame;
    }

    bool isSymmetric(TreeNode* root) {
        if(root == NULL)
            return true;
        return compareNode(root->left, root->right);

    }
};

// 法二：迭代法
class Solution {
public:
    bool isSymmetric(TreeNode* root) {
        if(root == NULL)
            return true;
        queue<TreeNode*> que;
        que.push(root->left); // 将左子树头结点加入队列
        que.push(root->right); // 将右子树头结点加入队列
        while(!que.empty()) {
            TreeNode* leftNode = que.front(); que.pop();
            TreeNode* rightNode = que.front(); que.pop();
            if(leftNode == NULL && rightNode == NULL) // 左节点为空、右节点为空，此时说明是对称的
                continue;
            // 左右一个节点不为空，或者都不为空但数值不相同，返回false
            if(!leftNode || !rightNode || (leftNode->val != rightNode->val))
                return false;
            que.push(leftNode->left);
            que.push(rightNode->right);
            que.push(leftNode->right);
            que.push(rightNode->left);
        }
        return true;
    }
};

// 法二：迭代法
class Solution {
    public boolean isSymmetric(TreeNode root) {
        if(root == null)
            return true;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root.left);
        queue.offer(root.right);
        while(!queue.isEmpty()) {
            TreeNode leftNode = queue.poll();
            TreeNode rightNode = queue.poll();
            
            if(leftNode == null && rightNode == null)
                continue;
            if(leftNode == null || rightNode == null || (leftNode.val != rightNode.val))
                return false;
            
            queue.offer(leftNode.left);
            queue.offer(rightNode.right);
            queue.offer(leftNode.right);
            queue.offer(rightNode.left);
        }
        return true;
    }
}