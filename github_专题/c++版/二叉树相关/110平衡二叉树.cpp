/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

// 后序遍历：求深度可以从上到下去查 所以需要前序遍历（中左右），而高度只能从下到上去查，所以只能后序遍历（左右中）
/*
通过本题可以了解求二叉树深度 和 二叉树高度的差异，求深度适合用前序遍历，而求高度适合用后序遍历。
*/
class Solution {
public:
    int getDepth(TreeNode* root) // 返回以该节点为根节点的二叉树的高度，如果不是二叉搜索树了则返回-1
    {
        if(root == NULL)
            return 0;
        int leftDepth = getDepth(root->left);
        if(leftDepth == -1)  // 说明左子树已经不是二叉平衡树
            return -1;
        int rightDepth = getDepth(root->right);
        if(rightDepth == -1) // 说明右子树已经不是二叉平衡树
            return -1;
        return abs(leftDepth - rightDepth) > 1 ? -1 : 1 + max(leftDepth, rightDepth);  
    }
    bool isBalanced(TreeNode* root) {
        if(root == NULL)
            return true;
        return getDepth(root) == -1 ? false : true;
    }
};