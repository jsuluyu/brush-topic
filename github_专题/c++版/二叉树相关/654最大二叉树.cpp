class Solution {
public:
    TreeNode* constructMaximumBinaryTree(vector<int>& nums) {
        // 那么应该定义一个新的节点，并把这个数组的数值赋给新的节点，
        // 然后返回这个节点。这表示一个数组大小是1的时候，构造了一个新的节点，并返回
        TreeNode* node = new TreeNode(0);
        if(nums.size() == 1) {
            node->val = nums[0];
            return node;
        }

        // 找到数组中最大的值和对应的下标
        int maxValue = 0;
        int maxIdx;
        for(int i=0; i<nums.size(); i++) {
            if(nums[i] > maxValue) {
                maxValue = nums[i];
                maxIdx = i;
            }
        }
        node->val = maxValue;

        // 最大值所在的下标左区间 构造左子树
        if(maxIdx > 0) {
            vector<int> newVec(nums.begin(), nums.begin() + maxIdx);
            node->left = constructMaximumBinaryTree(newVec);
        }

        // 最大值所在的下表右区间 构造右子树
        if(maxIdx < nums.size() - 1) {
            vector<int> newVec(nums.begin() + maxIdx + 1, nums.end());
            node->right = constructMaximumBinaryTree(newVec);
        }

        return node;
    }
};