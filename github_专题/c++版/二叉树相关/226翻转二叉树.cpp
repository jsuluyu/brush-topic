/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

// 前序遍历递归写法
class Solution {
public:
    void invert(TreeNode* root)
    {
        TreeNode* node = root->left;
        root->left = root->right;
        root->right = node;
        if(root->left != nullptr)
            invert(root->left);
        if(root->right != nullptr)
            invert(root->right);
    }
    TreeNode* invertTree(TreeNode* root) {
        if(root == nullptr)
            return nullptr;
        invert(root);
        return root;
    }
};

// 简便递归
class Solution {
public:
    TreeNode* invertTree(TreeNode* root) {
        if(root == NULL)
            return root;
        swap(root->left, root->right);
        invertTree(root->left);
        invertTree(root->right);

        return root;
    }
};