// 法1：利用搜索二叉树性质，构造数组
class Solution {
public:
    vector<int> vec;
    int getMinimumDifference(TreeNode* root) {
        traversal(root);
        
        if(vec.size() < 2)
            return 0;

        int ans = INT_MAX;
        for(int i=1; i<vec.size(); i++) {
            ans = min(ans, vec[i] - vec[i-1]);
        }
        return ans;
    }

    void traversal(TreeNode* root) {
        if(root == nullptr)
            return;
        traversal(root->left);
        vec.push_back(root->val);
        traversal(root->right);
    }
};


// 法二：利用搜索二叉树性质，利用双指针比较大小
class Solution {
public:
    int ans = INT_MAX;
    TreeNode* pre;
    int getMinimumDifference(TreeNode* root) {
        traversal(root);
        return ans;
    }

    void traversal(TreeNode* cur)
    {
        if(cur == nullptr)
            return;
        traversal(cur->left);
        if(pre != nullptr) {
            ans = min(ans, cur->val - pre->val);
        }
        pre = cur; // 更新pre，指向前一个
        traversal(cur->right);
    }
};