/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

// 简化版（隐含回溯）
class Solution {
public:
    vector<string> candidate;
    void findPath(TreeNode* cur, string path)
    {
        path += to_string(cur->val);
        if(cur->left == NULL && cur->right == NULL) {
            candidate.push_back(path);
            return;
        }

        if(cur->left)
            findPath(cur->left, path + "->");
        if(cur->right)
            findPath(cur->right, path + "->");
    }

    vector<string> binaryTreePaths(TreeNode* root) {
        string path = "";
        findPath(root, path);
        return candidate;
    }
};


// 清晰回溯
class Solution {
public:
    vector<string> candidate;

    void findPath(TreeNode* cur, vector<int>& path)
    {
        path.push_back(cur->val);
        if(cur->left == NULL && cur->right == NULL) {
            string sPath = "";
            for(int i=0; i<path.size()-1; i++) {
                sPath += to_string(path[i]) + "->";
            }
            sPath += to_string(path[path.size()-1]);
            candidate.push_back(sPath);
            return;
        }
        
        if(cur->left) {
            findPath(cur->left, path);
            path.pop_back(); // 回溯
        }
        if(cur->right) {
            findPath(cur->right, path);
            path.pop_back(); // 回溯
        }
    }

    vector<string> binaryTreePaths(TreeNode* root) {
        vector<int> path;
        findPath(root, path);
        return candidate;
    }
};